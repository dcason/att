#ifndef RTX64WINSOCKINCLUDES_H
#define RTX64WINSOCKINCLUDES_H

#include <SDKDDKVer.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#include "RTX64StandardIncludes.h"

#endif // RTX64WINSOCKINCLUDES_H
