#ifndef RESPONSE_H
#define RESPONSE_H

#include <array>

#include "JsonParser.h"
#include "HTTPDecls.h"

#define RESPONSE_ERROR_MAX_LENGTH 255

class Response
{
public:
	bool parse(const char* buffer);

	bool success() const;
	const char* error() const;

	bool get(const char* field, char* val, std::size_t maxLength) const;
	bool get(const char* field, int &val) const;
	bool get(const char* field, unsigned int &val) const;
	bool get(const char* field, float &val) const;
	bool get(const char* field, double &val) const;

private:
	json_token_t m_tokens[MAX_TOKENS];
	json_size_t m_count = 0;

	std::array<char, RCV_BUFFER_SIZE + 1> m_buffer;

	bool m_parsed = false;
	bool m_lastSuccess = false;
	std::array<char, RESPONSE_ERROR_MAX_LENGTH + 1> m_lastError;
};

#endif // RESPONSE_H
