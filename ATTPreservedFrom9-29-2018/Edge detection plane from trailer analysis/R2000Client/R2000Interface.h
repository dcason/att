#pragma once

#include <string>

#include "RuntimeDecls.h"

#include "R2000Config.h"
#include "HTTPSocket.h"
#include "DataClient.h"
#include "ScanSignaler.h"
#include "PacketParser.h"

class R2000Interface
{
public:
	explicit R2000Interface(ScanCallback scanCb, R2000Config config = R2000Config());
	explicit R2000Interface(ScanCallback scanCb, const std::string &ipv4);
	~R2000Interface();

	bool connect();
	bool disconnect();
	bool connected() const;

	bool startScanning();
	bool stopScanning();
	bool scanning() const;

	bool reboot();

	R2000Config& config() { return m_config; }
	const char* deviceInformation() const { return m_config.deviceInfo(); }

private:
	R2000Config			m_config;
	HTTPSocket			m_http;

#if USE_THREADED_SCAN_SIGNALER
	ScanSignaler		m_signaler;
#endif

	PacketParser		m_parser;
	DataClient			m_dataClient;

	bool				m_connected = false;
	bool				m_scanning = false;

	HANDLE				m_dataThread = INVALID_HANDLE_VALUE;
	HANDLE				m_parseThread = INVALID_HANDLE_VALUE;

#if USE_THREADED_SCAN_SIGNALER
	HANDLE				m_signalingThread = INVALID_HANDLE_VALUE;
#endif

	HANDLE				m_watchDogTimer = INVALID_HANDLE_VALUE;
};
