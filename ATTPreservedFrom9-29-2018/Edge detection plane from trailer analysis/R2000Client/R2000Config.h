#ifndef R2000CONFIG_H
#define R2000CONFIG_H

#include <array>

class R2000Config
{
public:
	enum ProtocolType { TCP = 0 }; // UDP is currently unsupported
	enum PacketType : char { A = 'A', B = 'B'}; // PacketType::C is currently unsupported
	
	R2000Config();

	const char* ipv4() const;
	void ipv4(const char* ip);
	
	int port() const;
	void port(int val);
	
	const char* scanHandle();
	void scanHandle(const char* val);
	
	ProtocolType protocol() const;
	void protocol(ProtocolType val);
	
	int frequency() const;
	void frequency(int val);
	
	int samples() const;
	void samples(int val);
	
	int startAngle() const;
	void startAngle(const int val);

	int maxNumPointsScan() const;
	void maxNumPointsScan(int val);
	
	PacketType packetType() const;
	void packetType(PacketType type);

	const char* deviceInfo() const;
	void deviceInfo(const char* val);

	void print() const;

private:
	std::array<char, 16> m_ip;
	int m_port = -1;

	std::array<char, 17> m_scanHandle; // from R2000 docs: handle consists of max. 16 characters (+ \0)
	ProtocolType m_protocol = ProtocolType::TCP;

	int m_frequency = 30;
	int m_samples = 5040;
	int m_startAngle = 0;
	int m_maxNumPointsScan = 0;

	PacketType m_packetType = PacketType::B;
	std::array<char, 512> m_deviceInfo;
};

#endif // R2000CONFIG_H
