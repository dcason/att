#ifndef HTTP_DECLS_H
#define HTTP_DECLS_H

#define RCV_TIMEOUT			5000	// receive timeout (milliseconds)
#define SND_TIMEOUT			5000	// send timeout (milliseconds)

#define SEND_BUFFER_SIZE	512		// receive buffer size
#define RCV_BUFFER_SIZE		1024	// receive buffer size
#define MAX_TOKENS			24		// max number of parsed response json primitives

#define WATCHDOG_TIMER_PERIOD    60000000  // RTX64 timer period - 6 sec. in 100ns steps
#define R2000_WATCHDOG_TIMEOUT   10000     // R2000 watchdog timeout period - 10 sec. in 1ms steps    

#endif // HTTP_DECLS_H
