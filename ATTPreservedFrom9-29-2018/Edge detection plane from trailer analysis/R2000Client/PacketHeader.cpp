#include "PacketHeader.h"

template <typename Val> inline 
void memcopy(const char* buffer, uint32_t pos, Val &val)
{
	memcpy(&val, buffer + pos, sizeof(Val));
}

bool PacketHeader::parse(const char* buffer)
{
	memcopy(buffer, static_cast<uint32_t>(HeaderPositions::packet_type), m_packet_type);
	memcopy(buffer, static_cast<uint32_t>(HeaderPositions::packet_size), m_packet_size);
	memcopy(buffer, static_cast<uint16_t>(HeaderPositions::header_size), m_header_size);
	memcopy(buffer, static_cast<uint32_t>(HeaderPositions::scan_number), m_scan_number);
	memcopy(buffer, static_cast<uint32_t>(HeaderPositions::packet_number), m_packet_number);
	memcopy(buffer, static_cast<uint32_t>(HeaderPositions::timestamp_raw), m_timestamp_raw);
	memcopy(buffer, static_cast<uint32_t>(HeaderPositions::num_points_scan), m_num_points_scan);
	memcopy(buffer, static_cast<uint32_t>(HeaderPositions::num_points_packet), m_num_points_packet);
	memcopy(buffer, static_cast<uint32_t>(HeaderPositions::first_index), m_first_index);
	memcopy(buffer, static_cast<uint32_t>(HeaderPositions::first_angle), m_first_angle);
	return true;
}
