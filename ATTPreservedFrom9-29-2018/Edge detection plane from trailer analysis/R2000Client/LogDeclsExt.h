#ifndef LOGDECLSEXT_H
#define LOGDECLSEXT_H

#include "LogDecls.h"

#define LOG_DEBUG(name, func, ...)		(void)0
#define LOG_INFO(name, func, ...)		(void)0
#define LOG_WARN(name, func, ...)		(void)0
#define LOG_ERROR(name, func, ...)		(void)0
#define LOG_FATAL(name, func, ...)		(void)0

#if !NO_LOG_OUTPUT
	#if MIN_LOG_SEVERITY <= LOG_SEV_DEBUG
	#undef LOG_DEBUG
	#define LOG_DEBUG(name, func, ...)				RtPrintf("[" name "::" func "] <DEBUG> " __VA_ARGS__)
	#endif
	
	#if MIN_LOG_SEVERITY <= LOG_SEV_INFO
	#undef LOG_INFO
	#define LOG_INFO(name, func, ...)				RtPrintf("[" name "::" func "] <INFO> " __VA_ARGS__)
	#endif
	
	#if MIN_LOG_SEVERITY <= LOG_SEV_WARN
	#undef LOG_WARN
	#define LOG_WARN(name, func, ...)				RtPrintf("[" name "::" func "] <WARN> " __VA_ARGS__)
	#endif

	#if MIN_LOG_SEVERITY <= LOG_SEV_ERROR
	#undef LOG_ERROR
	#define LOG_ERROR(name, func, ...)				RtPrintf("[" name "::" func "] <ERROR> " __VA_ARGS__)
	#endif

	#if MIN_LOG_SEVERITY <= LOG_SEV_FATAL
	#undef LOG_FATAL
	#define LOG_FATAL(name, func, ...)				RtPrintf("[" name "::" func "] <FATAL> " __VA_ARGS__)
	#endif
#endif

#endif // LOGDECLSEXT_H
