#include "PacketParser.h"
#include "LogDeclsExt.h"

#define RAD2DEG		3.14159265358979323846f / 180.0f

bool PacketParser::initParsing()
{
	if (m_parsing)
	{
		LOG_INFO("PacketParser", "initParsing", "parser already active.\n");
		return false;
	}

	m_buffer.reset();

	m_stopParsingEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_parsingStoppedEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_dataAvailableEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	return true;
}

void PacketParser::stopParsing()
{
	if (m_parsing)
	{
		SetEvent(m_stopParsingEvent);
		WaitForSingleObject(m_parsingStoppedEvent, INFINITE);
		m_parsing = false;
	}
	else
	{
		LOG_INFO("PacketParser", "stopParsing", "parser not active.\n");
	}

	CloseHandle(m_stopParsingEvent);
	CloseHandle(m_parsingStoppedEvent);
	CloseHandle(m_dataAvailableEvent);

	m_stopParsingEvent = INVALID_HANDLE_VALUE;
	m_parsingStoppedEvent = INVALID_HANDLE_VALUE;
	m_dataAvailableEvent = INVALID_HANDLE_VALUE;
}

void PacketParser::appendScan(const char* scanBuf, unsigned int size)
{
	if (!m_parsing)
		return;

	SignalerPushResult res = m_buffer.push(BufEntry::data_t(scanBuf, size));
	if (res == SignalerPushResult::Success)
		SetEvent(m_dataAvailableEvent);
	else if (res == SignalerPushResult::BufferFull)
		LOG_ERROR("PacketParser", "appendScan", "buffer was full, data discarded.\n");
	else // if (res == Buffer::SignalerPushResult::ErrorInEntry)
		LOG_ERROR("PacketParser", "appendScan", "could not copy data to entry, data discarded.\n");
}

template <typename ValT, typename BufT> inline
ValT readAndIncrement(BufT*& buf)
{
	static_assert(sizeof(uint32_t) > sizeof(BufT), "buffer type size >= 32 bit");

	auto val = *reinterpret_cast<const ValT*>(buf);
	buf += (sizeof(ValT) / sizeof(BufT));
	return val;
}

R2000Scan::Ptr PacketParser::parseScanTypeA(const PacketHeader& header, const char* buffer, uint32_t bufferSize) const
{
	if (header.packetSize() > bufferSize)
		return nullptr;

	if (header.packetType() != static_cast<uint16_t>(PacketHeader::PacketType::A))
		return nullptr;

	auto scan = std::make_unique<R2000Scan>(header.numPointsPacket(), header.timestamp());
	scan->scanNumber = header.scanNumber();
	scan->packetNumber = header.packetNumber();

	const float angularIncrement = 360.0f / header.numPointsScan();
	for (uint16_t i = 0; i < header.numPointsPacket(); ++i)
	{
		const uint32_t distance = readAndIncrement<uint32_t>(buffer);
		if (distance == 0xffffffff)
			continue;

		const float angle = (m_startAngle + (header.firstPointIndex() + i) * angularIncrement) * RAD2DEG;
		scan->points.emplace_back(
			angle,
			std::cosf(angle) * distance,
			std::sinf(angle) * distance,
			1.0f);
	}

	return scan;
}

R2000Scan::Ptr PacketParser::parseScanTypeB(const PacketHeader& header, const char* buffer, uint32_t bufferSize) const
{
	if (header.packetSize() > bufferSize)
		return nullptr;

	if (header.packetType() != static_cast<uint16_t>(PacketHeader::PacketType::B))
		return nullptr;

	auto scan = std::make_unique<R2000Scan>(header.numPointsPacket(), header.timestamp());
	scan->scanNumber = header.scanNumber();
	scan->packetNumber = header.packetNumber();

	const float angularIncrement = 360.0f / header.numPointsScan();
	for (uint16_t i = 0; i < header.numPointsPacket(); ++i)
	{
		const uint32_t distance = readAndIncrement<uint32_t>(buffer);
		const uint16_t amplitude = readAndIncrement<uint16_t>(buffer);

		if (distance == 0xffffffff)
			continue;

		const float angle = (m_startAngle + (header.firstPointIndex() + i) * angularIncrement) * RAD2DEG;
		scan->points.emplace_back(
			angle,
			std::cosf(angle) * distance,
			std::sinf(angle) * distance,
			amplitude / PacketHeader::amplitude_divisor());
	}

	return scan;
}

void PacketParser::parse()
{
	if (m_parsing)
		return;

	m_parsing = true;

	bool bufferEmpty = true;
	BufEntry entry;
	PacketHeader header;
	R2000Scan::Ptr scan;

	do {
		if (bufferEmpty)
		{
			if (DWORD error = WaitForSingleObject(m_dataAvailableEvent, 500))
			{
				if (m_parsing)
					LOG_ERROR("PacketParser", "parse", 
						"wait failed with error (%i). Aborted parsing.\n", error);
				break;
			}

			ResetEvent(m_dataAvailableEvent);
		}

		if (!m_buffer.pull(entry, bufferEmpty))
			continue; // wait for data

		if (header.parse(entry.buf()))
		{
			if (header.packetType() == static_cast<uint16_t>(PacketHeader::PacketType::A))
				scan = parseScanTypeA(header, entry.buf() + header.headerSize(), entry.size);
			else if (header.packetType() == static_cast<uint16_t>(PacketHeader::PacketType::B))
				scan = parseScanTypeB(header, entry.buf() + header.headerSize(), entry.size);

			if (!scan)
				LOG_WARN("PacketParser", "parse", "error parsing packet body. Discarding buffer.\n");
		}
		else
		{
			LOG_WARN("PacketParser", "parse", "error parsing packet header. Discarding buffer.\n");
		}

#if USE_THREADED_SCAN_SIGNALER
		if (scan)
			m_signaler.append(std::move(scan));
#else
		if (scan)
			m_cb(std::move(scan));
#endif


	} while (WaitForSingleObject(m_stopParsingEvent, 0));

	LOG_INFO("PacketParser", "parse", "Stopped parsing\n");
}
