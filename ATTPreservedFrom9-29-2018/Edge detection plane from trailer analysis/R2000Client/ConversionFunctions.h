#ifndef CONVERSIONFUNCTIONS_H
#define CONVERSIONFUNCTIONS_H

#include <cstddef>

// String conversion functions:
//	supported:
//		leading minus (-) sign
//		
//	unsupported:
//		no checks are performed (i.e. if the number fits inside the given data type)
//		leading whitespaces
//		scientific notation (e/E)
//		hexadecimal, octal and binary notation
//		non-English decimal separators
//		any other non-numeric character

extern inline bool str_conv(const char* str, std::size_t length, int& val);
extern inline bool str_conv(const char* str, std::size_t length, unsigned int& val);
extern inline bool str_conv(const char* str, std::size_t length, float& val);
extern inline bool str_conv(const char* str, std::size_t length, double& val);

#endif // CONVERSIONFUNCTIONS_H
