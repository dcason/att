#include "ScanSignaler.h"

#if USE_THREADED_SCAN_SIGNALER
#include "LogDeclsExt.h"

void ScanSignaler::init()
{
	if (m_signaling)
	{
		LOG_INFO("ScanSignaler", "init", "signaler already active.\n");
		return;
	}

	m_buffer.reset();

	m_dataAvailableEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_stopSignalingEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_stoppedEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
}

void ScanSignaler::stop()
{
	if (m_signaling)
	{
		SetEvent(m_stopSignalingEvent);
		WaitForSingleObject(m_stoppedEvent, INFINITE);
		m_signaling = false;
	}
	else
	{
		LOG_INFO("ScanSignaler", "stop", "signaler not active.\n");
	}

	CloseHandle(m_dataAvailableEvent);
	CloseHandle(m_stopSignalingEvent);
	CloseHandle(m_stoppedEvent);

	m_dataAvailableEvent = INVALID_HANDLE_VALUE;
	m_stopSignalingEvent = INVALID_HANDLE_VALUE;
	m_stoppedEvent = INVALID_HANDLE_VALUE;
}

void ScanSignaler::append(R2000Scan::Ptr&& scan)
{
	if (!m_signaling)
		return;

	SignalerPushResult res = m_buffer.push(BufEntry::data_t(std::move(scan)));
	if (res == SignalerPushResult::Success)
		SetEvent(m_dataAvailableEvent);
	else if (res == SignalerPushResult::BufferFull)
		LOG_ERROR("ScanSignaler", "append", "buffer was full, data discarded.\n");
	else // if (res == Buffer::SignalerPushResult::ErrorInEntry)
		LOG_ERROR("ScanSignaler", "append", "could not copy data to entry, data discarded.\n");
}

void ScanSignaler::start()
{
	if (m_signaling)
		return;

	m_signaling = true;

	bool bufferEmpty = true;
	BufEntry entry;

	do {
		if (bufferEmpty)
		{
			if (DWORD error = WaitForSingleObject(m_dataAvailableEvent, 1000))
			{
				if (m_signaling)
					LOG_ERROR("ScanSignaler", "start",
						"wait failed with error (%i). Aborted signaling.\n", error);
				break;
			}

			ResetEvent(m_dataAvailableEvent);
		}

		if (!m_buffer.pull(entry, bufferEmpty))
			continue; // wait for data

		if (entry.scan)
			m_cb(std::move(entry.scan));

	} while (WaitForSingleObject(m_stopSignalingEvent, 0));

	LOG_INFO("ScanSignaler", "start", "Stopped signaling\n");
}

#endif // USE_THREADED_SCAN_SIGNALER