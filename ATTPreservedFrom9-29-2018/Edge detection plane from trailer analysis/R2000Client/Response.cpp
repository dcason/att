#include "Response.h"
#include "RTX64StandardIncludes.h"
#include "LogDeclsExt.h"
#include "ConversionFunctions.h"

namespace response
{
	static const int  error_code_success = 0;
	static const char *error_code = "error_code";
	static const char *error_text = "error_text";
	static const char *http_ok = "HTTP/1.1 200 OK";
}

inline const char* strbegincmp(const char* str, json_size_t strLength, const char* query)
{
	if (!str || !query || !strLength)
		return false;

	for (json_size_t i = 0; i < strLength; ++i, ++str, ++query)
	{
		if (*query == '\0')
		{
			if (*str == *query)
				return str;

			return NULL;
		}

		if (*str != *query)
			return NULL;
	}

	return str;
}

inline const char* strbegincmp(const char* str, json_size_t strLength, char query)
{
	if (!str || !strLength)
		return false;

	for (json_size_t i = 0; i < strLength; ++i, ++str)
	{
		if (*str == query)
			return str;
	}

	return NULL;
}

bool Response::parse(const char* buffer)
{
	m_parsed = false;
	m_lastError[0] = '\0';

	if (!strstr(buffer, response::http_ok))
	{
		LOG_INFO("Response", "parse", "response did not contain HTTP OK status");
		return false;
	}

	const char* jsonBegin = strchr(buffer, '{');
	if (!jsonBegin)
	{
		LOG_INFO("Response", "parse", "response did not contain json object");
		return false;
	}

	std::size_t length = strlen(jsonBegin);
	memcpy(m_buffer.data(), jsonBegin, length);
	m_count = json_parse(m_buffer.data(), length, m_tokens, MAX_TOKENS);
	
	if (m_count == 0)
		return false;

	for (json_size_t i = 0; i < m_count; ++i)
	{
		json_token_t& token = m_tokens[i];
		if (token.id_length == 0 || token.value_length == 0)
			continue;

		if (token.type == json_type_t::JSON_PRIMITIVE &&
			strbegincmp(m_buffer.data() + token.id, token.id_length, response::error_code))
		{
			token.type = json_type_t::JSON_UNDEFINED; // avoid rechecking using get() functions
			int errorCode;
			if (str_conv(m_buffer.data() + token.value, token.value_length, errorCode))
			{
				m_lastSuccess = (errorCode == 0);
				m_parsed = true;
			}
		}
		else if (token.type == json_type_t::JSON_STRING &&
			strbegincmp(m_buffer.data() + token.id, token.id_length, response::error_text))
		{
			token.type = json_type_t::JSON_UNDEFINED; // avoid rechecking using get() functions
			std::size_t errLength = min(RESPONSE_ERROR_MAX_LENGTH - 1, token.value_length);
			memcpy(m_lastError.data(), m_buffer.data() + token.value, errLength);
			m_lastError[errLength] = '\0';
		}
	}

	if (!m_parsed)
		LOG_WARN("Response", "parse", "could not parse response: error code not found");

	return m_parsed;
}

bool Response::success() const
{
	return m_parsed && m_lastSuccess;
}

const char* Response::error() const
{
	if (!m_parsed)
		return NULL;

	return m_lastError.data();
}

bool Response::get(const char* field, char* val, std::size_t maxLength) const
{
	if (!val)
		return false;

	for (json_size_t i = 0; i < m_count; ++i)
	{
		const json_token_t& token = m_tokens[i];
		if (token.id_length == 0 || token.value_length == 0)
			continue;

		if (token.type == json_type_t::JSON_STRING &&
			strbegincmp(m_buffer.data() + token.id, token.id_length, field))
		{
			std::size_t length = min(maxLength - 1, token.value_length);
			memcpy(val, m_buffer.data() + token.value, length);
			val[length] = '\0';
			return true;
		}
	}

	LOG_WARN("Response", "get", "could not parse element \"%s\"", field);
	return false;
}

bool Response::get(const char* field, int &val) const
{
	for (json_size_t i = 0; i < m_count; ++i)
	{
		const json_token_t& token = m_tokens[i];
		if (token.id_length == 0 || token.value_length == 0)
			continue;

		if (token.type == json_type_t::JSON_PRIMITIVE &&
			strbegincmp(m_buffer.data() + token.id, token.id_length, field))
		{
			str_conv(m_buffer.data() + token.value, token.value_length, val);
			return true;
		}
	}

	LOG_WARN("Response", "get", "could not parse element \"%s\"", field);
	return false;
}

bool Response::get(const char* field, unsigned int &val) const
{
	for (json_size_t i = 0; i < m_count; ++i)
	{
		const json_token_t& token = m_tokens[i];
		if (token.id_length == 0 || token.value_length == 0)
			continue;

		if (token.type == json_type_t::JSON_PRIMITIVE &&
			strbegincmp(m_buffer.data() + token.id, token.id_length, field))
		{
			str_conv(m_buffer.data() + token.value, token.value_length, val);
			return true;
		}
	}

	LOG_WARN("Response", "get", "could not parse element \"%s\"", field);
	return false;
}

bool Response::get(const char* field, float &val) const
{
	for (json_size_t i = 0; i < m_count; ++i)
	{
		const json_token_t& token = m_tokens[i];
		if (token.id_length == 0 || token.value_length == 0)
			continue;

		if (token.type == json_type_t::JSON_PRIMITIVE &&
			strbegincmp(m_buffer.data() + token.id, token.id_length, field))
		{
			str_conv(m_buffer.data() + token.value, token.value_length, val);
			return true;
		}
	}

	LOG_WARN("Response", "get", "could not parse element \"%s\"", field);
	return false;
}

bool Response::get(const char* field, double &val) const
{
	for (json_size_t i = 0; i < m_count; ++i)
	{
		const json_token_t& token = m_tokens[i];
		if (token.id_length == 0 || token.value_length == 0)
			continue;

		if (token.type == json_type_t::JSON_PRIMITIVE &&
			strbegincmp(m_buffer.data() + token.id, token.id_length, field))
		{
			str_conv(m_buffer.data() + token.value, token.value_length, val);
			return true;
		}
	}

	LOG_WARN("Response", "get", "could not parse element \"%s\"", field);
	return false;
}
