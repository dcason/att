#pragma once

/**
 * Buffering configuration for R2000Client
 */

// USE_THREADED_SCAN_SIGNALER:
// Setting this to true results in another (fixed-size) buffer for scans
// being created to buffer delays in the scan received callback.
// Another thread is created as well.

#define USE_THREADED_SCAN_SIGNALER	true

// PP_MAX_PACKETS:
// The maximum number of packets to buffer (before parsing)
// Increasing the buffer size results in a fast increase of used up
// heap memory (max. ~1kb per packet)

#if USE_THREADED_SCAN_SIGNALER
#define PP_MAX_PACKETS 600
#else
#define PP_MAX_PACKETS 1800
#endif

// SS_MAX_BUFFER_SIZE:
// The maximum number of parsed scans to buffer (before handing them over to the callback).
// Only used in conjunction with (USE_THREADED_SCAN_SIGNALER).
// Buffer elements are small (size of a pointer)

#if USE_THREADED_SCAN_SIGNALER
#define SS_MAX_BUFFER_SIZE 2000
#endif