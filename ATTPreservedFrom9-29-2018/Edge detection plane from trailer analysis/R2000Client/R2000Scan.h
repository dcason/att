#ifndef R2000SCAN_H
#define R2000SCAN_H

#include <cstdint>
#include <memory>
#include <vector>
#include <functional>

#include "RuntimeDecls.h"

struct R2000Scan
{
	using Ptr = std::unique_ptr<R2000Scan>;
	struct Point
	{
		float angle;	// angle in radian
		float x;
		float y;
		float i;		// value range: [0,1]

		Point(float _angle, float _x, float _y, float _i) :  angle(_angle), x(_x), y(_y), i(_i) {}
	};

	struct TimeStamp
	{
		uint32_t secs;	// the number of seconds (since last R2000 boot)
		uint32_t nsecs;	// the number of nanoseconds (fractional part of ntp64 time stamp)

		TimeStamp(uint64_t stamp) 
			: secs(static_cast<uint32_t>(stamp >> 32)),
			nsecs(static_cast<uint32_t>(static_cast<uint32_t>(stamp) / float(UINT_MAX) * 1e9f)) 
		{}
	};

	uint32_t scanNumber;
	uint32_t packetNumber;
	TimeStamp timeStamp;

	std::vector<Point> points;

	R2000Scan(uint32_t pointCount, TimeStamp stamp)
		: timeStamp(std::move(stamp))
	{
		points.reserve(pointCount);
	}
};

using ScanCallback = std::function<void(R2000Scan::Ptr&&)>;

#endif // R2000SCAN_H
