#ifndef PACKETHEADER_H
#define PACKETHEADER_H

#include <cstdint>
#include "RTX64StandardIncludes.h"
#include "LogDeclsExt.h"

class PacketHeader
{
public:
	enum class HeaderPositions : uint32_t
	{
		magic = 0,
		packet_type = 2,
		packet_size = 4,
		header_size = 8,
		scan_number = 10,
		packet_number = 12,
		timestamp_raw = 14,
		timestamp_sync = 22,
		status_flags = 30,
		scan_frequency = 34,
		num_points_scan = 38,
		num_points_packet = 40,
		first_index = 42,
		first_angle = 44,
		angular_increment = 48,
		iq_input = 52,
		iq_overload = 56
	};

	enum class PacketType : uint16_t
	{
		A = 0x0041,						// distance only (uint32_t)
		B = 0x0042,						// distance (uint32_t) and amplitude (uint16_t, padded 12 bit)
		C = 0x0043,						// distance (20 bit unsigned int) and amplitude (12 bit unsigned int)
		Unknown
	};

	static const unsigned int min_buffer_size() { return 60; }
	static const float scan_frequency_factor() { return 1000.0f; }
	static const float angle_divisor() { return 10000.0f; }
	static const float amplitude_divisor() { return 4095.0f; }
	static const uint16_t magic_byte() { return 0xa25c; }

	inline static bool parsePacketSize(const char* buffer, uint32_t &packetSize)
	{
		uint16_t magicByte;
		memcpy(&magicByte, buffer + static_cast<uint32_t>(HeaderPositions::magic), sizeof(uint16_t));
		if (magicByte != magic_byte())
			return false;

		memcpy(&packetSize, buffer + static_cast<uint32_t>(HeaderPositions::packet_size), sizeof(uint32_t));
		return true;
	}

	bool parse(const char* buffer);

	uint16_t packetType() const { return m_packet_type; }
	uint32_t packetSize() const { return m_packet_size; }
	uint16_t headerSize() const { return m_header_size; }
	uint16_t scanNumber() const { return m_scan_number; }
	uint16_t packetNumber() const { return m_packet_number; }
	uint64_t timestamp() const { return m_timestamp_raw; }
	uint16_t numPointsScan() const { return m_num_points_scan; }
	uint16_t numPointsPacket() const { return m_num_points_packet; }
	uint16_t firstPointIndex() const { return m_first_index; }
	int32_t firstPointAngle() const { return m_first_angle; }

private:
	uint16_t m_packet_type;
	uint32_t m_packet_size;
	uint16_t m_header_size;
	uint16_t m_scan_number;
	uint16_t m_packet_number;
	uint64_t m_timestamp_raw;
	uint16_t m_num_points_scan;
	uint16_t m_num_points_packet;
	uint16_t m_first_index;
	int32_t m_first_angle;
};

#endif // PACKETHEADER_H
