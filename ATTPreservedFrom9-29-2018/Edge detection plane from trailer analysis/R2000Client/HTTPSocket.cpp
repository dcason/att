#include "HTTPSocket.h"
#include "R2000Config.h"
#include "LogDeclsExt.h"

#define HTTP_PORT "80"

namespace cmd
{
	static const char *base = "GET  /cmd/%s  HTTP/1.0\r\n\r\n";
	static const char *set_frequency = "GET  /cmd/set_parameter?scan_frequency=%i  HTTP/1.0\r\n\r\n";
	static const char *get_frequency = "GET  /cmd/get_parameter?list=scan_frequency  HTTP/1.0\r\n\r\n";
	static const char *get_samples_per_scan = "GET  /cmd/get_parameter?list=samples_per_scan  HTTP/1.0\r\n\r\n";
	static const char *set_samples_per_scan = "GET  /cmd/set_parameter?samples_per_scan=%i  HTTP/1.0\r\n\r\n";

	//static const char *request_handle = "GET  /cmd/request_handle_tcp?packet_type=%c&watchdog=off&start_angle=%i&max_num_points_scan=%i  HTTP/1.0\r\n\r\n";
	static const char *request_handle = "GET  /cmd/request_handle_tcp?packet_type=%c&watchdog=on&watchdogtimeout=%i&start_angle=%i&max_num_points_scan=%i  HTTP/1.0\r\n\r\n";

	static const char *release_handle = "GET  /cmd/release_handle?handle=%s  HTTP/1.0\r\n\r\n";
	static const char *start_scan_output = "GET  /cmd/start_scanoutput?handle=%s  HTTP/1.0\r\n\r\n";
	static const char *stop_scan_output = "GET  /cmd/stop_scanoutput?handle=%s  HTTP/1.0\r\n\r\n";

	static const char *get_device_infos = "GET  /cmd/get_parameter?list=vendor;product;serial;ip_address_current;subnet_mask_current;max_connections  HTTP/1.0\r\n\r\n";

	static const char *reboot = "GET  /cmd/reboot_device  HTTP/1.0\r\n\r\n";
	static const char *feed_watchdog = "GET  /cmd/feed_watchdog?handle=%s  HTTP/1.0\r\n\r\n";
}

namespace response
{
	static const char *scan_frequency = "scan_frequency";
	static const char *samples_per_scan = "samples_per_scan";

	static const char *port = "port";
	static const char *handle = "handle";

	static const char *vendor = "vendor";
	static const char *product = "product";
	static const char *serial = "serial";
	static const char *ip_address_current = "ip_address_current";
	static const char *subnet_mask_current = "subnet_mask_current";
	static const char *max_connections = "max_connections";
}

HTTPSocket::HTTPSocket(R2000Config& config)
	: m_config(config)
{
}

HTTPSocket::~HTTPSocket()
{
	closeSocket();
}

bool HTTPSocket::resolve()
{
	struct addrinfo *result = NULL, *ptr = NULL, hints;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	int res;
	if ((res = getaddrinfo(m_config.ipv4(), HTTP_PORT, &hints, &result)) != 0) 
	{
		LOG_ERROR("HTTPSocket", "resolve", "getaddrinfo failed with error (%d)\n", res);
		return false;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		m_httpSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (m_httpSocket == INVALID_SOCKET)
		{
			LOG_ERROR("HTTPSocket", "resolve", "socket failed with error (%d)\n", WSAGetLastError());
			return false;
		}

		// Connect to server.
		if (connect(m_httpSocket, ptr->ai_addr, static_cast<int>(ptr->ai_addrlen)) == SOCKET_ERROR)
		{
			m_httpSocket = INVALID_SOCKET;
			continue;
		}

		break;
	}

	freeaddrinfo(result);

	if (m_httpSocket == INVALID_SOCKET)
		LOG_ERROR("HTTPSocket", "resolve", "connecting to device at %s:%s failed with error (%d)\n",
			m_config.ipv4(), HTTP_PORT, WSAGetLastError());

	return m_httpSocket != INVALID_SOCKET;
}

bool HTTPSocket::waitForData(WaitType type)
{
	struct timeval timeout;
	fd_set fdSet;
	long errorCode;

	timeout.tv_sec = RCV_TIMEOUT / 1000;
	timeout.tv_usec = (RCV_TIMEOUT % 1000) * 1000;

	// Clear file descriptor set
	FD_ZERO(&fdSet);

	// Set the bit for network socket 'dataSock' in the descriptor set
	FD_SET(m_httpSocket, &fdSet);

	// Block until data is available on this socket, or iTimeOut milliseconds has passed
	if (type == WaitType::Receive)
		errorCode = select(1, &fdSet, NULL, NULL, &timeout);
	else // type == WaitType::Send
		errorCode = select(1, NULL, &fdSet, NULL, &timeout);

	return errorCode > 0;
}

long HTTPSocket::receive()
{
	if (!waitForData(WaitType::Receive))
	{
		LOG_INFO("HTTPSocket", "receive", "time out reached\n");
		return 0;
	}

	int bytesRead = recv(m_httpSocket, m_rcvBuffer.data(), RCV_BUFFER_SIZE, 0);
	if (bytesRead == 0)
	{
		LOG_INFO("HTTPSocket", "receive", "client disconnected\n");
		return 0;
	}
	else if (bytesRead == SOCKET_ERROR)
	{
		LOG_INFO("HTTPSocket", "receive", "failed with error (%d)\n", WSAGetLastError());
		return 0;
	}

	long totalReceived = bytesRead;
	while (totalReceived < RCV_BUFFER_SIZE)
	{
		bytesRead = recv(m_httpSocket, &m_rcvBuffer[totalReceived], RCV_BUFFER_SIZE - totalReceived, 0);
		if (bytesRead == 0)
			return totalReceived;

		if (bytesRead == SOCKET_ERROR)
		{
			LOG_INFO("HTTPSocket", "receive", "failed with error (%d)\n", WSAGetLastError());
			return false;
		}

		totalReceived += bytesRead;
	}

	return totalReceived;
}

bool HTTPSocket::closeSocket()
{
	if (m_httpSocket == INVALID_SOCKET)
		return true;

	bool result = true;
	if (shutdown(m_httpSocket, SD_BOTH) == SOCKET_ERROR)
	{
		LOG_INFO("HTTPSocket", "closeSocket", "socket shutdown failed with error (%d)\n", WSAGetLastError());
		result = false;
	}

	if (closesocket(m_httpSocket) == SOCKET_ERROR)
	{
		LOG_INFO("HTTPSocket", "closeSocket", "socket shutdown failed with error (%d)\n", WSAGetLastError());
		result = false;
	}

	m_httpSocket = INVALID_SOCKET;
	return result;
}

bool HTTPSocket::sendBuf(int cmdLength, const char* shortTitle)
{
	if (!resolve())
		return false;

	LOG_DEBUG("HTTPSocket", "send", "~~~~~~~~~~~~ BEGIN REQUEST ~~~~~~~~~~~~ \n");
	LOG_DEBUG("HTTPSocket", "send", "REQUEST: %s\n", m_sndBuffer);

	// Send an initial buffer
	int res = send(m_httpSocket, m_sndBuffer.data(), cmdLength, 0);
	if (res == 0)
	{
		LOG_DEBUG("HTTPSocket", "send", "~~~~~~~~~~~~ END REQUEST ~~~~~~~~~~~~ \n");
	
		closeSocket();
		LOG_ERROR("HTTPSocket", "send", "Error sending \"%s\": client disconnected\n", shortTitle);
		return false;
	}
	else if (res == SOCKET_ERROR) 
	{
		LOG_DEBUG("HTTPSocket", "send", "~~~~~~~~~~~~ END REQUEST ~~~~~~~~~~~~ \n");

		closeSocket();
		LOG_ERROR("HTTPSocket", "send", "Error sending \"%s\": socket error (%d)\n", shortTitle, WSAGetLastError());
		return false;
	}

	// Receive until the peer closes the connection
	res = receive();
	closeSocket();

	if (res == 0)
	{
		LOG_DEBUG("HTTPSocket", "send", "~~~~~~~~~~~~ END REQUEST ~~~~~~~~~~~~ \n");
		LOG_ERROR("HTTPSocket", "send", "Error sending \"%s\": client disconnected\n", shortTitle);
	}
	else if (res == SOCKET_ERROR)
	{
		LOG_DEBUG("HTTPSocket", "send", "~~~~~~~~~~~~ END REQUEST ~~~~~~~~~~~~ \n");
		LOG_ERROR("HTTPSocket", "send", "Error sending \"%s\": socket error (%d)\n", shortTitle, WSAGetLastError());
	}
	else
	{
		m_rcvBuffer[res] = '\0';

		LOG_DEBUG("HTTPSocket", "send", "RESPONSE:\n%s\n", m_rcvBuffer);
		LOG_DEBUG("HTTPSocket", "send", "~~~~~~~~~~~~ END REQUEST ~~~~~~~~~~~~ \n");

		if (!m_response.parse(m_rcvBuffer.data()))
			return false;

		if (!m_response.success())
		{
			const char* errorText = m_response.error();
			if (errorText)
				LOG_INFO("HTTPSocket", "send", "Error sending \"%s\". Received error text: \"%s\"\n", shortTitle, errorText);
			else
				LOG_INFO("HTTPSocket", "send", "Error sending \"%s\". Could not parse error text.\n", shortTitle);

			return false;
		}

		return true;
	}

	return false;
}

bool HTTPSocket::setFrequency()
{
	int length = sprintf_s(m_sndBuffer.data(), SEND_BUFFER_SIZE, cmd::set_frequency, m_config.frequency());
	return sendBuf(length, "set_parameter: scan_frequency");
}

bool HTTPSocket::getFrequency()
{
	int length = sprintf_s(m_sndBuffer.data(), SEND_BUFFER_SIZE, cmd::get_frequency);
	if (sendBuf(length, "get_parameter: scan_frequency"))
	{
		int frequency;
		if (m_response.get(response::scan_frequency, frequency))
		{
			m_config.frequency(frequency);
			return true;
		}
	}

	return false;
}

bool HTTPSocket::getDeviceInfo()
{
	int length = sprintf_s(m_sndBuffer.data(), SEND_BUFFER_SIZE, cmd::get_device_infos);
	if (sendBuf(length, "get_device_information"))
	{
		char vendor[16];
		char product[16];
		char serial[16];
		char ip_address_current[16];
		char subnet_mask_current[16];
		char max_connections[2];

		if (m_response.get(response::vendor, vendor, sizeof(vendor)) &&
			m_response.get(response::product, product, sizeof(product)) &&
			m_response.get(response::serial, serial, sizeof(serial)) &&
			m_response.get(response::ip_address_current, ip_address_current, sizeof(ip_address_current)) &&
			m_response.get(response::subnet_mask_current, subnet_mask_current, sizeof(subnet_mask_current)) &&
			m_response.get(response::max_connections, max_connections, sizeof(max_connections)))
		{
			char buf[512];
			sprintf_s(buf, sizeof(buf), "vendor: %s\nproduct: %s\nserial: %s\nip_address_current: %s\n"
				"subnet_mask_current: %s\nmax_connectons: %s\n",
				vendor, product, serial, 
				ip_address_current, subnet_mask_current, 
				max_connections);

			m_config.deviceInfo(buf);
			return true;
		}
	}

	return false;
}

bool HTTPSocket::getSamplesPerScan()
{
	int length = sprintf_s(m_sndBuffer.data(), SEND_BUFFER_SIZE, cmd::get_samples_per_scan);
	if (sendBuf(length, "get_parameter: samples_per_scan"))
	{
		int samples;
		if (m_response.get(response::samples_per_scan, samples))
		{
			m_config.samples(samples);
			return true;
		}
	}

	return false;
}

bool HTTPSocket::setSamplesperScan()
{
	int length = sprintf_s(m_sndBuffer.data(), SEND_BUFFER_SIZE, cmd::set_samples_per_scan, m_config.samples());
	return sendBuf(length, "set_parameter: samples_per_scan");
}

bool HTTPSocket::requestScanHandle()
{
	//"GET  /cmd/request_handle_tcp?packet_type=%c&watchdog=on&watchdogtimeout=%i&start_angle=%i&max_num_points_scan=%i  HTTP/1.0\r\n\r\n";
	int length = sprintf_s(m_sndBuffer.data(), SEND_BUFFER_SIZE, cmd::request_handle, 
		m_config.packetType(), R2000_WATCHDOG_TIMEOUT, m_config.startAngle() * 10000, m_config.maxNumPointsScan());

	if (sendBuf(length, "request_scan_handle"))
	{
		int port;
		char handle[17];
		if (m_response.get(response::port, port) &&
			m_response.get(response::handle, handle, sizeof(handle)))
		{
			m_config.port(port);
			m_config.scanHandle(handle);
			return true;
		}
	}
	
	return false;
}

bool HTTPSocket::startScanOutput()
{
	int length = sprintf_s(m_sndBuffer.data(), SEND_BUFFER_SIZE, cmd::start_scan_output, m_config.scanHandle());
	return sendBuf(length, "start_scanoutput");
}

bool HTTPSocket::stopScanOutput()
{
	int length = sprintf_s(m_sndBuffer.data(), SEND_BUFFER_SIZE, cmd::stop_scan_output, m_config.scanHandle());
	return sendBuf(length, "stop_scanoutput");
}

bool HTTPSocket::releaseScanHandle()
{
	int length = sprintf_s(m_sndBuffer.data(), SEND_BUFFER_SIZE, cmd::release_handle, m_config.scanHandle());
	return sendBuf(length, "release_scan_handle");
}

bool HTTPSocket::rebootDevice()
{
	int length = sprintf_s(m_sndBuffer.data(), SEND_BUFFER_SIZE, cmd::reboot);
	return sendBuf(length, "reboot_device");
}

bool HTTPSocket::feedWatchdog()
{
	int length = sprintf_s(m_sndBuffer.data(), SEND_BUFFER_SIZE, cmd::feed_watchdog, m_config.scanHandle());
	return sendBuf(length, "feed_watchdog");
}
