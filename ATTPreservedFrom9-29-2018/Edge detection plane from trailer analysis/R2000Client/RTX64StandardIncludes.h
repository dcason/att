#ifndef RTX64STANDARDINCLUDES_H
#define RTX64STANDARDINCLUDES_H

#include <SDKDDKVer.h>

#include <windows.h>
#include <tchar.h>
#include <rtapi.h>    // RTX64 APIs that can be used in real-time and Windows applications.

#ifdef UNDER_RTSS
#include <rtssapi.h>  // RTX64 APIs that can only be used in real-time applications.

#undef CreateEvent
#define CreateEvent				RtCreateEvent
#define SetEvent				RtSetEvent
#define PulseEvent				RtPulseEvent
#define ResetEvent				RtResetEvent
#define CloseHandle				RtCloseHandle
#define WaitForSingleObject		RtWaitForSingleObject
#define WaitForMultipleObjects	RtWaitForMultipleObjects
#define SetThreadPriority		RtSetThreadPriority

#endif // UNDER_RTSS

typedef VOID(RTFCNDCL *RTX64_TIMERPROC) (PVOID context);

#endif // RTX64STANDARDINCLUDES_H
