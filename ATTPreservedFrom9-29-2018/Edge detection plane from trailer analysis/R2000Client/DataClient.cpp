#include "DataClient.h"
#include "R2000Config.h"
#include "PacketParser.h"
#include "LogDeclsExt.h"

DataClient::DataClient(R2000Config& config, PacketParser& parser)
	: m_config(config),
	m_parser(parser)
{
	intitializeWinSock();
}

DataClient::~DataClient()
{
	SetEvent(m_receivingStoppedEvent);
	closeSocket();
	WSACleanup();
}

bool DataClient::intitializeWinSock()
{
	int res = WSAStartup(MAKEWORD(2, 2), &m_wsaData);
	if (res != 0) 
	{
		LOG_FATAL("DataClient", "init", "WSAStartup failed with error (%d)\n", res);
		return false;
	}

	return true;
}

bool DataClient::initReceiving()
{
	if (m_receiving)
	{
		LOG_INFO("DataClient", "initReceiving", "socket already receiving\n");
		return false;
	}

	m_receivingStoppedEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_stopReceivingEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	return true;
}

void DataClient::stopReceiving()
{
	if (m_receiving)
	{
		m_receiving = false;
		SetEvent(m_stopReceivingEvent);
		WaitForSingleObject(m_receivingStoppedEvent, INFINITE);
	}
	else
	{
		LOG_INFO("DataClient", "stopReceiving", "socket is not receiving\n");
	}

	CloseHandle(m_stopReceivingEvent);
	CloseHandle(m_receivingStoppedEvent);

	m_stopReceivingEvent = INVALID_HANDLE_VALUE;
	m_receivingStoppedEvent = INVALID_HANDLE_VALUE;
}

bool DataClient::closeSocket()
{
	if (m_dataSocket == INVALID_SOCKET)
		return true;

	bool result = true;
	if (shutdown(m_dataSocket, SD_BOTH) == SOCKET_ERROR)
	{
		LOG_INFO("DataClient", "closeSocket", "socket shutdown failed with error (%d)\n", WSAGetLastError());
		result = false;
	}

	if (closesocket(m_dataSocket) == SOCKET_ERROR)
	{
		LOG_INFO("DataClient", "closeSocket", "socket shutdown failed with error (%d)\n", WSAGetLastError());
		result = false;
	}

	m_dataSocket = INVALID_SOCKET;
	return result;
}

bool DataClient::receive()
{
	if (m_receiving)
	{
		LOG_INFO("DataClient", "receive", "already receiving\n");
		return false;
	}

	struct addrinfo *result = NULL, *ptr = NULL, hints;
	
	char port[24];
	sprintf_s(port, 24, "%d", m_config.port());

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	int res = getaddrinfo(m_config.ipv4(), port, &hints, &result);
	if (res != 0) 
	{
		LOG_ERROR("DataClient", "receive", "getaddrinfo failed with error (%d)\n", res);
		return false;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr; ptr = ptr->ai_next)
	{
		// Create a socket for connecting to server
		m_dataSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (m_dataSocket == INVALID_SOCKET) 
		{
			LOG_ERROR("DataClient", "receive", "socket failed with error (%d)\n", WSAGetLastError());
			return false;
		}

		// Connect to server
		res = connect(m_dataSocket, ptr->ai_addr, static_cast<int>(ptr->ai_addrlen));
		if (res == SOCKET_ERROR) 
		{
			closeSocket();
			continue;
		}

		break;
	}

	freeaddrinfo(result);

	if (m_dataSocket == INVALID_SOCKET)
	{
		LOG_ERROR("DataClient", "receive", "connecting to device at %s:%d failed with error (%d)\n",
			m_config.ipv4(), m_config.port(), WSAGetLastError());
		return false;
	}

	m_receiving = true;

	std::array<char, DATA_CLIENT_RCV_BUFFER_SIZE> recvbuf;
	uint32_t bytesRead = 0;
	uint32_t lastPacketSize = 0;

	do {
		res = recv(m_dataSocket, recvbuf.data() + bytesRead, 
			static_cast<uint32_t>(DATA_CLIENT_RCV_BUFFER_SIZE) - bytesRead, 0);
		
		if (res == 0)
		{
			if (m_receiving)
				LOG_ERROR("DataClient", "receive", "connection closed\n");
			
			break;
		}
		else if (res == SOCKET_ERROR)
		{
			if (m_receiving)
				LOG_ERROR("DataClient", "receive", "recv failed with error (%d)\n", WSAGetLastError());
			
			break;
		}
		else
		{
			bytesRead += static_cast<uint32_t>(res);
			if (bytesRead > PacketHeader::min_buffer_size())
			{
				if (lastPacketSize == 0)
				{
					if (!PacketHeader::parsePacketSize(recvbuf.data(), lastPacketSize))
					{
						bytesRead = 0;
						lastPacketSize = 0;
						LOG_WARN("DataClient", "receive", "missing magic byte, discarded buffer.\n");
						continue;
					}
				}

				if (bytesRead >= lastPacketSize)
				{
					m_parser.appendScan(recvbuf.data(), lastPacketSize);
					bytesRead -= lastPacketSize;
					if (bytesRead > 0)
						memmove(recvbuf.data(), recvbuf.data() + lastPacketSize, bytesRead);

					lastPacketSize = 0;
				}
			}
		}
	} while (WaitForSingleObject(m_stopReceivingEvent, 0));

	closeSocket();

	LOG_INFO("DataClient", "receive", "stopped receiving\n");
	return true;
}
