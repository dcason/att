#pragma once

#define LOG_SEV_DEBUG	0	// all information, such as request/response communication
#define LOG_SEV_INFO	1	// less verbose than LOG_SEV_DEBUG
#define LOG_SEV_WARN	2	// warnings
#define LOG_SEV_ERROR	3	// errors which potentially hinder program flow
#define LOG_SEV_FATAL	4	// fatal errors

// Modify the following values
#define NO_LOG_OUTPUT		false			// if true, log output is completely disabled
#define MIN_LOG_SEVERITY	LOG_SEV_INFO	// the minimum severity to generate logging output for
//

#if NO_LOG_OUTPUT
#define PrintMsg(...)		(void)0
#else
	#ifndef UNDER_RTSS
	#define RtPrintf		printf
	#endif
	#define PrintMsg(...)	RtPrintf(__VA_ARGS__)
#endif