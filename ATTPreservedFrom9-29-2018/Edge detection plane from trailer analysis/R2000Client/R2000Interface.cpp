#include "R2000Interface.h"
#include "LogDeclsExt.h"

R2000Interface::R2000Interface(ScanCallback scanCb, R2000Config config)
	: m_config(std::move(config)),
	m_http(m_config),
#if USE_THREADED_SCAN_SIGNALER
	m_signaler(std::move(scanCb)),
	m_parser(m_signaler),
#else
	m_parser(std::move(scanCb)),
#endif
	m_dataClient(m_config, m_parser)
{
}

R2000Interface::R2000Interface(ScanCallback scanCb, const std::string &ipv4)
	: m_config(),
	m_http(m_config),
#if USE_THREADED_SCAN_SIGNALER
	m_signaler(std::move(scanCb)),
	m_parser(m_signaler),
#else
	m_parser(std::move(scanCb)),
#endif
	m_dataClient(m_config, m_parser)
{
	m_config.ipv4(ipv4.c_str());
}

R2000Interface::~R2000Interface()
{
	disconnect();
}

bool R2000Interface::connect()
{
	if (m_connected)
		return false;

	m_parser.setStartAngle(m_config.startAngle());

	if (!m_http.setFrequency() ||
		!m_http.setSamplesperScan())
		return false;
	
	m_connected = true;
	return true;
}

bool R2000Interface::disconnect()
{
	if (!m_connected)
		return false;

	if (scanning())
		stopScanning();

	m_connected = false;

	LARGE_INTEGER timeRemaining;
	RtCancelTimer(m_watchDogTimer, &timeRemaining);
	RtDeleteTimer(m_watchDogTimer);
	m_watchDogTimer = INVALID_HANDLE_VALUE;

	return true;
}

bool R2000Interface::connected() const
{
	return m_connected;
}

bool R2000Interface::startScanning()
{
	if (m_scanning)
		return false;

	if (!m_http.requestScanHandle())
		return false;

	if (!m_http.startScanOutput())
	{
		m_http.releaseScanHandle();
		return false;
	}

	if (!m_dataClient.initReceiving())
	{
		m_http.stopScanOutput();
		m_http.releaseScanHandle();
		return false;
	}

	if (!m_parser.initParsing())
	{
		m_dataClient.stopReceiving();
		m_http.stopScanOutput();
		m_http.releaseScanHandle();
		return false;
	}

	if (!(m_watchDogTimer = RtCreateTimer(
		NULL,
		0,
		(RTX64_TIMERPROC)HTTPSocket::watchDogTimerHandler,
		&m_http,
		RT_PRIORITY_MAX,
		CLOCK_2)))
	{
		LOG_ERROR("R2000Interface", "startScanning", "Could not create the watchdog / RTAPI timer.\n");

		m_dataClient.stopReceiving();
		m_http.stopScanOutput();
		m_http.releaseScanHandle();

		LARGE_INTEGER timeRemaining;
		RtCancelTimer(m_watchDogTimer, &timeRemaining);
		RtDeleteTimer(m_watchDogTimer);
		return false;
	}

	LARGE_INTEGER timerPeriod;
	timerPeriod.QuadPart = WATCHDOG_TIMER_PERIOD;
	
	if (!RtSetTimerRelative(m_watchDogTimer, &timerPeriod, &timerPeriod))
	{
		LOG_ERROR("R2000Interface", "startScanning", "Could not set and start the watchdog / RTAPI timer.\n");

		m_dataClient.stopReceiving();
		m_http.stopScanOutput();
		m_http.releaseScanHandle();

		LARGE_INTEGER timeRemaining;
		RtCancelTimer(m_watchDogTimer, &timeRemaining);
		RtDeleteTimer(m_watchDogTimer);
		return false;
	}

#if USE_THREADED_SCAN_SIGNALER
	m_signaler.init();
#endif

	m_scanning = true;

	m_dataThread = CreateThread(NULL, 0,
		(LPTHREAD_START_ROUTINE)DataClient::staticThreadStart,
		(LPVOID)&m_dataClient, 0, NULL);

	m_parseThread = CreateThread(
		NULL, 0, 
		(LPTHREAD_START_ROUTINE)PacketParser::staticThreadStart, 
		(LPVOID)&m_parser, 0, NULL);
	
#if USE_THREADED_SCAN_SIGNALER
	m_signalingThread = CreateThread(
		NULL, 0,
		(LPTHREAD_START_ROUTINE)ScanSignaler::staticThreadStart,
		(LPVOID)&m_signaler, 0, NULL);
#endif

	

	LOG_INFO("R2000Interface", "startScanning", "Started scanning.\n");
	return true;
}

bool R2000Interface::stopScanning()
{
	if (!m_scanning)
		return false;

#if USE_THREADED_SCAN_SIGNALER
	m_signaler.stop();
#endif

	m_parser.stopParsing();
	m_dataClient.stopReceiving();
	
#if USE_THREADED_SCAN_SIGNALER
	WaitForSingleObject(m_signalingThread, INFINITE);
#endif
	WaitForSingleObject(m_parseThread, INFINITE);
	WaitForSingleObject(m_dataThread, INFINITE);

	CloseHandle(m_dataThread);
	CloseHandle(m_parseThread);

#if USE_THREADED_SCAN_SIGNALER
	CloseHandle(m_signalingThread);
#endif

	m_dataThread = INVALID_HANDLE_VALUE;
	m_parseThread = INVALID_HANDLE_VALUE;
#if USE_THREADED_SCAN_SIGNALER
	m_signalingThread = INVALID_HANDLE_VALUE;
#endif

	LARGE_INTEGER timeRemaining;
	RtCancelTimer(m_watchDogTimer, &timeRemaining);
	RtDeleteTimer(m_watchDogTimer);
	m_watchDogTimer = INVALID_HANDLE_VALUE;

	m_http.stopScanOutput();
	m_http.releaseScanHandle();
	m_scanning = false;

	LOG_INFO("R2000Interface", "stopScanning", "Stopped scanning.\n");
	return true;
}

bool R2000Interface::scanning() const
{
	return m_scanning;
}

bool R2000Interface::reboot()
{
	disconnect();
	return m_http.rebootDevice();
}