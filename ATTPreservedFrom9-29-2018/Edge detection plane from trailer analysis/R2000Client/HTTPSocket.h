#ifndef HTTPSOCKET_H
#define HTTPSOCKET_H

#include "RTX64WinsockIncludes.h"
#include "Response.h"
#include "HTTPDecls.h"

class R2000Config;
class HTTPSocket
{
public:
	HTTPSocket(R2000Config& config);
	~HTTPSocket();

	bool setFrequency();
	bool getFrequency();

	bool setSamplesperScan();
	bool getSamplesPerScan();

	bool requestScanHandle();
	bool releaseScanHandle();

	bool startScanOutput();
	bool stopScanOutput();
	
	bool getDeviceInfo();
	bool rebootDevice();

	bool feedWatchdog();

	static VOID RTFCNDCL watchDogTimerHandler(PVOID pHTTPContext)
	{
		HTTPSocket *httpSocket = static_cast<HTTPSocket*>(pHTTPContext);
		
		if (httpSocket != nullptr) 
			httpSocket->feedWatchdog();
		
		return;
	}

private:
	enum class WaitType { Send, Receive };

	R2000Config& m_config;
	Response m_response;
	SOCKET m_httpSocket = INVALID_SOCKET;
	std::array<char, SEND_BUFFER_SIZE> m_sndBuffer;
	std::array<char, RCV_BUFFER_SIZE> m_rcvBuffer;

	bool resolve();
	bool sendBuf(int cmdlength, const char* shortTitle);

	long receive();
	bool waitForData(WaitType type);
	bool closeSocket();
};

#endif // HTTPSOCKET_H
