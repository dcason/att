#include "ConversionFunctions.h"

#include <cctype>

// RTX64 (under RTSS) only supports a subset of the C runtime string conversion functions

template <typename T>
inline bool str_conv_real_impl(const char* str, std::size_t length, T& val)
{
	val = 0;
	std::size_t i{ 0 };
	bool inv;
	if ((inv = (*str == '-')))
	{
		if (length < 2)
			return false;

		++str;
		++i;
	}

	std::size_t parsedPoint{ 0 };
	for (; i < length; ++i, ++str)
	{
		if (*str >= '0' && *str <= '9')
			val = val * 10 + (*str - '0');
		else if (*str == '.')
			parsedPoint = i;
		else
			return false;
	}

	if (parsedPoint)
	{
		if (parsedPoint == length - 1)
			return false;

		val /= 10 * (length - parsedPoint - 1);
	}

	if (inv)
		val *= -1;

	return true;
}

inline bool str_conv(const char* str, std::size_t length, int& val)
{
	val = 0;
	std::size_t i{ 0 };
	bool inv;
	if ((inv = (*str == '-')))
	{
		if (length < 2)
			return false;

		++str;
		++i;
	}

	for (; i < length; ++i, ++str)
	{
		if (*str >= '0' && *str <= '9')
			val = val * 10 + (*str - '0');
		else
			return false;
	}

	if (inv)
		val *= -1;

	return true;
}

inline bool str_conv(const char* str, std::size_t length, unsigned int& val)
{
	val = 0;
	for (std::size_t i = 0; i < length; ++i, ++str)
	{
		if (*str >= '0' && *str <= '9')
			val = val * 10 + (*str - '0');
		else
			return false;
	}

	return true;
}

inline bool str_conv(const char* str, std::size_t length, float& val)
{
	return str_conv_real_impl(str, length, val);
}

inline bool str_conv(const char* str, std::size_t length, double& val)
{
	return str_conv_real_impl(str, length, val);
}
