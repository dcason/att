#include "RTX64StandardIncludes.h"
#include "R2000Config.h"
#include "LogDeclsExt.h"

#define CLAMP(minv, maxv, val) (((val) > (maxv)) ? (maxv) : (((val) < (minv)) ? (minv) : (val)))

#define MIN_FREQUENCY 10
#define MAX_FREQUENCY 50

#define MIN_STARTANGLE -180
#define MAX_STARTANGLE 179

static const int Samples[29] =
{
	72, 90, 120, 144, 180, 240, 360, 400, 450, 480, 600, 720,
	800, 900, 1200, 1440, 1800, 2400, 3600, 4200, 5040, 5600,
	6300, 7200, 8400, 10080, 12600, 16800, 25200
};

R2000Config::R2000Config()
	: m_ip({"127.0.0.1\0"}),
	m_scanHandle({'\0'}),
	m_deviceInfo({"<NOT SET>\0"})
{
}

const char* R2000Config::ipv4() const
{
	return m_ip.data();
}

void R2000Config::ipv4(const char* ip)
{
	std::size_t length = min(m_ip.size(), strlen(ip) + 1);
	if (length == 0)
		return;

	memcpy(m_ip.data(), ip, length);
	m_ip[length - 1] = '\0';
}

int R2000Config::port() const
{
	return m_port;
}

void R2000Config::port(int val)
{
	m_port = val;
}

const char* R2000Config::scanHandle()
{
	return m_scanHandle.data();
}

void R2000Config::scanHandle(const char* handle)
{
	std::size_t length = min(m_scanHandle.size(), strlen(handle) + 1);
	if (length == 0)
		return;

	memcpy(m_scanHandle.data(), handle, length);
	m_scanHandle[length - 1] = '\0';
}

R2000Config::ProtocolType R2000Config::protocol() const
{
	return m_protocol;
}

void R2000Config::protocol(ProtocolType type)
{
	m_protocol = type;
}

int R2000Config::frequency() const
{
	return m_frequency;
}

void R2000Config::frequency(int val)
{
	m_frequency = CLAMP(MIN_FREQUENCY, MAX_FREQUENCY, val);;
}

int R2000Config::samples() const
{
	return m_samples;
}

void R2000Config::samples(int val)
{
	if (val <= Samples[0])
	{		
		val = Samples[0];
	}
	else
	{
		for (std::size_t i = 1; i < sizeof(Samples)/sizeof(int); ++i)
		{
			if (Samples[i] >= val)
			{
				val = (Samples[i] - val < val - Samples[i - 1]) ? Samples[i] : Samples[i - 1];
				break;
			}
		}
	}

	m_samples = val;
}

void R2000Config::startAngle(const int val)
{
	m_startAngle = CLAMP(MIN_STARTANGLE, MAX_STARTANGLE, val);
}

int R2000Config::startAngle() const
{
	return m_startAngle;
}

int R2000Config::maxNumPointsScan() const
{
	return m_maxNumPointsScan;
}

void R2000Config::maxNumPointsScan(int val)
{
	m_maxNumPointsScan = CLAMP(0, INT_MAX, val);
}

R2000Config::PacketType R2000Config::packetType() const
{
	return m_packetType;
}

void R2000Config::packetType(PacketType type)
{
	m_packetType = type;
}

const char* R2000Config::deviceInfo() const
{
	return m_deviceInfo.data();
}

void R2000Config::deviceInfo(const char* val)
{
	std::size_t length = min(m_deviceInfo.size(), strlen(val) + 1);
	if (length == 0)
		return;

	memcpy(m_deviceInfo.data(), val, length);
	m_deviceInfo.data()[length - 1] = '\0';
}

void R2000Config::print() const
{
	PrintMsg("\n[R2000Config] Current configuration:\n"
		"\tip: %s\n"
		"\tport: %i\n"
		"\tprotocol: %s\n"
		"\tpacket type: %c\n"
		"\tfrequency: %i\n"
		"\tsamples: %i\n"
		"\tstart angle: %i\n"
		"\tmax points / scan: %i\n"
		"\tscan handle: %s\n"
		"\tdevice info: %s\n\n",
		m_ip.data(),
		m_port,
		m_protocol == ProtocolType::TCP ? "TCP" : "UDP",
		m_packetType,
		m_frequency,
		m_samples,
		m_startAngle,
		m_maxNumPointsScan,
		m_scanHandle.data(),
		m_deviceInfo.data());
}
