#ifndef SCANSIGNALER_H
#define SCANSIGNALER_H

#include "RuntimeDecls.h"

#if USE_THREADED_SCAN_SIGNALER

#include "R2000Scan.h"
#include "CircularBuffer.h"

class ScanSignaler
{
public:
	ScanSignaler(ScanCallback cb) : m_cb(std::move(cb)) {}

	void init();
	void stop();
	
	void append(R2000Scan::Ptr&& scan);

	static DWORD WINAPI staticThreadStart(PVOID pContext)
	{
		ScanSignaler* signaler = static_cast<ScanSignaler*>(pContext);
		signaler->start();
		SetEvent(signaler->m_stoppedEvent);
		return 0;
	}

private:
	class BufEntry
	{
	public:
		struct data_t
		{
			R2000Scan::Ptr scan;
			data_t(R2000Scan::Ptr&& _scan) : scan(std::forward<R2000Scan::Ptr>(_scan)) {}
		};

		R2000Scan::Ptr scan;
		
		inline bool copyFrom(data_t &data)
		{
			scan.swap(data.scan);
			return true;
		}

		inline void copyTo(BufEntry &entry)
		{
			entry.scan.swap(scan);
		}
	};

	bool m_signaling = false;
	HANDLE m_stopSignalingEvent = INVALID_HANDLE_VALUE;
	HANDLE m_dataAvailableEvent = INVALID_HANDLE_VALUE;
	HANDLE m_stoppedEvent = INVALID_HANDLE_VALUE;

	using Buffer = CircularBuffer<BufEntry, SS_MAX_BUFFER_SIZE>;
	Buffer m_buffer;
	ScanCallback m_cb;

	void start();
};

#endif // USE_THREADED_SCAN_SIGNALER
#endif // SCANSIGNALER_H
