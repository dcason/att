#ifndef PACKETPARSER_H
#define PACKETPARSER_H

#include <array>
#include <functional>

#include "RuntimeDecls.h"
#include "R2000Scan.h"
#include "PacketHeader.h"
#include "ScanSignaler.h"
#include "CircularBuffer.h"

#define PP_MAX_PACKET_SIZE 1500

class PacketParser
{
public:
#if USE_THREADED_SCAN_SIGNALER
	PacketParser(ScanSignaler& signaler) : m_signaler(signaler) {}
#else
	PacketParser(ScanCallback cb) : m_cb(std::move(cb)) {}
#endif

	bool initParsing();
	void stopParsing();

	void appendScan(const char* scanBuf, unsigned int size);
	void setStartAngle(int angle) { m_startAngle = angle; }

	static DWORD WINAPI staticThreadStart(PVOID pContext)
	{
		PacketParser* parser = static_cast<PacketParser*>(pContext);
		parser->parse();
		SetEvent(parser->m_parsingStoppedEvent);
		return 0;
	}

private:
	class BufEntry
	{
	public:
		struct data_t
		{
			const char* buf;
			unsigned int size;

			data_t(const char* _buf, unsigned int _size) : buf(_buf), size(_size) {}
		};

		unsigned int size = 0;
		inline char* buf() { return m_buffer.data(); }

		inline bool copyFrom(data_t &data)
		{
			if (size > PP_MAX_PACKET_SIZE)
			{
				LOG_ERROR("CircularBuffer", "push",
					"error trying to append too big buffer (%d bytes)\n", size);
				return false;
			}

			size = data.size;
			memcpy(buf(), data.buf, data.size);
			return true;
		}

		inline void copyTo(BufEntry &entry)
		{
			entry.size = size;
			memcpy(entry.buf(), buf(), size);
		}

	private:
		std::array<char, PP_MAX_PACKET_SIZE> m_buffer;
	};

#if USE_THREADED_SCAN_SIGNALER
	ScanSignaler& m_signaler;
#else
	ScanCallback m_cb;
#endif

	int m_startAngle = 0;

	HANDLE m_dataAvailableEvent = INVALID_HANDLE_VALUE;
	HANDLE m_stopParsingEvent = INVALID_HANDLE_VALUE;
	HANDLE m_parsingStoppedEvent = INVALID_HANDLE_VALUE;

	using Buffer = CircularBuffer<BufEntry, PP_MAX_PACKETS>;
	Buffer m_buffer;
	bool m_parsing = false;

	PacketParser(const PacketParser&) = delete;
	PacketParser& operator=(const PacketParser&) = delete;

	void parse();
	R2000Scan::Ptr parseScanTypeA(const PacketHeader& header, const char* buffer, uint32_t bufferSize) const;
	R2000Scan::Ptr parseScanTypeB(const PacketHeader& header, const char* buffer, uint32_t bufferSize) const;
};

#endif // PACKETPARSER_H
