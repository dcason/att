#ifndef DATA_CLIENT_H
#define DATA_CLIENT_H

#include "RTX64WinsockIncludes.h"

#define DATA_CLIENT_RCV_BUFFER_SIZE 16000

class R2000Config;
class PacketParser;

class DataClient
{
public:
	DataClient(R2000Config& config, PacketParser& parser);
	~DataClient();
	
	bool initReceiving();
	void stopReceiving();

	static DWORD WINAPI staticThreadStart(PVOID pContext)
	{
		DataClient* conn = static_cast<DataClient*>(pContext);
		bool res = conn->receive();
		SetEvent(conn->m_receivingStoppedEvent);
		return res ? 0 : 1;
	}

private:
	WSADATA	m_wsaData;
	R2000Config& m_config;
	PacketParser& m_parser;

	bool m_receiving = false;
	SOCKET m_dataSocket = INVALID_SOCKET;
	HANDLE m_receivingStoppedEvent = INVALID_HANDLE_VALUE;
	HANDLE m_stopReceivingEvent = INVALID_HANDLE_VALUE;

	DataClient(const DataClient&) = delete;
	DataClient& operator=(const DataClient&) = delete;

	bool intitializeWinSock();
	bool closeSocket();
	bool receive();
};

#endif // DATA_CLIENT_H