#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H

#include <cstddef>
#include <array>

#include "RTX64StandardIncludes.h"

enum class SignalerPushResult { Success, BufferFull, ErrorInEntry };

template <class EntryT, unsigned int MaxElems>
class CircularBuffer
{
public:
	CircularBuffer() { InitializeCriticalSection(&m_cs); }
	~CircularBuffer() { DeleteCriticalSection(&m_cs); }

	void reset();
	bool empty() const;

	SignalerPushResult push(typename EntryT::data_t &&data);
	bool pull(EntryT &entry, bool &bufferNowEmpty);

private:
	using Array = typename std::array<EntryT, MaxElems>;
	Array m_buffer;

	mutable CRITICAL_SECTION m_cs;
	unsigned int m_count = 0;
	typename Array::iterator m_head, m_tail;
};

template <class EntryT, unsigned int MaxElems>
SignalerPushResult CircularBuffer<EntryT, MaxElems>::push(typename EntryT::data_t &&data)
{
	EnterCriticalSection(&m_cs);

	if (m_count == m_buffer.size())
	{
		LeaveCriticalSection(&m_cs);
		return SignalerPushResult::BufferFull;
	}

	if (!m_head->copyFrom(data))
	{
		LeaveCriticalSection(&m_cs);
		return SignalerPushResult::ErrorInEntry;
	}

	if (++m_head == m_buffer.end())
		m_head = m_buffer.begin();

	++m_count;

	LeaveCriticalSection(&m_cs);
	return SignalerPushResult::Success;
}

template <class EntryT, unsigned int MaxElems>
bool CircularBuffer<EntryT, MaxElems>::pull(EntryT &entry, bool &bufferNowEmpty)
{
	EnterCriticalSection(&m_cs);

	if (m_count == 0)
	{
		LeaveCriticalSection(&m_cs);
		bufferNowEmpty = true;
		return false;
	}

	m_tail->copyTo(entry);
	if (++m_tail == m_buffer.end())
		m_tail = m_buffer.begin();

	bufferNowEmpty = (--m_count == 0);

	LeaveCriticalSection(&m_cs);
	return true;
}

template <class EntryT, unsigned int MaxElems>
void CircularBuffer<EntryT, MaxElems>::reset()
{
	EnterCriticalSection(&m_cs);

	m_count = 0;
	m_head = m_buffer.begin();
	m_tail = m_buffer.begin();

	LeaveCriticalSection(&m_cs);
}

template <class EntryT, unsigned int MaxElems>
bool CircularBuffer<EntryT, MaxElems>::empty() const
{
	EnterCriticalSection(&m_cs);

	unsigned int count = m_count;

	LeaveCriticalSection(&m_cs);
	return (count == 0);
}

#endif // CIRCULARBUFFER_H
