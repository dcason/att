//////////////////////////////////////////////////////////////////
//
// RtssApp1.h - header file
//
// This file was generated using the RTX64 Application Template for Visual Studio.
//
// Created: 6/11/2018 11:19:50 AM 
// User: dcason
//
//////////////////////////////////////////////////////////////////

#pragma once
//This define will deprecate all unsupported Microsoft C-runtime functions when compiled under RTSS.
//If using this define, #include <rtapi.h> should remain below all windows headers
//#define UNDER_RTSS_UNSUPPORTED_CRT_APIS

//#include <SDKDDKVer.h>

//#include <stdio.h>
//#include <string.h>
//#include <ctype.h>
//#include <conio.h>
//#include <stdlib.h>
//#include <math.h>
//#include <errno.h>
//#include <windows.h>
//#include <tchar.h>
//#include <rtapi.h>    // RTX64 APIs that can be used in real-time and Windows applications.

#ifdef UNDER_RTSS
//#include <rtssapi.h>  // RTX64 APIs that can only be used in real-time applications.
#endif // UNDER_RTSS
#pragma once

#ifndef XBEE_HARVESTER_DRIVER

#define XBEE_HARVESTER_DRIVER

//#include "XBee Harvester Driver.h"
#include <Windows.h>

#include <Rtapi.h>
#include <tchar.h>
#include <string>

#include <bitset>

#include <vector>
#include <ctime>
#include <iostream>

//#include <Windows.h>


//#include <mutex>
using namespace std;
//mutex writeToXbee;
//mutex outgoingSharedMemory;
//mutex incomingSharedMemory;
LPSECURITY_ATTRIBUTES mine;

HANDLE writeToXbeeMutex = RtCreateMutexW(mine, false, L"inUseMutex1");
HANDLE outgoingSharedMemory = RtCreateMutexW(mine, false, L"inUseMutex2");
HANDLE incomingSharedMemory = RtCreateMutexW(mine, false, L"inUseMutex3");
HANDLE mutexForEnquedUnion = RtCreateMutexW(mine, false, L"inUseMutex4");





//LPCWSTR outgoing = L"RawDataOutgoingThroughXBee";
#define SM_NAME_XBEE_OUTGOING  L"RawDataOutgoingThroughXBee"
#define SM_NAME_XBEE_INCOMING L"RawDataIncomingThroughXBee"
UCHAR theAddress = 0x2f8;
//PUCHAR test = &temp;
union currentAdressUnion

{

	PUCHAR formPUCHAR;                                

	WORD formWORD = 0x2f8;

};

union receivedUnion
{
	byte formByte;
	UCHAR formUCHAR;
};

receivedUnion currentReceivedUnion;
currentAdressUnion portAddressUnion;

BYTE *addressOfHarvesterLongForm[8];

BYTE *addressOfPaired16[2];



int timesRunCounter = 0;

int timeSincePairConfirm = 0;

bool harvesterIsPairedWithATractor = false;

struct messagePacket { BYTE magicByte; BYTE messageType; BYTE payload[20]; BYTE stopByte; };

messagePacket *receivedPacket = new messagePacket;



bool queryForPairs();

bool pairHarvesterToTractor();

bool pingTheTractor();

//bool resendLastSent();

bool isMessageTypeDriverReserved(messagePacket);

bool checkSharedOutgoing();

messagePacket pullFromSharedOutgoing();

bool pushToSharedMemory();

bool destroySharedIncoming();

bool destroySharedOutgoing();

bool sendByte(BYTE);

bool sendMessage(messagePacket);

WORD openAndConfigurePort();

bool checkIfIncoming();

bool receiveMessage();

BYTE receiveNextByte();



template<class messageDataType>														//Template declaration

bool enqueueMessageToSend(messageDataType messageToEnqueue, BYTE messageType);		//Function to push message into shared memory as a message packet



BYTE getNextByte()
{
	RtPrintf("Looking for next byte\n");
	while ((RtReadPortUchar(portAddressUnion.formPUCHAR + 5) & 0b00000001) == 0)
	{
		RtSleep(12);
	}
	currentReceivedUnion.formByte = RtReadPortUchar(portAddressUnion.formPUCHAR);
	return 0;
}





bool xbeeDriver()

{
//	RtPrintf("Inside xbeeDriver\n");
/*	if (timesRunCounter == 1000 && harvesterIsPairedWithATractor)

	{

		timesRunCounter = 0;

		pingTheTractor();

	}*/
//	printf("Checkpoint101\n");
	if (checkSharedOutgoing())

	{
//		printf("Checkpoint102\n");
		//		RtPrintf("shared outgoing detected\n");
		messagePacket outgoingPacket = pullFromSharedOutgoing();
//		printf("Check9\n");

//		printf("Checkpoint103\n");
		destroySharedOutgoing();
//		printf("Check10\n");
//		printf("Checkpoint104\n");

		sendMessage(outgoingPacket);
//		printf("Check11\n");

	}
//	printf("Checkpoint105\n");
	//	printf("Check12\n");

	bool thereIsIncoming = false;
	int x = 0;
	
	while (x < 10 && !thereIsIncoming)
	{
//		printf("Checkpoint106\n");
		if (checkIfIncoming())
		{
//			printf("Checkpoint107\n");
			thereIsIncoming = true;
		}
		++x;
	}
//	printf("Checkpoint108\n");

	if (thereIsIncoming)

	{
	//	RtPrintf("Incoming detected\n");
//		printf("Checkpoint109\n");
		if (receiveMessage())
		{
//			printf("Checkpoint110\n");
			RtPrintf("Message has been received\n");
		}
//		RtPrintf("Checkpoint 2");
//		printf("Checkpoint111\n");
		destroySharedIncoming();
	//	printf("Checkpoint112\n");

		pushToSharedMemory();

	}
//	printf("Checkpoint113\n");

	return 0;

}













bool checkSharedOutgoing()		//returns false if nothing waiting to be sent, otherwise returns true.

{
	//outgoingSharedMemory.lock();
	void *ptr;
//	RtPrintf("Inside Check shared outgoing\n");
	RtWaitForSingleObject(outgoingSharedMemory, 1000);
//	printf("Check\n");
	SetLastError(0x20000000L);
	DWORD temp = GetLastError();
	if (temp == WAIT_ABANDONED)
	{
		return false;
	}
	if (temp == WAIT_TIMEOUT)
	{
		return false;
	}
//	printf("Check2\n");

	HANDLE tempHandle;

	tempHandle = RtOpenSharedMemory(SHM_MAP_READ, false, SM_NAME_XBEE_OUTGOING, &ptr);
//	printf("Check5\n");
	if (temp == NULL)
	{
		RtPrintf("Null handle in checksharedoutgoing.\n");
		RtCloseHandle(tempHandle);
		return false;
	}
//	printf("Check6\n");
	//RtPrintf("Checksharedoutgoing checkpoint 2\n\n");;


	BYTE *myByte = static_cast<BYTE*>(ptr);
//	printf("Check7\n");



	for (int i = 0; i < 23; i++)

	{

		if (*(myByte + i) != 0)

		{
//			RtPrintf("myByte = %x and i = %i\n", myByte, i);
			myByte = NULL;

			ptr = NULL;

			delete myByte;

			delete ptr;
//			printf("Check8\n");

			return true;

		}

	}
//	printf("Check3\n");
	myByte = NULL;

	ptr = NULL;
//	delete myByte;
//	delete ptr;

	RtCloseHandle(tempHandle);
	RtReleaseMutex(outgoingSharedMemory);
//	printf("Check4\n");
	//outgoingSharedMemory.unlock();
	return false;

}



messagePacket pullFromSharedOutgoing()

{
	//outgoingSharedMemory.lock();
	void *ptr = NULL;
	RtWaitForSingleObject(outgoingSharedMemory, 1000);
	SetLastError(0x20000000L);
	DWORD temp = GetLastError();
	if (temp == WAIT_ABANDONED)
	{
		messagePacket temp;
		temp.magicByte = 0;
		RtPrintf("pullFromSharedOutgoing() WAIT_ABANDONED\n");
		return temp;
	}
	if (temp == WAIT_TIMEOUT)
	{
		messagePacket temp;
		temp.magicByte = 0;
		RtPrintf("pullFromSharedOutgoing() WAIT_TIMEOUT\n");
		return temp;
	}

	BYTE *tempArray[20];

	HANDLE tempHandle = RtOpenSharedMemory(SHM_MAP_READ, false, SM_NAME_XBEE_OUTGOING, &ptr);

	if (tempHandle == INVALID_HANDLE_VALUE)
	{
		messagePacket temp{ 0, 0, 0,0 };
		RtCloseHandle(tempHandle);
		return temp;
	}
	if (tempHandle == NULL)
	{
		messagePacket temp{ 0, 0, 0,0 };
		RtCloseHandle(tempHandle);
		return temp;

	}

	BYTE *myPtr = static_cast<BYTE*>(ptr);

	messagePacket myMessagePacket;



	myMessagePacket.magicByte = (*(myPtr + 0));

	myMessagePacket.messageType = (*(myPtr + 1));



	for (int i = 0; i < 20; i++)					//20 places is a arbitrary value for the limit of bytes

	{

		myMessagePacket.payload[i] = (*(myPtr + i + 2));
	//	RtPrintf("The value stored at place %i is: %u\n", (i + 2), (*(myPtr + i + 2)));
	}

	myMessagePacket.stopByte = (*(myPtr + 22));
	RtReleaseMutex(outgoingSharedMemory);
	RtCloseHandle(tempHandle);

	//outgoingSharedMemory.unlock();
//	delete tempArray;
//	delete myPtr;
	return myMessagePacket;

}





bool destroySharedOutgoing()

{
	//outgoingSharedMemory.lock();
	void *ptr = NULL;
	RtWaitForSingleObject(outgoingSharedMemory, 1000);
	SetLastError(0x20000000L);
	DWORD temp = GetLastError();
	if (temp == WAIT_ABANDONED)
	{
//		delete ptr;
		return false;
	}
	if (temp == WAIT_TIMEOUT)
	{
//		delete ptr;
		return false;
	}

	HANDLE myHANDLE = RtOpenSharedMemory(SHM_MAP_READ, false, SM_NAME_XBEE_OUTGOING, &ptr);

	if (myHANDLE = INVALID_HANDLE_VALUE)
	{
		RtCloseHandle(myHANDLE);
		return false;
	}
	if (myHANDLE = NULL)
	{
		RtCloseHandle(myHANDLE);
		return false;
	}


	BYTE *myByte = static_cast<BYTE*>(ptr);



	for (int i = 0; i < 23; i++)

	{

		*(myByte + i) = 0;

	}
	RtReleaseMutex(outgoingSharedMemory);
	RtCloseHandle(myHANDLE);

	//outgoingSharedMemory.unlock();
//	delete ptr;
//	delete myByte;
	return true;

}



bool destroySharedIncoming()

{
	printf("Checkpoint 8\n");
	RtWaitForSingleObject(incomingSharedMemory, 10);
	printf("Checkpoint 9\n");
	SetLastError(0x20000000L);
	printf("Checkpoint 10\n");
	DWORD temp = GetLastError();
	printf("Checkpoint 11\n");
	if (temp == WAIT_ABANDONED)
	{
		return false;
	}
	if (temp == WAIT_TIMEOUT)
	{
		return false;
	}
	void *ptr;
	printf("Checkpoint 12\n");

	HANDLE myHandle = RtOpenSharedMemory(SHM_MAP_READ, false, SM_NAME_XBEE_INCOMING, &ptr);

	if (myHandle == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	if (myHandle == NULL)
	{
		return false;
	}

	printf("Checkpoint 13\n");

	BYTE *myByte = static_cast<BYTE*>(ptr);


	printf("Checkpoint 14\n");

	for (int i = 0; i < 23; i++)

	{

		*(myByte + i) = 0;

	}
	RtCloseHandle(myHandle);
	printf("Checkpoint 15\n");

	RtReleaseMutex(incomingSharedMemory);
	printf("Checkpoint 16\n");

//	delete ptr;
//	delete myByte;
	printf("Checkpoint 17\n");

	return true;

}







template<class T>

union enqueueUnion			//Used to enable transferring of whole 

{


	T unionMessage;
	//std::bitset<(sizeof(unionMessage))> formBit160;
	BYTE formBit160[20];
	enqueueUnion() {

		formBit160[0] =  0 ;

	}

	enqueueUnion(enqueueUnion&) = default;

};



bool sendByte(BYTE toSend)

{
	RtSleep(1);

	while ((RtReadPortUchar(portAddressUnion.formPUCHAR + 6) & 0b00010000) == 0)

	{
		RtSleep(1);


	}

	//	PUCHAR myPtr = static_cast<PUCHAR>(CURRENT_PORT_ADDRESS);

	RtWritePortUchar(portAddressUnion.formPUCHAR, toSend);

	return true;
}



bool sendMessage(messagePacket toSend)

{

	BYTE checksum = 0;

//	sendByte(0x7E);		//Magic Byte

//	checksum += 0x7e;

//	sendByte(0x0);		//MSB size of frame(normally 0)

//	sendByte(0b00100101);		//LSB size of frame

//	checksum += 0b00100101;

//	sendByte(0x10);		//API command. Using simple transmission request.

//	checksum += 0x10;

	for (int i = 0; i < 8; i++)	//sending 64bit address

	{

//		sendByte(*(addressOfHarvesterLongForm[0] + i));

//		checksum += *(addressOfHarvesterLongForm[0] + i);

	}

//	sendByte(*(addressOfPaired16[0]));		//Sending 16bit address

//	checksum += *(addressOfPaired16[0]);

//	sendByte(*(addressOfPaired16[0] + 1));

//	checksum += *(addressOfPaired16[0] + 1);



//	sendByte(0);								//Setting frame options

//	sendByte(0x20);								//Setting frame options

//	checksum += 0x20;


//	printf("Check13\n");

	sendByte(toSend.magicByte);
//	printf("Check14\n");

//	checksum += toSend.magicByte;
//	printf("Check15\n");

	sendByte(toSend.messageType);
//	printf("Check16\n");

//	checksum += toSend.messageType;
//	RtPrintf("MagicByte = %u\n", toSend.magicByte);
//	RtPrintf("messageType = %u\n", toSend.messageType);
	for (int i = 0; i < 20; i++)

	{

		sendByte(toSend.payload[i]);
	//	RtPrintf("payload at [%i] = %u\n", i, toSend.payload[i]);
//		checksum += toSend.payload[i];

	}

	sendByte(toSend.stopByte);
//	RtPrintf("stopByte = %u\n", toSend.stopByte);
//	checksum += toSend.stopByte;

	return true;

}



WORD openAndConfigurePort()

{
//	RtPrintf("In open and configure port Mark 1\n");
	VOID *ptr;
	printf("Checkpoint 2");
	RtCreateSharedMemory(PAGE_READWRITE, 0, 40, SM_NAME_XBEE_INCOMING, &ptr);
	RtCreateSharedMemory(PAGE_READWRITE, 0, 40, SM_NAME_XBEE_OUTGOING, &ptr);
//	RtPrintf("In open and configure port Mark 2\n");
	printf("Checkpoint 3");
	if (!(destroySharedIncoming()))
	{
		RtPrintf("Failed to destroy shared incoming\n");
	}
	printf("Checkpoint 4");

	if (!(destroySharedOutgoing()))
	{
		RtPrintf("Failed to destroy shared outgoing\n");
	}
	printf("Checkpoint 5");
	bool statusa = RtEnablePortIo(portAddressUnion.formPUCHAR, 9);
	printf("Checkpoint 6");
	if (!statusa)
	{
		return 0;
	}

//	RtPrintf("\n\nThe byte received was: %u\n", getNextByte());
//	RtPrintf("\n\nThe byte received was: %u\n", getNextByte());
//	RtPrintf("\n\nThe byte received was: %u\n", getNextByte());
	RtWritePortUchar(portAddressUnion.formPUCHAR + 3, 0b00000011);		//Configure LCR and disable DLAB (bit 7)
	RtSleep(1);
	RtWritePortUchar(portAddressUnion.formPUCHAR + 1, 0b00000001);		//Disable all interrupts
	RtSleep(1);
	RtWritePortUchar(portAddressUnion.formPUCHAR + 3, 0b10000011);		//Enable DLAB to be able to set baud rate
	RtSleep(1);
	RtWritePortUchar(portAddressUnion.formPUCHAR + 0, 0b00000001);		//Setting Divisor Latch Low Byte
	RtSleep(1);
	RtWritePortUchar(portAddressUnion.formPUCHAR + 1, 0b00000000);		//Setting Divisor Latch High Byte
	RtSleep(1);
	RtWritePortUchar(portAddressUnion.formPUCHAR + 3, 0b00000011);		//Configure LCR and disable DLAB (bit 7)
	RtSleep(1);
	RtWritePortUchar(portAddressUnion.formPUCHAR + 2, 0b00000001);		//Configuring FIFO control. Setting the interrupt theshold as 14 bytes for testing
	RtSleep(1);
	RtWritePortUchar(portAddressUnion.formPUCHAR + 4, 0b00001011);		//Configure Modem control register
	RtSleep(1);
	printf("Checkpoint 7");
	//RtDisablePortIo(portAddressUnion.formPUCHAR, 8);
	return 1;
}



template<class messageDataType>

bool enqueueMessageToSend(messageDataType messageToEnqueue, BYTE messageType)

{

	RtWaitForSingleObject(mutexForEnquedUnion, 1000);
	SetLastError(0x20000000L);
	DWORD temp = GetLastError();
	if (temp == WAIT_ABANDONED)
	{
		return false;
	}
	if (temp == WAIT_TIMEOUT)
	{
		return false;
	}


	enqueueUnion<messageDataType> enqueuedUnion;
	for (int i = 0; i < 20; ++i)
	{
		enqueuedUnion.formBit160[i] = 0;
	}
	enqueuedUnion.unionMessage = messageToEnqueue;



	typedef struct

	{

		BYTE magicByte = 0xAD;
		BYTE messageType;
		//std::bitset<160> payload = enqueuedUnion.formBit160;
		BYTE payload[20];
		BYTE stopByte = 0xAA;

	}structFinalFormData;



	structFinalFormData *finalFormPointer = new structFinalFormData;

	void * toMap;

	LARGE_INTEGER size;



	size.QuadPart = sizeof(structFinalFormData);



	HANDLE tempHandle = RtOpenSharedMemory(SHM_MAP_WRITE, false, SM_NAME_XBEE_OUTGOING, &toMap);

	if (tempHandle == NULL)
	{
//		for(int i = 0; i < )

		RtCloseHandle(tempHandle);
		RtPrintf("temp is NULL.\n");
		return false;
	}
	finalFormPointer->magicByte = 0xAD;
	finalFormPointer->messageType = messageType;
	for (int i = 0; i < 20; ++i)
	{
		finalFormPointer->payload[i] = enqueuedUnion.formBit160[sizeof(enqueuedUnion.formBit160) - 1 -i];
	}
	finalFormPointer->stopByte = 0xAA;
	*static_cast<structFinalFormData*>(toMap) = *finalFormPointer;
//	RtPrintf("%u\n", finalFormPointer->magicByte);
//	RtPrintf("%u\n", static_cast<structFinalFormData*>(toMap)->magicByte);
	RtReleaseMutex(mutexForEnquedUnion);
	RtCloseHandle(tempHandle);
	toMap = NULL;
	delete toMap;
	delete finalFormPointer;
	return true;

}



bool checkIfIncoming()

{
	UCHAR temp = (RtReadPortUchar(portAddressUnion.formPUCHAR + 5));
	//RtSleep(10);
	if ((static_cast<byte>(temp) & 0b0000001) == 0)

	{
		//RtPrintf("%x\n", (RtReadPortUchar(portAddressUnion.formPUCHAR + 4) & 0b0000001));
		//RtPrintf("No incoming\n");
		return false;

	}

	else

	{
	//	RtPrintf("Incoming detected\n");
		return true;

	}

}



bool receiveMessage()

{
	receivedPacket->magicByte = 0;
/*	bool status = RtEnablePortIo(portAddressUnion.formPUCHAR, 8);
	if (status == false)
	{
		RtPrintf("Fail\n");
	}
	if (status == true)
	{
		RtPrintf("Success\n");
	}*/

//	RtPrintf("In receiveMessage mark 10\n");
//	BYTE temp = (getNextByte());
//	RtPrintf("In receiveMessage mark 3\n");
//	receivedPacket->magicByte = (getNextByte());
//	RtPrintf("In receiveMessage mark 2\n");
	int tries = 0;
	
	while ((receivedPacket->magicByte != 0xAD) && (tries != 1000))

	{

		receivedPacket->magicByte = receiveNextByte();		//This will clear the buffer until it finds a start byte
	//	RtPrintf("%c\n", receivedPacket->magicByte);
		++tries;

	}



	if (tries == 1000)

	{
//		RtPrintf("Failed to find Magic Byte \n");
		return false;

	}
//	RtPrintf("About to receive packet type\n");
	receivedPacket->messageType = receiveNextByte();
//	RtPrintf("Have received packlet type\n");
	for (int i = 0; i < 20; i++)
	{
//		RtPrintf("About to receive the %i'th payload byte\n", i);
		receivedPacket->payload[i] = receiveNextByte();

	}
//	RtPrintf("About to receive the stop byte\n");
	receivedPacket->stopByte = receiveNextByte();

	return true;

}



BYTE receiveNextByte()

{

	while ((RtReadPortUchar(portAddressUnion.formPUCHAR + 5) & 0b0000001) < 1)

	{

		RtSleep(1);


	}

	return RtReadPortUchar(portAddressUnion.formPUCHAR);

}



bool pushToSharedMemory()

{

	void * ptr;

	RtWaitForSingleObject(outgoingSharedMemory, 1000);
	SetLastError(0x20000000L);
	DWORD temp = GetLastError();
	if (temp == WAIT_ABANDONED)
	{
		return false;
	}
	if (temp == WAIT_TIMEOUT)
	{
		return false;
	}



	HANDLE myHandle = RtOpenSharedMemory(SHM_MAP_READ, false, SM_NAME_XBEE_INCOMING, &ptr);

	if (myHandle == INVALID_HANDLE_VALUE)
	{
		RtCloseHandle(myHandle);
		return false;
	}
	if (myHandle == NULL)
	{
		RtCloseHandle(myHandle);
		return false;
	}

	BYTE *myPtr = static_cast<BYTE*>(ptr);



	*myPtr = receivedPacket->messageType;

	for (int i = 0; i < 20; i++)

	{

		*myPtr = receivedPacket->payload[i];

	}

	RtReleaseMutex(mutexForEnquedUnion);
	RtCloseHandle(myHandle);

//	delete ptr;
//	delete myPtr;

	return true;



}



bool queryForPairs()

{

	messagePacket queryPacket;

	queryPacket.magicByte = 0xAD;

	queryPacket.messageType = 0xFD;

	queryPacket.payload[20] = 0;

	queryPacket.stopByte = 0xAA;

	return sendMessage(queryPacket);



}



bool pairHarvesterToTractor()

{

	*addressOfPaired16[0] = 0;

	*addressOfPaired16[1] = receivedPacket->payload[1];



	messagePacket pairPacket;



	pairPacket.magicByte = 0xAD;

	pairPacket.messageType = 0xFC;

	for (int i = 0; i < 20; i++)

	{

		pairPacket.payload[20] = 0;

	}

	pairPacket.stopByte = 0xAA;

	timesRunCounter = 0;

	return sendMessage(pairPacket);

}



bool isMessagedTypeDriverReserved()

{

	switch (receivedPacket->messageType)

	{

	case 0xFE:

		//		if (harvesterIsPairedWithATractor)

		//		{

		timeSincePairConfirm = 0;

		//		}

		return 1;

		break;

	case 0xFD:

		harvesterIsPairedWithATractor = pairHarvesterToTractor();

		return 1;

		break;

	case 0xFC:

		harvesterIsPairedWithATractor = false;

		return 1;

		break;

	case 0xFB:

		pingTheTractor();

		return 1;

		break;

	case 0xFA:

		//		resendLastSent();

		return 1;

		break;



		return 0;

	}

}



bool pingTheTractor()

{

	messagePacket pingMessage;

	pingMessage.magicByte = 0xAD;

	pingMessage.messageType = 0xFB;

	for (int i = 0; i < 20; i++)

	{

		pingMessage.payload[i] = 0;

	}

	pingMessage.stopByte = 0XAA;

	return sendMessage(pingMessage);

}

#endif
 
