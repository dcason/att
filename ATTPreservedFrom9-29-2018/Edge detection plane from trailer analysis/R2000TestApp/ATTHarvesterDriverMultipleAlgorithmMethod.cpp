//////////////////////////////////////////////////////////////////
//
// R2000TestApp.cpp
//
// Sample usage for the R2000Client library
//
//////////////////////////////////////////////////////////////////

#include <cmath>
#include <queue>
#include <deque>
#include <iterator>
#include <unordered_map>
#include <ctime>

#include "RuntimeDecls.h"
#include "LogDecls.h"
#include "R2000Interface.h"
#include "RtssApp1.h"

#define PACKET_VECTOR_RESERVE		11000
#define PACKET_VECTOR_CLEAR_AT_SIZE 10000
#define PI 3.14159265

float lowestPoint = 0;
float highestPoint = 0;
float mostForwardPoint = 0;
float mostBackwardPoint = 0;

using namespace std;

/********************************************************************************************************************************************************/
/********************************************************************************************************************************************************/
/****************************************************************Variables****************************************************************************************/
/********************************************************************************************************************************************************/
/********************************************************************************************************************************************************/

struct Points 
{
	int scanID = -1;
	vector <float> distances;

	Points() = default;
	Points(int _scanID, float distance) : scanID(_scanID) { distances.push_back(distance); }
};


// [Philipp]: Added a critical section here, since there were race conditions (shared memory access from 2 threads)
// from filterReflectiveIntensity() [main thread] and received(...) [a different thread in R2000Interface]
CRITICAL_SECTION criticalSection;
vector<R2000Scan::Ptr> tempHoldPackets;
vector<R2000Scan::Ptr> holdPackets;

unordered_map<int, Points> leftEdge150;
unordered_map<int, Points> rightEdge150;

//vector to hold distance averages per scan for last 150 scans
float trueAverageOfOutput = 0;
bool edgeFound = true;
float error = 0.0;
deque<float> averageOfOutputs;
R2000Scan::Point lastRightEdge{0,0,0,0};
R2000Scan::Point lastLeftEdge{ 0,0,0 ,0};
vector<R2000Scan::Point> rightEdgeReflectivity;
vector<R2000Scan::Point> leftEdgeReflectivity;
vector<R2000Scan::Point> possibleLeftEdge;
vector<R2000Scan::Point> possibleRightEdge;
float distanceThreshold = 1219.2;
bool initialEdgesFound = false;
vector<float> AveragePerScanLeftEdge;
vector<float> AveragePerScanRightEdge;
bool validAverage = false;
bool validError = false;
float averageOfLast500Outputs = 0;
deque<float> last500Outputs;
float Kp = 22;
float Ki = 22;
float Kd = 16;
float gainModifierMod = .03;
float maxChange = 3.0;
float output = 0;
float outputLast = 0;
float leftChangeWeight = 1; //A weight applied to the last amount of change on the left side of the tractor in validateAverage ::DMC::
float leftMaxChange = 4; //The maximum amount of change we will accept on the left side of the tractor ::DMC::
float rightChangeWeight = 1; //A weight applied to the last amount of change on the right side of the tractor in validateAverage ::DMC::
float rightMaxChange = 4; //The maximum amount of change we will accept on the right side of the tractor ::DMC::
float lastError = 0;
//vectors to hold accumulated average Distance from last 150 scans in feet (conversion from mm to feet
vector<float> RunningLeftTrueDistancePer150Scans;
vector<float> RunningRightTrueDistancePer150Scans;
vector<float> RunningError;
vector<float> storageOfPastRunningRightTrueDistancePer150Scans[10];
vector<float> storageOfPastRunningLeftTrueDistancePer150Scans[10];

//keep counter to keep track of how many times we dont see valid Distances
int lostModeRunCounter = 0;
bool enteredLostMode = false;

BYTE voltage = 0;
BYTE voltageCarry = 0;
float voltagePrecision = 0.0;
void received(R2000Scan::Ptr&&);

vector<R2000Scan::Point> allPoints;
vector<R2000Scan::Point> beneathLidarPoints;

float finalUnmodifiedOutput = 0;


/********************************************************************************************************************************************************/
/********************************************************************************************************************************************************/
/********************************************************************************************************************************************************/
/********************************************************************************************************************************************************/
/********************************************************************************************************************************************************/


float rangeOfChange = 22.86;	//controls the distance that the edges can drift between scans in each direction. 22.86 millimeters equates to 6 mph difference in speed
R2000Scan::Point tractor{ 0,0,0,0};
R2000Scan::Point edgeOnePoint{ 0,0,0,0 };
R2000Scan::Point edgeTwoPoint{ 0,0,0,0 };
R2000Scan::Point edgeThreePoint{ 0,0,0,0 };
R2000Scan::Point edgeFourPoint{ 0,0,0,0 };

bool overFirstTrailer = false;
bool overSecondTrailer = false;
bool overGap = false;

float localizedPosition = 0;
bool localizeAccomplished = false;
#define HALFWAY_SECOND_TRAILER 14630

float groundBasedLocalizationChosenPosition = 0;

float mostAheadReflectivePointBasedPosition = 0;

float errorInFeet = 0;

float theGoalPoint = (HALFWAY_SECOND_TRAILER);

bool groundBasedLocalization(vector<R2000Scan::Point> &toFind);
bool mostAheadReflictivePointBased(vector<R2000Scan::Point> &toFind);
void infillControl();
bool initialFixFound = false;
bool positionsInitiated = false;

deque<float> positionsFirstTrailer;	//All distances will be based off of the millimeters from the tractor

deque<float> positionsSecondTrailer;


float lengthOfFirstTrailer = 6710.8;
float lengthOfSecondTrailer = 6500;
float lengthOfTractorToTrailerOne = 3357.2;
float lengthOfGap = 2189.2;

int numberOfStepsToFull = 6;	//Should be as few as possible while still being smooth enough to fill the trailer. Keep an EVEN number. Never odd.
float percentageofHeightToFill = 100; //This marks how high the tomatoes should fill to. 100 is the height of the trailer edges

int timesReadFilledRequired = 2;				//How many times the consecutive filled count must be true to signal point filled
int consecutiveTimesReadFilledRequired = 2;
int pointsPerTrailer = 25;

float safetyFillDistanceFromTheEdge = 914.4; //Minimum mm to be from the edge. This avoids spilling tomatoes over the sides

//////deque<float> positionsFirstTrailer;	//All distances will be based off of the millimeters from the tractor
//////
//////deque<float> positionsSecondTrailer;

bool secondTrailerCompleted = false;

bool goingBack = true;

int currentPoint = 0;

float atGoalPointThreshold = 700;
float currentLidarPosition = 0;
float goalPoint = 0;
float goalFillHeight = 800;
float depthOfTrailer = 1200;

int currentPositionReference = 0;
int currentLayerNumber = 1;

bool done = false;

bool firstFirstTrailerDone = false;
bool firstTrailerComplete = false;

bool tooFar = false;

bool goneTooFar();

int whichTrailerAtStart();


void checkpointN(int n)
{
	printf("Checkpoint\t%i\n", n);
}

/*Function will take all samples collected so far in Lidar_Sensor->holdPackets vector and filter them out based
on the value of each points light intensity, value needs to be >= 0.23 to be valid point
**/

float findDistance(R2000Scan::Point positionOne, R2000Scan::Point positionTwo)
{
	float temp = 0;

	temp = sqrt(((positionOne.x - positionTwo.x) * (positionOne.x - positionTwo.x)) + ((positionOne.y - positionTwo.y) * (positionOne.y - positionTwo.y)));

	return temp;
}

float findDistanceUsingOnlyX(R2000Scan::Point positionOne, R2000Scan::Point positionTwo)
{
	float temp = 0;

	temp = abs(positionOne.x - positionTwo.x);

	return temp;
}

bool jumpDetection(vector<R2000Scan::Point> toFind)
{
	R2000Scan::Point previousPoint{ 0, 0, 0,0 };
	vector<R2000Scan::Point> possiblePairs;						//Pairs of points that could represent edges. Should always be an even number
	int distanceTraveledRightSide = 0;
	int distanceTraveledLeftSide = 0;
	for (int i = 0; i < toFind.size(); i++) {		//Iterate over every member of holdpackets
		if ((findDistance(toFind[i], previousPoint)) > 619.2)	//If distance is greater than threshold (edge detection)
			{
				if (toFind[i].y > 0 && toFind[i].y < 7315.2)	//If point is on left side but still less than trailer length
				{
					float temp = findDistance(toFind[i], previousPoint);
					distanceTraveledLeftSide = static_cast<int>(temp);
					if (previousPoint.y < toFind[i].y)
					{
						possibleLeftEdge.push_back(previousPoint);						//store point as possible edge
																						//						PrintMsg("Possible edge 1\n");
					}
					else
					{
						possibleLeftEdge.push_back(toFind[i]);						//store point as possible edge
																									//						PrintMsg("Possible edge 2\n");
					}
				}
				else if (toFind[i].y < 0 && toFind[i].y > -7315.2)	//If point is on right side but still less than trailer length
				{
					float temp = findDistance(toFind[i], previousPoint);
					distanceTraveledRightSide = static_cast<int>(temp);

					if (previousPoint.y < toFind[i].y)
					{
						possibleRightEdge.push_back(toFind[i]);						//store point as possible edge
																									//						PrintMsg("Possible edge 3\n");
					}
					else
					{
						possibleRightEdge.push_back(previousPoint);						//store point as possible edge
																						//						PrintMsg("Possible edge 4\n");
					}
				}
			}
			previousPoint = toFind[i];
		}
	/*	if (possibleLeftEdge.size() == 0 || possibleRightEdge.size() == 0)
	{
	holdPackets.clear();
	possibleLeftEdge.clear();
	possibleRightEdge.clear();
	return;

	}*/
	if (initialEdgesFound)
	{
		if (possibleLeftEdge.size() > 0 && possibleRightEdge.size() > 0)
		{
			for (int i = 0; i < possibleLeftEdge.size(); ++i)
			{
				for (int j = 0; j < possibleRightEdge.size(); ++j)
				{
					if (findDistance(possibleRightEdge[j], possibleLeftEdge[i]) >= 6705.6 && findDistance(possibleRightEdge[j], possibleLeftEdge[i]) <= 7924.8)
					{
						possiblePairs.push_back(possibleLeftEdge[i]);
						possiblePairs.push_back(possibleRightEdge[j]);
					}
				}
			}
		}
		if (possiblePairs.size() == 0)
		{
			return false;
		}
		/*		if (possiblePairs.size() == 0)
		{
		for (int i = 0; i < possibleLeftEdge.size(); i++)
		{
		if (abs(abs(possibleLeftEdge[i].y) - abs(lastLeftEdge.y)) < 40)
		{
		if (abs(abs(possibleLeftEdge[i].x) - abs(lastLeftEdge.x)) < 20)
		{
		error += abs(abs(possibleLeftEdge[i].y) - abs(lastLeftEdge.y));
		PrintMsg("Single edge found\n");
		PrintMsg("Number of possible left side edges:\t%i\n", possibleLeftEdge.size());
		PrintMsg("Number of possible right side edges:\t%i\n", possibleRightEdge.size());
		PrintMsg("Distance traveled right side:\t%i\n", distanceTraveledRightSide);
		PrintMsg("Distance traveled left side:\t%i\n", distanceTraveledLeftSide);
		edgeFound = true;

		return;
		}
		}
		}

		for (int i = 0; i < possibleRightEdge.size(); i++)
		{
		if (abs(abs(possibleRightEdge[i].y) - abs(lastRightEdge.y)) < 40)
		{
		if (abs(abs(possibleRightEdge[i].x) - abs(lastRightEdge.x)) < 20)
		{
		error += abs(abs(possibleLeftEdge[i].y) - abs(lastLeftEdge.y));
		PrintMsg("Single edge found\n");
		PrintMsg("Number of possible left side edges:\t%i\n", possibleLeftEdge.size());
		PrintMsg("Number of possible right side edges:\t%i\n", possibleRightEdge.size());
		PrintMsg("Distance traveled right side:\t%i\n", distanceTraveledRightSide);
		PrintMsg("Distance traveled left side:\t%i\n", distanceTraveledLeftSide);
		edgeFound = true;
		return;

		}
		}
		return;
		*/


		vector<double>distances;
		for (int i = 0; i < possiblePairs.size(); i += 2)
		{
			distances.push_back((findDistance(possiblePairs[i], possiblePairs[i + 1])));
		}
		double bestDistance = 10000;
		int bestDistanceIndex = 0;
		for (int i = 0; i < distances.size(); i++)
		{
			if (abs(abs(distances[i]) - 7315.2) <= bestDistance)
			{
				bestDistance = distances[i];
				bestDistanceIndex = i;
			}
		}
		float rightEdge = possiblePairs[bestDistanceIndex * 2].y;
		float leftEdge = possiblePairs[(bestDistanceIndex * 2) + 1].y;
		error = rightEdge * -1;	//Right edge will always be negative so make it positive
		PrintMsg("Edges found\n");
		PrintMsg("Number of possible left side edges:\t%i\n", possibleLeftEdge.size());
		PrintMsg("Number of possible right side edges:\t%i\n", possibleRightEdge.size());
		PrintMsg("Distance traveled right side:\t%i\n", distanceTraveledRightSide);
		PrintMsg("Distance traveled left side:\t%i\n", distanceTraveledLeftSide);
		edgeFound = true;
	}
	else
	{
		if (possibleLeftEdge.size() > 0 && possibleRightEdge.size() > 0)
		{
			for (int i = 0; i < possibleLeftEdge.size(); ++i)
			{
				for (int j = 0; j < possibleRightEdge.size(); ++j)
				{
					if (findDistance(possibleRightEdge[j], possibleLeftEdge[i]) >= 6705.6 && findDistance(possibleRightEdge[j], possibleLeftEdge[i]) <= 7924.8)
					{
						if (possibleRightEdge[j].x > -2500 && possibleLeftEdge[i].x > -2500)
						{
							PrintMsg("Test checkpoint\n");
							if (abs(abs(possibleRightEdge[j].x) - abs(possibleLeftEdge[i].x)) < 401)	//Checking vertical distance of points
							{
								possiblePairs.push_back(possibleLeftEdge[i]);
								possiblePairs.push_back(possibleRightEdge[j]);
							}
						}
					}
				}
			}

		}
		if (possiblePairs.size() == 0)
		{
			//This will run the edge detection off of a single edge. This will be done by looking for the x value as well as looking
			//At the change in the y value.

			holdPackets.clear();
			possibleLeftEdge.clear();
			possibleRightEdge.clear();
			PrintMsg("No edges found2\n");
			return false;
		}

		vector<double> distances;
		for (int i = 0; i < possiblePairs.size(); i += 2)
		{
			distances.push_back((findDistance(possiblePairs[i], possiblePairs[i + 1])));
		}
		double bestDistance = 10000;
		int bestDistanceIndex = 0;
		for (int i = 0; i < distances.size(); i++)
		{
			if (abs(abs(distances[i]) - 7315.2) <= bestDistance)
			{
				bestDistance = distances[i];
				bestDistanceIndex = i;
			}
		}
		float rightEdge = possiblePairs[bestDistanceIndex * 2].y;
		float leftEdge = possiblePairs[(bestDistanceIndex * 2) + 1].y;
		error = rightEdge * -1;	//Right edge will always be negative so make it positive
		PrintMsg("Edges found\n");
		PrintMsg("Number of possible left side edges:\t%i\n", possibleLeftEdge.size());
		PrintMsg("Number of possible right side edges:\t%i\n", possibleRightEdge.size());
		PrintMsg("Distance traveled right side:\t%i\n", distanceTraveledRightSide);
		PrintMsg("Distance traveled left side:\t%i\n", distanceTraveledLeftSide);
		edgeFound = true;
		initialEdgesFound = true;
		lastRightEdge = possiblePairs[bestDistanceIndex * 2];
		lastLeftEdge = possiblePairs[(bestDistanceIndex * 2) + 1];
	}
}




/*
This function fins where the edge is given a vector of points and returns a single point chosen to represent the edge
TODO: find the std dev of the points height and use this to filter based on z score to eliminate random floating points or debree
*/
R2000Scan::Point findEdge(vector<R2000Scan::Point> toFind)
{
	deque<R2000Scan::Point> topFiveHeight;
	float averageOfHeight;
	float averageOfLateral;
	int numOfPoints = toFind.size();
	for (int i = 0; i < toFind.size(); i++)
	{
		if (topFiveHeight.size() >= 5)
		{
			if (toFind[i].x < topFiveHeight[4].x)	//Making this check first lowers loop time
			{
				if (toFind[i].x < topFiveHeight[3].x)
				{
					if (toFind[i].x < topFiveHeight[2].x)
					{
						if (toFind[i].x < topFiveHeight[1].x)
						{
							if (toFind[i].x < topFiveHeight[0].x)
							{
								topFiveHeight[0] = toFind[i];
							}
							else
							{
								topFiveHeight[1] = toFind[i];
							}
						}
						else
						{
							topFiveHeight[2] = toFind[i];
						}
					}
					else
					{
						topFiveHeight[3] = toFind[i];
					}
				}
				else
				{
					topFiveHeight[4] = toFind[i];
				}
			}
		}
		else
		{	//Since the points dont have to be in any particular order we can just push them to the front of the conatiner
			topFiveHeight.push_back(toFind[i]);
		}
	}
	for (int i = 0; i < topFiveHeight.size(); i++)
	{
		averageOfHeight += topFiveHeight[i].x / topFiveHeight.size();	//Finding average height of top five points
		averageOfLateral += topFiveHeight[i].y / topFiveHeight.size();	//Finding average lateral of top five points
	}
	R2000Scan::Point temp{0, 0, 0,0};
	temp.x = averageOfHeight;
	temp.y = averageOfLateral;
	return temp; //The location of the chosen edge
}

bool findEdgesInitial(vector<R2000Scan::Point>& toFind)
{
	float maxYValue = 0;
	float maxXValue = 0;
	float minYValue = 1000000;
	float minXValue = 1000000;
	overFirstTrailer = false;
	overSecondTrailer = false;
	overGap = false;
	vector<R2000Scan::Point> possibleTractor;
	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].y > 1000 && abs(toFind[i].x) < 201)
		{
			possibleTractor.push_back(toFind[i]);
			if (toFind[i].x > maxXValue)
			{
				maxXValue = toFind[i].x;
			}
			if (abs(toFind[i].y) > maxYValue)
			{
				maxYValue = toFind[i].y;
			}
			if (toFind[i].x < minXValue)
			{
				minXValue = toFind[i].x;
			}
			if (toFind[i].y < minYValue)
			{
				minYValue = toFind[i].y;
			}
		}
	}
	if (possibleTractor.size() == 0)
	{
		return false;
	}
	float centerY = maxYValue - minYValue;
	float centerX = maxXValue - minXValue;
	float bestAngle = 0;
	int indexOfBestAngle = 0;
	for (int i = 0; i < possibleTractor.size(); i++)
	{
		possibleTractor[i].x = possibleTractor[i].x - centerX;
		possibleTractor[i].y = possibleTractor[i].y - centerY;
		float testAngle = sqrt((possibleTractor[i].x * possibleTractor[i].x) + (possibleTractor[i].y * possibleTractor[i].y));
		testAngle = atan(possibleTractor[i].y / possibleTractor[i].x) * 180 / PI;
		if (abs(testAngle - 45) < abs(bestAngle - 45))		//Finding point closest to chosen angle
		{
			indexOfBestAngle = i;
			bestAngle = testAngle;
		}
	}											//Theoretically the tractor will have been found. From here find a group of points for each of the four edges
	vector<R2000Scan::Point> edgeOne;
	vector<R2000Scan::Point> edgeTwo;
	vector<R2000Scan::Point> edgeThree;
	vector<R2000Scan::Point> edgeFour;

	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].y < (possibleTractor[indexOfBestAngle].y - 1500) && toFind[i].y >(possibleTractor[indexOfBestAngle].y - 3500))//Lateral position analysis for edge one
		{	//Need to review the .pcd files for the vertical difference between tractor and the tops of trailer edges
			edgeOne.push_back(toFind[i]);
		}//Need to run analysis on the edge itself before finding the others. This way the floating distance problem is eliminated.
		 //With the floating distance problem in place the range of the final edge can be as wide as 6 meters(assumed with unkown trailer lengths
	}
	if (edgeOne.size() > 0)
	{
		edgeOnePoint = findEdge(edgeOne);//Finding the first edge
	}

	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].y < (edgeOnePoint.y - 6000) && toFind[i].y >(edgeOnePoint.y - 7500))
		{
			edgeTwo.push_back(toFind[i]);
		}
	}
	if (edgeTwo.size() > 0)
	{
		edgeTwoPoint = findEdge(edgeTwo);
	}
	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].y < (edgeTwoPoint.y - 1500) && toFind[i].y >(edgeTwoPoint.y - 2500))
		{
			edgeThree.push_back(toFind[i]);
		}
	}
	if (edgeThree.size() > 0)
	{
		edgeThreePoint = findEdge(edgeThree);
	}

	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].y < (edgeThreePoint.y - 6000) && toFind[i].y >(edgeThreePoint.y - 7500))
		{
			edgeFour.push_back(toFind[i]);
		}
	}
	if (edgeFour.size() > 0)
	{
		edgeFourPoint = findEdge(edgeFour);
	}
	tractor = possibleTractor[indexOfBestAngle];
	//	cout << "The chosen point for the tractor is: " << possibleTractor[indexOfBestAngle].x << " " << possibleTractor[indexOfBestAngle].y << endl;
	//	cout << "The chosen point for the first edge is: " << edgeOnePoint.x << " " << edgeOnePoint.y << endl;
	//	cout << "The chosen point for the second edge is: " << edgeTwoPoint.x << " " << edgeTwoPoint.y << endl;
	//	cout << "The chosen point for the third edge is: " << edgeThreePoint.x << " " << edgeThreePoint.y << endl;
	//	cout << "The chosen point for the fourth edge is: " << edgeFourPoint.x << " " << edgeFourPoint.y << endl;
	if (edgeOnePoint.y > 0 && edgeTwoPoint.y < 0)	//Signifies that lidar is over front trailer
	{
		//		cout << "The lidar is over the front trailer and it is " << abs(edgeTwoPoint.y) << " millimeters from the back.\n";
		overFirstTrailer = true;
	}
	else if (edgeThreePoint.y > 0 && edgeFourPoint.y < 0)	//Signifies that lidar is over back trailer
	{
		//		cout << "The lidar is over the back trailer and it is " << abs(edgeFourPoint.y) << " millimeters from the back.\n";
		overSecondTrailer = true;
	}
	else if (edgeTwoPoint.y > 0 && edgeThreePoint.y < 0)	//Signifies that the lidar is between the trailers
	{
		//		cout << "The lidar is between the trailer and it is " << abs(edgeThreePoint.y) << " millimeters from the third edge.\n";
		overGap = true;
	}
	return true;
}


/*
This function will try to find the five points of interests. If less than five are found it will
base the positioning off of the ones that it has found.
If none could be found it will return false.
*/
bool findEdgesAfterInitial(vector<R2000Scan::Point> toFind)
{
	vector<R2000Scan::Point> tractorVec;
	vector<R2000Scan::Point> edgeOne;
	vector<R2000Scan::Point> edgeTwo;
	vector<R2000Scan::Point> edgeThree;
	vector<R2000Scan::Point> edgeFour;
	R2000Scan::Point oldTractor {0, 0, 0,0};
	R2000Scan::Point oldEdgeOnePoint{ 0,0,0,0};
	R2000Scan::Point oldEdgeTwoPoint{ 0,0,0,0};
	R2000Scan::Point oldEdgeThreePoint{ 0,0,0,0};
	R2000Scan::Point oldEdgeFourPoint{ 0,0,0,0};
	bool tractorFound = false;
	bool oneFound = false;
	bool twoFound = false;
	bool threeFound = false;
	bool fourFound = false;


	overFirstTrailer = false;
	overSecondTrailer = false;
	overGap = false;


	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].y < (tractor.y + rangeOfChange) && toFind[i].y >(tractor.y - rangeOfChange))
		{
			tractorVec.push_back(toFind[i]);
		}
	}
	if (tractorVec.size() > 0)
	{
		oldTractor = tractor;
		tractor = findEdge(tractorVec);
		tractorFound = true;
	}
	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].y < (edgeOnePoint.y + rangeOfChange) && toFind[i].y >(edgeOnePoint.y - rangeOfChange))
		{
			edgeOne.push_back(toFind[i]);
		}
	}
	if (edgeOne.size() > 0)
	{
		oldEdgeOnePoint = edgeOnePoint;
		edgeOnePoint = findEdge(edgeOne);
		oneFound = true;
	}
	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].y < (edgeTwoPoint.y + rangeOfChange) && toFind[i].y >(edgeTwoPoint.y - rangeOfChange))
		{
			edgeTwo.push_back(toFind[i]);
		}
	}
	if (edgeTwo.size() > 0)
	{
		oldEdgeTwoPoint = edgeTwoPoint;
		edgeTwoPoint = findEdge(edgeTwo);
		twoFound = true;
	}
	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].y < (edgeThreePoint.y + rangeOfChange) && toFind[i].y >(edgeThreePoint.y - rangeOfChange))
		{
			edgeThree.push_back(toFind[i]);
		}
	}
	if (edgeThree.size() > 0)
	{
		oldEdgeThreePoint = edgeThreePoint;
		edgeThreePoint = findEdge(edgeThree);
		threeFound = true;
	}
	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].y < (edgeFourPoint.y + rangeOfChange) && toFind[i].y >(edgeFourPoint.y - rangeOfChange))
		{
			edgeFour.push_back(toFind[i]);
		}
	}
	if (edgeFour.size() > 0)
	{
		oldEdgeFourPoint = edgeFourPoint;
		edgeFourPoint = findEdge(edgeFour);
		fourFound = true;
	}
	if (!tractorFound)
	{
		if (oneFound)
		{
			tractor.x = tractor.x + (edgeOnePoint.x - oldEdgeOnePoint.x);
			tractor.y = tractor.y + (edgeOnePoint.y - oldEdgeOnePoint.y);
		}
		else if (twoFound)
		{
			tractor.x = tractor.x + (edgeTwoPoint.x - oldEdgeTwoPoint.x);
			tractor.y = tractor.y + (edgeTwoPoint.y - oldEdgeTwoPoint.y);

		}
		else if (threeFound)
		{
			tractor.x = tractor.x + (edgeThreePoint.x - oldEdgeThreePoint.x);
			tractor.y = tractor.y + (edgeThreePoint.y - oldEdgeThreePoint.y);

		}
		else if (fourFound)
		{
			tractor.x = tractor.x + (edgeFourPoint.x - oldEdgeFourPoint.x);
			tractor.y = tractor.y + (edgeFourPoint.y - oldEdgeFourPoint.y);

		}
	}
	if (!oneFound)
	{
		if (tractorFound)
		{
			edgeOnePoint.x = edgeOnePoint.x + (tractor.x - oldTractor.x);
			edgeOnePoint.y = edgeOnePoint.y + (tractor.y - oldTractor.y);
		}
		else if (twoFound)
		{
			edgeOnePoint.x = edgeOnePoint.x + (edgeTwoPoint.x - oldEdgeTwoPoint.x);
			edgeOnePoint.y = edgeOnePoint.y + (edgeTwoPoint.y - oldEdgeTwoPoint.y);

		}
		else if (threeFound)
		{
			edgeOnePoint.x = edgeOnePoint.x + (edgeThreePoint.x - oldEdgeThreePoint.x);
			edgeOnePoint.y = edgeOnePoint.y + (edgeThreePoint.y - oldEdgeThreePoint.y);
		}
		else if (fourFound)
		{
			edgeOnePoint.x = edgeOnePoint.x + (edgeFourPoint.x - oldEdgeFourPoint.x);
			edgeOnePoint.y = edgeOnePoint.y + (edgeFourPoint.y - oldEdgeFourPoint.y);
		}
	}
	if (!twoFound)
	{
		if (tractorFound)
		{
			edgeTwoPoint.x = edgeTwoPoint.x + (tractor.x - oldTractor.x);
			edgeTwoPoint.y = edgeTwoPoint.y + (tractor.y - oldTractor.y);
		}
		else if (oneFound)
		{
			edgeTwoPoint.x = edgeTwoPoint.x + (edgeOnePoint.x - oldEdgeOnePoint.x);
			edgeTwoPoint.y = edgeOnePoint.y + (edgeOnePoint.y - oldEdgeOnePoint.y);
		}
		else if (threeFound)
		{
			edgeTwoPoint.x = edgeTwoPoint.x + (edgeThreePoint.x - oldEdgeThreePoint.x);
			edgeTwoPoint.y = edgeTwoPoint.y + (edgeThreePoint.y - oldEdgeThreePoint.y);
		}
		else if (fourFound)
		{
			edgeTwoPoint.x = edgeTwoPoint.x + (edgeFourPoint.x - oldEdgeFourPoint.x);
			edgeTwoPoint.y = edgeTwoPoint.y + (edgeFourPoint.y - oldEdgeFourPoint.y);
		}
	}
	if (!threeFound)
	{
		if (tractorFound)
		{
			edgeThreePoint.x = edgeThreePoint.x + (tractor.x - oldTractor.x);
			edgeThreePoint.y = edgeThreePoint.y + (tractor.y - oldTractor.y);
		}
		else if (oneFound)
		{
			edgeThreePoint.x = edgeThreePoint.x + (edgeOnePoint.x - oldEdgeOnePoint.x);
			edgeThreePoint.y = edgeThreePoint.y + (edgeOnePoint.y - oldEdgeOnePoint.y);
		}
		else if (twoFound)
		{
			edgeThreePoint.x = edgeThreePoint.x + (edgeTwoPoint.x - oldEdgeTwoPoint.x);
			edgeThreePoint.y = edgeThreePoint.y + (edgeTwoPoint.y - oldEdgeTwoPoint.y);
		}
		else if (fourFound)
		{
			edgeThreePoint.x = edgeThreePoint.x + (edgeFourPoint.x - oldEdgeFourPoint.x);
			edgeThreePoint.y = edgeThreePoint.y + (edgeFourPoint.y - oldEdgeFourPoint.y);
		}
	}
	if (!fourFound)
	{
		if (tractorFound)
		{
			edgeFourPoint.x = edgeFourPoint.x + (tractor.x - oldTractor.x);
			edgeFourPoint.y = edgeFourPoint.y + (tractor.y - oldTractor.y);
		}
		else if (oneFound)
		{
			edgeFourPoint.x = edgeFourPoint.x + (edgeOnePoint.x - oldEdgeOnePoint.x);
			edgeFourPoint.y = edgeFourPoint.y + (edgeOnePoint.y - oldEdgeOnePoint.y);
		}
		else if (twoFound)
		{
			edgeFourPoint.x = edgeFourPoint.x + (edgeTwoPoint.x - oldEdgeTwoPoint.x);
			edgeFourPoint.y = edgeFourPoint.y + (edgeTwoPoint.y - oldEdgeTwoPoint.y);
		}
		else if (threeFound)
		{
			edgeFourPoint.x = edgeFourPoint.x + (edgeThreePoint.x - oldEdgeThreePoint.x);
			edgeFourPoint.y = edgeFourPoint.y + (edgeThreePoint.y - oldEdgeThreePoint.y);
		}
	}
	if (!tractorFound && !oneFound && !twoFound && !threeFound && !fourFound)
	{
		return false;
	}
	if (edgeOnePoint.y > 0 && edgeTwoPoint.y < 0)	//Signifies that lidar is over front trailer
	{
//		cout << "The lidar is over the front trailer and it is " << abs(edgeTwoPoint.y) << " millimeters from the back.\n";
		overFirstTrailer = true;
	}
	else if (edgeThreePoint.y > 0 && edgeFourPoint.y < 0)	//Signifies that lidar is over back trailer
	{
//		cout << "The lidar is over the back trailer and it is " << abs(edgeFourPoint.y) << " millimeters from the back.\n";
		overSecondTrailer = true;
	}
	else if (edgeTwoPoint.y > 0 && edgeThreePoint.y < 0)	//Signifies that the lidar is between the trailers
	{
//		cout << "The lidar is between the trailer and it is " << abs(edgeThreePoint.y) << " millimeters from the third edge.\n";
		overGap = true;
	}
	return true;
}


R2000Scan::Point reflectiveTractorPoint{0,0,0,0};

//Function tries to find the tape on the tractor.
//Returns true if found, and false if not.
bool findTractorTape(vector<R2000Scan::Point> toFind)
{
	vector<R2000Scan::Point> pointsThatFellOnTape;
	bool found = false;
	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].x < 50 && toFind[i].y > 1000)	//this narrows the range of possible findings to those in the fourth quadrant
		{
			if (toFind[i].i > 0.7)
			{
				pointsThatFellOnTape.push_back(toFind[i]);
				found = true;
			}
		}
	}
	float chosenX = 0;
	float chosenY = 0;
	for (int i = 0; i < pointsThatFellOnTape.size(); i++)
	{
		chosenX += pointsThatFellOnTape[i].x / pointsThatFellOnTape.size();
		chosenY += pointsThatFellOnTape[i].y / pointsThatFellOnTape.size();
	}
	if (pointsThatFellOnTape.size() > 0)
	{
		reflectiveTractorPoint.x = chosenX;
		reflectiveTractorPoint.y = chosenY;
		printf("reflectiveTractorPointFound: %f, \t%f", chosenX, chosenY);
	}
	return found;
}


//This function sorts a YBin by the x value so that we have a nice ordered container of points
void sortYBin(vector<R2000Scan::Point> &toFind, int left, int right)
{
	if (left >= right)
	{
		return;
	}

	float pivot = toFind[right].x;

	int cnt = left;

	for (int i = left; i <= right; i++)
	{
		if (toFind[i].x <= pivot)
		{
			swap(toFind[cnt], toFind[i]);
			cnt++;
		}
	}
	sortYBin(toFind, left, cnt - 2);	//Recursiveness. Need to analyze time spent on sorting and possibly switch to merge sort for time stability
	sortYBin(toFind, cnt, right);
}

vector <double> containerOfStdDevs;
bool checkIfTenHighest(double toCheck)
{
	for (int i = 0; i < containerOfStdDevs.size(); i++)
	{
		if (toCheck > containerOfStdDevs[i])
		{
			vector<double>::iterator it = containerOfStdDevs.begin() + i;
			containerOfStdDevs.insert(it, toCheck);
			if (containerOfStdDevs.size() > 10)
			{
				containerOfStdDevs.pop_back();
			}
			return true;
		}
	}
}


vector<vector<R2000Scan::Point>> yBins;
//Function will seperate the entire point cloud into y bins and look for those whose standard deviations of x changes are too large
void xChangeAnalysis(vector<R2000Scan::Point> toFind)
{
	yBins.clear();	//Get rid of the last iteratiion of bins
	vector<R2000Scan::Point> temp;
	struct stdDevContainer
	{
		vector<double> stdDevs;
		vector<int> indexReference;
		vector<double> conversionHolder;
		vector<double> zScores;
	} stdDevReferenceContainer;
	for (int i = 0; i < 2363; ++i)	//Resolution of one inch
	{
		yBins.push_back(temp);
	}
	
	vector<int> listOfPossibleSides;	//A reference to which bins represent a possible side. This will be paired up against 
	double meanOfYBin;
	for (int i = 0; i < toFind.size(); i++)	//putting each point into its respective y bin
	{
		int binToFill = (int)(((toFind[i].y)+30000)/25.4);		//Must put data into the positive first
		yBins[binToFill].push_back(toFind[i]);
	}
	for (int i = 0; i < yBins.size(); i++)			//This for loop calculates the std deviation for each bin. If a bin has less than 2 points it is ignored
	{
		if (yBins[i].size() != 0)
		{
			sortYBin(yBins[i], 0, yBins.size() - 1);
			meanOfYBin = 0;
			vector<double> simpleXChanges;
			for (int j = 0; j < yBins[i].size(); j++)
			{
//				if (j != 0)	//do not run the first because we are only looking at the change
				{
//					meanOfYBin += (abs((yBins[i][j].x) - (yBins[i][j - 1].x))) / (yBins.size() - 1);	//Finding the simple mean of x changes
					meanOfYBin += abs(yBins[i][j].x) / (yBins.size() - 1);	//Finding the simple mean of x changes
//					simpleXChanges.push_back(abs((yBins[i][j].x) - (yBins[i][j - 1].x)));
				}
			}
			if (meanOfYBin != 0)	//there are at least two points in the bin
			{
				vector<double> squaredXChangesMinusMean;
				for (int j = 0; j < yBins[i].size(); j++)
				{
//					double temp = simpleXChanges[j] - meanOfYBin;
					double temp = abs(yBins[i][j].x - meanOfYBin);		//Try to keep in the positive
					temp = temp * temp;
					squaredXChangesMinusMean.push_back(temp);
				}
				double finalMean;
				for (int j = 0; j < squaredXChangesMinusMean.size(); j++)
				{
					finalMean += (squaredXChangesMinusMean[j] / squaredXChangesMinusMean.size());
				}
				stdDevReferenceContainer.conversionHolder.push_back(0);
				stdDevReferenceContainer.indexReference.push_back(i);
				stdDevReferenceContainer.stdDevs.push_back(sqrt(finalMean));				//This creates a paired storage such that the vectors are linked
			}
		}
	}
	//Next we need to find the std deviation of the deviations to allow us to calculate the z score for each bin
	double simpleMean = 0;
	double meanOfSquared = 0;
	for (int i = 0; i < stdDevReferenceContainer.stdDevs.size(); i++)
	{
		simpleMean += stdDevReferenceContainer.stdDevs[i] / stdDevReferenceContainer.stdDevs.size();
	}
	for (int i = 0; i < stdDevReferenceContainer.stdDevs.size(); i++)
	{
		stdDevReferenceContainer.conversionHolder[i] = (stdDevReferenceContainer.stdDevs[i] - simpleMean) * (stdDevReferenceContainer.stdDevs[i] - simpleMean);
	}
	for (int i = 0; i < stdDevReferenceContainer.stdDevs.size(); i++)
	{
		meanOfSquared += stdDevReferenceContainer.conversionHolder[i] / stdDevReferenceContainer.conversionHolder.size();
	}
	double stdDevOfAllNonZeroBins = sqrt(meanOfSquared);
	for (int i = 0; i < stdDevReferenceContainer.stdDevs.size(); i++)
	{
		stdDevReferenceContainer.zScores.push_back((stdDevReferenceContainer.stdDevs[i] - simpleMean)/stdDevOfAllNonZeroBins);
	}
	//TODO: find the 10 highest bins by their stdDeviation
}


vector<int> yValuesSeen;

struct reflectiveObjectsContainer {
	vector<vector<R2000Scan::Point>> possibleReflectiveLocations;		//This will be split up into two inch wide segments that allow for grouping of points based on the y value
	vector<int> positionalTranslationMapping;
}reflectiveStructureContainer;

int currentPosition = 0;

int isReflectiveLocationFound(R2000Scan::Point toFind)
{
	int yScore = -1;

	yScore = int(toFind.y / 50.8);

	for (int i = 0; i < reflectiveStructureContainer.possibleReflectiveLocations.size(); i++)
	{
		for (int j = 0; j < reflectiveStructureContainer.possibleReflectiveLocations[i].size(); j++)
		{
			if (int((reflectiveStructureContainer.possibleReflectiveLocations[i][j].y) /50.8) == yScore)
			{
//				yValuesSeen.push_back(yScore);
//				vector<int> temp;
//				temp.push_back(currentPosition);
//				reflectiveStructureContainer.positionalTranslationMapping.push_back(i);
				reflectiveStructureContainer.possibleReflectiveLocations[i].push_back(toFind);
				return 0;
				//			return yScore;
			}
		}
	}
	vector<R2000Scan::Point> temp;
	temp.push_back(toFind);
	reflectiveStructureContainer.possibleReflectiveLocations.push_back(temp);
	reflectiveStructureContainer.positionalTranslationMapping.push_back(yScore);
	return -1;
}

void sortPointsByX(vector<R2000Scan::Point> &toFind, int left, int right)
{
	if (left >= right)
	{
		return;
	}

	float pivot = toFind[right].x;

	int cnt = left;

	for (int i = left; i <= right; i++)
	{
		if (toFind[i].x <= pivot)
		{
			swap(toFind[cnt], toFind[i]);
			cnt++;
		}
	}
	sortPointsByX(toFind, left, cnt - 2);	//Recursiveness. Need to analyze time spent on sorting and possibly switch to merge sort for time stability
	sortPointsByX(toFind, cnt, right);
}





bool firstBarcodeReadFound = false;
uint32_t scanNumberOfBarcode = 0;
float yValueOfReferencePoint = -1;
float lastReferencePointSolution = 0;

int currentReflectivePosition = -1;

bool findReferencePoint(vector<R2000Scan::Point> &toFind)
{
	localizeAccomplished = false;
	float twentyPercentHeight = (lowestPoint + abs(highestPoint)) / 5;
	twentyPercentHeight = twentyPercentHeight * 3;	//This creates the threshold value that requires considered points be in the top 20 percent of height
	twentyPercentHeight = ((lowestPoint + abs(highestPoint))) - twentyPercentHeight;
	firstBarcodeReadFound = false;
//	checkpointN(1);
	vector<R2000Scan::Point> reflectivePoints;
	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].i > 0.07)
		{
//			currentPosition = i;
			reflectivePoints.push_back(toFind[i]);
		}
	}
	if (reflectivePoints.size() < 5)
	{
//		printf("Returned false\n");
		return false;
	}
//	printf("Reflective points: %i\n", reflectivePoints.size());
	for (int i = 0; i < reflectivePoints.size(); i++)
	{
		if (isReflectiveLocationFound(reflectivePoints[i]) == -1)
		{
			yValuesSeen.push_back(int(reflectivePoints[i].y / 50.8));	//Need to pair up with other points at chosen position. For objrct detection.
		}
	}
//	printf("Reflective locations found: %i\n", reflectiveStructureContainer.possibleReflectiveLocations.size());
	bool bigJumpDetected = false;
	bool smallJumpDetected = false;
	float currentHeight = 0;
	int randomChosenPoint = 0;
	int chosenObject = -1;
	for (int i = 0; i < reflectiveStructureContainer.possibleReflectiveLocations.size(); i++)
	{
//		checkpointN(1);
		sortPointsByX(reflectiveStructureContainer.possibleReflectiveLocations[i], 0, reflectiveStructureContainer.possibleReflectiveLocations[i].size() - 1);	//First sort the points by their height.
		bigJumpDetected = false;
		smallJumpDetected = false;
		for (int j = 0; j < reflectiveStructureContainer.possibleReflectiveLocations[i].size(); j++)
		{
//			printf("Reflective points at location %i is: %i\n", i, reflectiveStructureContainer.possibleReflectiveLocations[i].size());

			if (reflectiveStructureContainer.possibleReflectiveLocations[i][j].x < 0)
			{
//				currentHeight = lowestPoint - abs((reflectiveStructureContainer.possibleReflectiveLocations[i][j].x)/* - lowestPoint*/);
				currentHeight = reflectiveStructureContainer.possibleReflectiveLocations[i][j].x;
			}
			else
			{
//				currentHeight = abs(reflectiveStructureContainer.possibleReflectiveLocations[i][j].x - (lowestPoint));
				currentHeight = reflectiveStructureContainer.possibleReflectiveLocations[i][j].x;
			}
//			checkpointN(6);
//			printf("The x value is: %f\n", reflectiveStructureContainer.possibleReflectiveLocations[i][j].x);
			if (reflectiveStructureContainer.possibleReflectiveLocations[i][j].y > 50.8 && currentHeight < twentyPercentHeight )//&& /*reflectiveStructureContainer.possibleReflectiveLocations[i][j].x < 1500*/)
			{
//				printf("The x value is: %f\n", reflectiveStructureContainer.possibleReflectiveLocations[i][j].x);
//				checkpointN(5);
				if (j > 0)
				{
//					printf("The current height is: %f      %f\n", currentHeight, reflectiveStructureContainer.possibleReflectiveLocations[i][j].y);
//					checkpointN(4);
					float tempDistance = findDistanceUsingOnlyX(reflectiveStructureContainer.possibleReflectiveLocations[i][j], reflectiveStructureContainer.possibleReflectiveLocations[i][j - 1]);
					if (tempDistance > 10)
					{
//						printf("The tempDistance on object %i at place %i is: %f at x: %f and y: %f\n", i, j, tempDistance, reflectiveStructureContainer.possibleReflectiveLocations[i][j].x, reflectiveStructureContainer.possibleReflectiveLocations[i][j].y);
					}
					if (tempDistance > 50 && !bigJumpDetected && reflectiveStructureContainer.possibleReflectiveLocations[i][j].x < 1700)	//Big jump
					{
						randomChosenPoint = j;
						//						printf("CurrentHeight is: %f\t%f\n", currentHeight, twentyPercentHeight);
//						checkpointN(2);
						bigJumpDetected = true;
					}
					else if (tempDistance > 50 && bigJumpDetected == true && reflectiveStructureContainer.possibleReflectiveLocations[i][j].x < 1700)	//Small jump
					{
						randomChosenPoint = j;
//						checkpointN(3);
						smallJumpDetected = true;
						chosenObject = i;	//Identify this object as the chosen one
					}
				}
			}
		}


	}
	if (chosenObject != -1)
	{
		float tempYValue = 0;
		float tempXValue = 0;

		for (int i = 0; i < reflectiveStructureContainer.possibleReflectiveLocations[chosenObject].size(); i++)
		{
			tempYValue += reflectiveStructureContainer.possibleReflectiveLocations[chosenObject][i].y / reflectiveStructureContainer.possibleReflectiveLocations[chosenObject].size();
			tempXValue += reflectiveStructureContainer.possibleReflectiveLocations[chosenObject][i].x / reflectiveStructureContainer.possibleReflectiveLocations[chosenObject].size();
		}
		localizeAccomplished = true;
		initialFixFound = true;
		localizedPosition = tempYValue;
//		printf("Barcode has been detected at: %f\t%f\n", tempYValue, tempXValue);
/*		if (reflectiveStructureContainer.possibleReflectiveLocations[i][randomChosenPoint].x < 0)
		{
			currentHeight = lowestPoint + abs(reflectiveStructureContainer.possibleReflectiveLocations[i][randomChosenPoint].x);
			printf("Height of random chosen point on reflective structure: %f\n", currentHeight);
		}
		else
		{
			currentHeight = abs(reflectiveStructureContainer.possibleReflectiveLocations[i][randomChosenPoint].x - (lowestPoint));
			//				printf("Height threshold currently: %f\n", currentHeight);
		}
		//			printf("Height threshold currently: %f\n", twentyPercentHeight);

		if (!firstBarcodeReadFound)
		{
			firstBarcodeReadFound = true;
			yValueOfReferencePoint = tempValue;
		}
		else
		{
			printf("\n");
			printf("Scan has more than one barcode detected\n");
			printf("\n");
		}*/
	}
//	printf("Returned false\n");
}










int currentTrailerBeingFilled = 0;

bool firstFound = false;
bool errorFound = false;



bool initialTrailerCalculated = false;

int timesInitialTrailerCorrectlyLookedAt = 0;

int timesTrailerOneGuessed = 0;
int timesTrailerTwoGuessed = 0;

int timesInitialTrailerCorrectlyLookedAtThreshold = 5;

int trailerAlgorithmStartedOn = -1;	//This should be reset when trailer is correctly calculated


void filterReflectiveIntensity()
{
	// [Philipp]: Enter critical section and swap the scan buffers
	holdPackets.clear();
	EnterCriticalSection(&criticalSection);
	if (tempHoldPackets.size() > 116)	//signifying a full rotation. Possibly switch this to a time based analysis as this appears unreliable.
	{
		std::swap(holdPackets, tempHoldPackets);
	}
	else
	{
		LeaveCriticalSection(&criticalSection);
		return;
	}
	LeaveCriticalSection(&criticalSection);
	allPoints.clear();

//	checkpointN(1);
	lowestPoint = 0;
	highestPoint = 0;
	mostForwardPoint = 0;
	for (int i = 0; i < holdPackets.size(); ++i)		//Filling allPoints;
	{
		for (int j = 0; j < holdPackets[i]->points.size(); ++j)
		{
			if (abs(holdPackets[i]->points[j].x) < 300 && abs(holdPackets[i]->points[j].y) < 300) //rejects any points within 5 inches of the lidar. Used for ignoring cables
			{
				continue;
			}
			if (holdPackets[i]->points[j].y < 1000 && holdPackets[i]->points[j].y > -1000)
			{
				beneathLidarPoints.push_back(holdPackets[i]->points[j]);
				continue;
			}
/*			if (holdPackets[i]->points[j].y < 700 && holdPackets[i]->points[j].y > -700)	//Ignore all points that might see the falling tomatoes
			{
				continue;
			}*/
			allPoints.push_back(holdPackets[i]->points[j]);
			if (allPoints[allPoints.size() - 1].x > lowestPoint)	//X values are backwards thus positive is down
			{
				lowestPoint = allPoints[allPoints.size() - 1].x;
			}
			else if(allPoints[allPoints.size() - 1].x < highestPoint && allPoints[allPoints.size() -1].x > -10000)	//This stops it from reacting to birds flying over the lidar. This actually happened at the test facility
			{
				highestPoint = allPoints[allPoints.size() - 1].x;
			}
			if (allPoints[allPoints.size() - 1].y > mostForwardPoint)
			{
				mostForwardPoint = allPoints[allPoints.size() - 1].y;
			}
		}
	}
/*	float highestPoint = -100000;
	float lowestPoint = -100000;
	bool trailerFound = false;
	int numOfTrailerFound = 0;
	for (int i = 0; i < allPoints.size(); i++)
	{
		if (trailerFound)
		{
			continue;
		}
		if (allPoints[i].angle > 29 && allPoints[i].angle < 31)
		{
			if (allPoints[i].x < 3048)
			{
				numOfTrailerFound++;
			}
			if (numOfTrailerFound >= 5)
			{
				trailerFound = true;
			}
		}
	}

	if (!trailerFound)
	{
		return;
	}*/

	bool groundBased = false;
	bool reflectiveBased = false;
	if (allPoints.size() > 0)
	{
		findReferencePoint(allPoints);
		if (!localizeAccomplished)
		{
			if (mostAheadReflictivePointBased(allPoints))
			{
				reflectiveBased = true;
				localizeAccomplished = true;
			}
/*			else
			{
				if (groundBasedLocalization(allPoints))
				{
					reflectiveBased = true;
					localizeAccomplished = true;
				}
			}*/
		}
	}

	if (groundBased)
	{
		localizedPosition = groundBasedLocalizationChosenPosition;
	}
	else if (reflectiveBased)
	{
		localizedPosition = mostAheadReflectivePointBasedPosition;
	}

	if (localizeAccomplished && !initialTrailerCalculated)
	{
		localizeAccomplished = false;
		int temp = whichTrailerAtStart();
		if (temp == 1)
		{
			timesTrailerOneGuessed++;
			timesInitialTrailerCorrectlyLookedAt++;
		}
		if (temp == 2)
		{
			timesTrailerTwoGuessed++;
			timesInitialTrailerCorrectlyLookedAt++;
		}
		else
		{

		}
		if (timesInitialTrailerCorrectlyLookedAt >= timesInitialTrailerCorrectlyLookedAtThreshold)
		{
			initialTrailerCalculated = true;
			localizeAccomplished = true;
			if (timesTrailerOneGuessed > timesTrailerTwoGuessed)
			{
				trailerAlgorithmStartedOn = 1;
			}
			else
			{
				trailerAlgorithmStartedOn = 2;
			}
		}
	}

	if (initialTrailerCalculated)
	{
//		printf("Initial trailer calculated\n");
		if (localizeAccomplished)
		{
//			printf("Localize accomplished\n");
		}
		else
		{
//			printf("Localize not accomplished\n");
		}
	}

/*	for (int i = 0; i < reflectiveStructureContainer.possibleReflectiveLocations.size(); i++)
	{
		float tempYValue = reflectiveStructureContainer.possibleReflectiveLocations[i][0].y / 50.8;	//Simply brute force checking to see if we can identify
																									//the reference point
		printf("Reflective location found at: %f\n", tempYValue);
	}*/
	if (localizeAccomplished)
	{
		infillControl();
		tooFar = goneTooFar();
	}



	reflectiveStructureContainer.positionalTranslationMapping.clear();
	reflectiveStructureContainer.possibleReflectiveLocations.clear();
	yValuesSeen.clear();
/*	if (!firstFound)
	{

		firstFound = findEdgesInitial(allPoints);
		errorFound = firstFound;
	}
	else
	{
		errorFound = findEdgesAfterInitial(allPoints);
	}*/

	beneathLidarPoints.clear();
	return;
}



// [Philipp]: removed (see below and at the top of this file)
//struct tempPoint
//{
//	float x;
//	float y;
//	float i;	// value range: [0,1]
//	float a;    // angle
//
//	tempPoint(float _x, float _y, float _i, float _a = 0.0f) : x(_x), y(_y), i(_i), a(_a) {}
//};

void received(R2000Scan::Ptr&& packet)
{
	// the callback function should be left as soon as possible, otherwise old scans might get discarded
	// hold packets in vector for evaluation later
	// newPackets.points.resize()

	// [Philipp]: It would be nice to also move this vector::clear outside of this
	// function, since clearing a vector with many elements is a costly operation
	// (each item's destructor has to be called and the memory deallocated)

	EnterCriticalSection(&criticalSection);

	if (tempHoldPackets.size() > PACKET_VECTOR_CLEAR_AT_SIZE)
	{
		tempHoldPackets.clear();
	}


	tempHoldPackets.push_back(std::move(packet));

	LeaveCriticalSection(&criticalSection);
}

#define heightOFTrailer 2387.6;

bool mostAheadReflictivePointBased(vector<R2000Scan::Point> &toFind)		//Now just highest point based as this became completely unreliable.
{

	float mostAheadYValue = 0;
	int chosenPoint = -1;

	float highestPoint = 1000000;
	int referenceOfHighestPoint = -1;
//	checkpointN(103);

	for (int i = 0; i < toFind.size(); i++)
	{
		if (toFind[i].x < highestPoint && toFind[i].y > 2000 && toFind[i].x < 1400)
		{
			highestPoint = toFind[i].y;
			referenceOfHighestPoint = i;
		}
	}
//	checkpointN(104);
	if (referenceOfHighestPoint == -1)
	{
//		checkpointN(102);
		return false;
	}
	else
	{
		if (toFind[referenceOfHighestPoint].y < 1000)
		{
			printf("The highest point is: %f\t%f\n", toFind[referenceOfHighestPoint].y, toFind[referenceOfHighestPoint].x);
			printf("The size of toFind is: %i\n", toFind.size());
		}
//		checkpointN(101);
		mostAheadReflectivePointBasedPosition = toFind[referenceOfHighestPoint].y;
		return true;
	}
}


//Trailer height 94 and one eighth inches

bool groundBasedLocalization(vector<R2000Scan::Point> &toFind)
{
	vector<R2000Scan::Point> groundPointsBehind;
	vector<R2000Scan::Point> groundPointsAhead;
	int nearestGroundBehind = -1;
	int nearestGroundAhead = -1;
	float yValueOfNearestGroundBehind = -10000000;
	float yValueOfNearestGroundAhead = 10000000;
	bool groundAhead = false;
	bool groundBehind = false;

	checkpointN(1);

	bool first = false;
	bool firstAhead = false;
	if ((abs(lowestPoint - highestPoint)) < 2743.2)	//Checking for seeing the ground. currently theshold is 9 feet
	{
		printf("LowestPoint: %f\tHighestPoint: %f\n", lowestPoint, highestPoint);
		checkpointN(2);
		return false;								//Unable to say if the ground is being seen
	}
	if (mostForwardPoint < 1000)
	{
		checkpointN(101);
		return false;
	}
	for (int i = 0; i < toFind.size(); i++)
	{
		if (abs(toFind[i].y) < 50.8)
		{
			if (toFind[i].x > 3000)
			{
				checkpointN(100);
				return false;					//Over ground or gap indication
			}
		}
		if (abs(toFind[i].x - lowestPoint) < 50.8)	//Within 2 inches of the ground will be counted as the ground
		{
			
			if (toFind[i].y < -25.4)
			{
				groundPointsBehind.push_back(toFind[i]);
				groundBehind = true;
				if (toFind[i].y > yValueOfNearestGroundBehind)
				{
					if (!first)
					{
						checkpointN(3);
						first = true;
					}
					nearestGroundBehind = groundPointsBehind.size() - 1;
					yValueOfNearestGroundBehind = toFind[i].y;
				}
			}
			else if(toFind[i].y > 25.4)
			{
				groundPointsAhead.push_back(toFind[i]);
				groundAhead = true;
				if (toFind[i].y < yValueOfNearestGroundBehind)
				{
					if (!firstAhead)
					{
						checkpointN(4);
						firstAhead = true;
					}
					nearestGroundAhead = groundPointsAhead.size() - 1;
					yValueOfNearestGroundAhead = toFind[i].y;
				}
			}
			else
			{
				checkpointN(5);
				return false;		//This indicates that we are over the ground and thus cannot do this angular analysis.
			}
		}
	}
	if (!groundAhead && !groundBehind)
	{
		checkpointN(6);
		return false;
	}
	else if (groundBehind)		//Behind will likely be more reliable. But this is an unkown.
	{
		checkpointN(7);

		float distanceHorizontal = groundPointsBehind[nearestGroundBehind].y;
		printf("The distance horizontal is: %f\n", distanceHorizontal);
		float distanceVertical = groundPointsBehind[nearestGroundBehind].x;
		printf("The distance vertical is: %f\n", distanceVertical);
		float slope = distanceHorizontal / distanceVertical;
		printf("The slope is: %f\n", slope);
		float heightToFind = distanceVertical - heightOFTrailer;
		printf("The heightToFind is: %f\n", heightToFind);
		float edgePosition = (slope * heightToFind);
		printf("The edgePosition is: %f\n", edgePosition);
		if (mostForwardPoint > 10000)	//If over the back trailer
		{
			checkpointN(8);
			groundBasedLocalizationChosenPosition = (18000 - abs(edgePosition));
			return true;
		}
		else
		{
			checkpointN(9);
			printf("The mostForwardPoint is: %f\n", mostForwardPoint);
			groundBasedLocalizationChosenPosition = (10000 - abs(edgePosition));
			return true;
		}
	}
	else                        //If ground ahead
	{
/*		checkpointN(10);
		float distanceHorizontal = groundPointsAhead[nearestGroundAhead].y;
		checkpointN(13);
		float distanceVertical = groundPointsAhead[nearestGroundAhead].x;
		checkpointN(14);
		float slope = distanceVertical / distanceHorizontal;
		checkpointN(15);
		float heightToFind = distanceVertical + heightOFTrailer;
		checkpointN(16);
		float edgePosition = (slope * heightToFind);
		checkpointN(17);
		if (mostForwardPoint > 10000)	//If over the back trailer
		{
			checkpointN(11);
			groundBasedLocalizationChosenPosition = (18000 + abs(edgePosition));
			return true;
		}
		else
		{
			checkpointN(12);
			groundBasedLocalizationChosenPosition = (10000 + abs(edgePosition));
			return true;
		}*/
		checkpointN(102);
		return false;

	}
}



deque<float> runningAveragesOfThreeErrors;
deque<float> lastThreeErrors;

deque<float> last20Errors;

int numberOfErrorCounts = 0;

float processErrorChange()
{
	float tempAverage = 0;
	for (int i = 1; i < 3; i++)
	{
		tempAverage += (lastThreeErrors[i-1] - lastThreeErrors[i]) / 2;	//Generating average of errors
	}

	runningAveragesOfThreeErrors.push_back(tempAverage);

	if (runningAveragesOfThreeErrors.size() > 3)
	{
		runningAveragesOfThreeErrors.pop_back();
	}

	float moreTemp = 0;

	for (int i = 1; i < runningAveragesOfThreeErrors.size(); i++)
	{
		moreTemp += (runningAveragesOfThreeErrors[i-1] - runningAveragesOfThreeErrors[i]) / runningAveragesOfThreeErrors.size();
	}
	if (error < 0 && moreTemp < 0)
	{
		return moreTemp * -2;
	}
	else if (error > 0 && moreTemp > 0)
	{
		return moreTemp * -2;
	}
	else
	{
		return 0;
	}

}

float calculateAverageOfErrorChange()
{
	float temp;
	if (last20Errors.size() == 0)
	{
		return 0;
	}
	for (int i = 1; i < last20Errors.size(); i++)
	{
		temp += abs(last20Errors[i - 1] - last20Errors[i]);
	}
	return temp;
}

bool one = true;
float lastOutput = 0;

deque<float> lastFiveSpeeds;
deque<float> lastSixErrors;
float lastVelocity = 0;
byte acceptedBaseVelocity = 0;


float calculateDifferenceInMPH(float theError)
{
	lastSixErrors.push_front(theError);
	if (lastSixErrors.size() < 6)
	{
		return 0;
	}

	if (lastSixErrors.size() > 6)
	{
		lastSixErrors.pop_back();
	}
	float velocity = (lastSixErrors[0] - lastSixErrors[1]);
	velocity = velocity / 1.4667;
	if (velocity < 0.05)	//If the current velocity is very low we accept it as them having the same speed must occur twice
	{
		if (lastVelocity < 0.05)
		{
			acceptedBaseVelocity = byte(finalUnmodifiedOutput);
//			printf("The new accepted velocity is: %f\n", (float)acceptedBaseVelocity);
		}
	}
	return velocity;
}




float brakeKp = .1;
float brakeKi = 0;
float brakeKd = 0;

float brakingControl(float Error)
{
	float output = (Error * brakeKp);
	if (output > 200)
	{
		output = 200;
	}
	else if (output < -200)
	{
		output = -200;
	}
	return output;
}










float gainModifier()
{
	float theGain = 0;

	deque<float> positions;
	vector<R2000Scan::Point> pointsInTrailer;

	if (secondTrailerCompleted || firstTrailerComplete)
	{
		return 40;
	}

	for (int i = 1; i < positionsSecondTrailer.size(); i++)
	{
		
	}
	checkpointN(1050);
//	if (!secondTrailerCompleted)
	{
		if (currentTrailerBeingFilled == 2)
		{
			for (int i = 0; i < allPoints.size(); i++)
			{
				//		printf("The point is: %f\tCondition one: %f\t Condition two: %f\n", allPoints[i].y, (localizedPosition - allPoints[i].y), (positionsSecondTrailer[positionsSecondTrailer.size() - 1] - localizedPosition));
				if ((localizedPosition - allPoints[i].y) > (positionsSecondTrailer[0]) && (localizedPosition - allPoints[i].y) < (positionsSecondTrailer[positionsSecondTrailer.size() - 1]))
				{
					//		printf("The point is: %f\tCondition one: %f\t Condition two: %f\n", allPoints[i].y, (positionsSecondTrailer[0] - localizedPosition), (positionsSecondTrailer[positionsSecondTrailer.size() - 1] - localizedPosition));
					pointsInTrailer.push_back(allPoints[i]);
				}
			}
		}
		else if (currentTrailerBeingFilled == 1)
		{
			for (int i = 0; i < allPoints.size(); i++)
			{
				//		printf("The point is: %f\tCondition one: %f\t Condition two: %f\n", allPoints[i].y, (localizedPosition - allPoints[i].y), (positionsSecondTrailer[positionsSecondTrailer.size() - 1] - localizedPosition));
				if ((localizedPosition - allPoints[i].y) >(positionsFirstTrailer[0]) && (localizedPosition - allPoints[i].y) < (positionsFirstTrailer[positionsFirstTrailer.size() - 1]))
				{
					//		printf("The point is: %f\tCondition one: %f\t Condition two: %f\n", allPoints[i].y, (positionsSecondTrailer[0] - localizedPosition), (positionsSecondTrailer[positionsSecondTrailer.size() - 1] - localizedPosition));
					pointsInTrailer.push_back(allPoints[i]);
				}
			}
		}

		
	}
	checkpointN(1051);
	float averageHeight = 0;
	
	if (pointsInTrailer.size() < 1)
	{
		return 0;
	}

	for (int i = 0; i < pointsInTrailer.size(); i++)
	{
		averageHeight += pointsInTrailer[i].x / pointsInTrailer.size();
	}
	checkpointN(1052);

	theGain = ((averageHeight) - 1000 - highestPoint - 890) * gainModifierMod;	//1000 used in place of lidar pbtained height. this is to allow for the ATT to be used on a partially filled trailer
	checkpointN(1053);
	theGain = theGain * -1;	//Inversion necessary due to lidar data read out
//	printf("The gain is: %f\n", theGain);
//	printf("The averageHeigh is: %f\n", averageHeight);
//	printf("The points in the trailer are: %i\n", pointsInTrailer.size());
//	printf("The position 0 in the second trailer is: %f\n", positionsSecondTrailer[0]);
//	printf("The position 0 in the second trailer is: %f\n", positionsSecondTrailer[positionsSecondTrailer.size() - 1]);

	if (theGain < 0)
	{
		return 0;
	}
	checkpointN(1054);

	return theGain;
}



float counterTheKiWeight = .1;
//float lastOutput = 0;

float bonusThrottle = 0;

deque<float> pastErrors;
bool growAllowed = true;
bool frontOfTrailerIncentiveUsed = false;

float frontOfTrailerIncentive = 40;

int timesNotPastTrailer = 0;
int timesPastTrailer = 0;

float ThrottleControl(float Error) {
/*	if (Error > 10)
	{
//		lastOutput = 100;
		return 200;
	}*/
//	Error = Error*-1;

	if (timesPastTrailer > 9)
	{
		averageOfOutputs.clear();
		timesPastTrailer = 0;
	}

	frontOfTrailerIncentiveUsed = false;
	checkpointN(100);
	if (!secondTrailerCompleted)
	{
		if (currentPositionReference <= 1)
		{
			if (Error > 2.6)
			{
				frontOfTrailerIncentiveUsed = true;
			}
		}
	}
	if (!firstTrailerComplete)
	{
		if (currentPositionReference <= 2)		//Front trailer has more weight and is closer to cab thus making this more crucial i.e. the jolt to the driver is more acceptable
		{
			if (Error > 2.5)
			{
				frontOfTrailerIncentiveUsed = true;
			}
		}
	}
	checkpointN(101);

	float element;
	trueAverageOfOutput = 0;
	float derivative = (Error - lastError);
	lastError = Error;

	checkpointN(102);


	if (averageOfOutputs.size() == 0)
	{
		averageOfOutputs.push_back(0);
	}

	for (int i = 0; i < averageOfOutputs.size(); i++)
	{
		trueAverageOfOutput += averageOfOutputs[i] / averageOfOutputs.size();
	}
	checkpointN(103);
	//	printf("The trueAverageOfOutput: %f\n", trueAverageOfOutput);
//	printf("The size of averageOfOutputs: %i\n", averageOfOutputs.size());
//	printf("The ki is: %f\n", (trueAverageOfOutput*Ki));
	output = (Error * Kp) + (trueAverageOfOutput * Ki) + (derivative * Kd);
	checkpointN(1040);
	if (positionsInitiated)
	{
		output += gainModifier();
	}
	checkpointN(1041);
//	output += bonusThrottle;
	checkpointN(104);

	if ((output - lastOutput) < -7)
	{
		output = lastOutput - 7;
	}
	if ((output - lastOutput) > 7)
	{
		output = lastOutput + 7;
	}
	checkpointN(105);

	if (frontOfTrailerIncentiveUsed)
	{
	//	output += frontOfTrailerIncentive;
	}

	if (output < -200)
	{
		output = -200;
	}
	if (output > 200)
	{
		output = 200;
	}
	checkpointN(106);

	averageOfOutputs.push_front(Error);

	if (averageOfOutputs.size() > 150)
	{
		averageOfOutputs.pop_back();
	}
	lastOutput = output;

	float finalMod = 0;
	checkpointN(107);


//	output += finalMod;

	if (output > 200)
	{
		output = 200;
	}

	if (output < -200)
	{
		output = -200;
	}
	checkpointN(108);

	return output;

//	outputLast = output;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////Calculating Voltage 




//	lastError = Error;

}

void updateOutput(float &output)
{
	if (averageOfOutputs.size() > 1)
	{
		for (int i = 0; i < averageOfOutputs.size(); i++)
		{
			trueAverageOfOutput += averageOfOutputs[i] / averageOfOutputs.size();
		}
	}

	BYTE temp = voltage;
	if (output - outputLast >= 0.2)
	{
		RtPrintf("Checkpoint one\n%i\n\n", static_cast<int>(output));
		float tempFloat = output / 0.2;
		voltageCarry = static_cast<int>(tempFloat);
		voltagePrecision = 0.2 * voltageCarry;
		temp += static_cast<int>(voltagePrecision);
		validError = true;
		RtPrintf("Checkpoint three\n%i\n\n", voltage);
/*		if (temp > voltage)
		{
			voltage = temp;
		}
		else
		{
			voltage = 255;
		}*/
	}
	else if (output - outputLast < 0.2)
	{
//		RtPrintf("Checkpoint two\n%i\n\n", static_cast<int>(output));
		float tempFloat = abs(output) / 0.2;
		voltageCarry = static_cast<int>(tempFloat);
		voltagePrecision = 0.2 * voltageCarry;
		temp -= static_cast<int>(voltagePrecision);
		validError = true;
//		RtPrintf("Checkpoint three\n%i\n\n", temp);
/*		if (temp <= voltage)
		{
			voltage = temp;
		}
		else
		{
			voltage = 0;
		}*/
	}

//	voltage = temp;

	if (output > 255)
	{
		output = 255;
	}
	else if (output < 0)
	{
		output = 0;
	}
/*	if (error >= 0)
	{
		voltage += abs(abs(static_cast<BYTE>(output)) - abs(outputLast));
	}
	else
	{
		voltage -= abs(abs(output) - abs(outputLast));
		::output -= abs(abs(output) - abs(outputLast));
	}*/
	voltage = output;
	if (averageOfOutputs.size()> 50)
	{
		averageOfOutputs.pop_front();
	}
	averageOfOutputs.push_back(output);
	return;
}


bool goneTooFar()
{
	if (localizedPosition > 18744)
	{
		for (int i = 0; i < beneathLidarPoints.size(); i++)
		{
			if (beneathLidarPoints[i].x > 2743)
			{
				timesPastTrailer++;
				return true;
			}
		}
	}
	timesNotPastTrailer++;
	return false;
}


int timesSinceBarcodeSeen = 0;
int timesSinceTractorSeen = 0;

float stdDevOfLastFiveTractor = 0;		//Half a second of variablility
float stdDevOfLastTenTractor = 0;		//One second of variability
float stdDevOfLastFiftyTractor = 0;	//Five seconds of variability. This is the most we should handle with floating solutions

float stdDevOfLastFiveJump = 0;		//Half a second of variablility
float stdDevOfLastTenJump = 0;		//One second of variability
float stdDevOfLastFiftyJump = 0;	//Five seconds of variability. This is the most we should handle with floating solutions

deque <float> tractorSolutions;
deque <float> jumpSolutions;

bool findEdgesInitialHasRunOnce = false;


/*
Due to the complexity of finding a single algorithm that perfectly handles localization
we will be using multiple algorithms. The algorithms are chosen in a hierarchy with a distrust value for each.
The most desirable one is the barcode finder in which we always accept a solution that it claims to be true.
From there we accept a solution off of the tractor.
If both of those methods fails we will weight the second off the tractor solution againse jump detection.
*/
void localize()		//A single function that manages the localization
{
	if (findReferencePoint(allPoints))	//Ideal case
	{
		return;
	}

	if (findEdgesInitial) //next ideal case
	{
		findEdgesInitialHasRunOnce = true;
		return;
	}
	if (findEdgesInitialHasRunOnce)	//not ideal. This should never run more than five seconds in a row. Must be weighed against the 
	{
		jumpDetection(allPoints);
	}
	return;
}


int whichTrailerAtStart()	//Need half a second of data to ensure that we are not seeing a bad localization
{
	printf("In whichTrailerAtStart ");
	printf("Localized position: %f\t", localizedPosition);
	if (localizedPosition > 2000 && localizedPosition < 9674)
	{
		printf("Returned 1");
		return 1;
	}
	else if (localizedPosition > 11318 && localizedPosition < 18565)
	{
		printf("Returned 2");
		return 2;
	}
	else
	{
		printf("Returned 3");
		return 3;
	}
}


void initiatePositions()		//Function to define the location of goal positions for both trailers
{
	float startPoint = (lengthOfTractorToTrailerOne + lengthOfFirstTrailer + lengthOfGap + safetyFillDistanceFromTheEdge);
	float endPoint = (lengthOfTractorToTrailerOne + lengthOfFirstTrailer + lengthOfGap + lengthOfSecondTrailer - safetyFillDistanceFromTheEdge);
	float incrementalDistance = abs(startPoint - endPoint) / pointsPerTrailer;

	for (int i = 0; i < pointsPerTrailer; i++)
	{
		positionsSecondTrailer.push_back(startPoint + (incrementalDistance * i));			//Generating the points for the second trailer
	}

	startPoint = (lengthOfTractorToTrailerOne + safetyFillDistanceFromTheEdge);
	endPoint = (lengthOfTractorToTrailerOne + lengthOfFirstTrailer - safetyFillDistanceFromTheEdge);
	incrementalDistance = abs(startPoint - endPoint) / pointsPerTrailer;

	for (int i = 0; i < pointsPerTrailer; i++)
	{
		positionsFirstTrailer.push_back(startPoint + (incrementalDistance * i));
	}

	for (int i = 0; i < positionsSecondTrailer.size(); i++)
	{
		printf("Position %i of the second trailer: %f\n", i, positionsSecondTrailer[i]);
	}
	positionsInitiated = true;
	if (trailerAlgorithmStartedOn == 1)
	{
		theGoalPoint = positionsFirstTrailer[positionsFirstTrailer.size()/2];
		currentPositionReference = positionsFirstTrailer.size() / 2;
		currentLayerNumber = 2;
		currentTrailerBeingFilled = 1;
	}
	else if (trailerAlgorithmStartedOn == 2)
	{
		theGoalPoint = positionsSecondTrailer[positionsSecondTrailer.size()/2];	//Fuck it. We're starting midway through the trailers.
		currentPositionReference = positionsSecondTrailer.size() / 2;
		currentLayerNumber = 2;
		currentTrailerBeingFilled = 2;
	}
}

bool isAtGoalPoint()	//function used to determine if elevator is within a set distance of goal point
{
//	printf("The goalPoint is: %f\n", theGoalPoint);
	if (abs(localizedPosition - theGoalPoint) < atGoalPointThreshold)
	{
		printf("At goalPoint of: %f\n", theGoalPoint);
		return true;
	}
	return false;
}

int numberOfGoodFills = 0;
int numberOfGoodFillsThreshold = 2;

bool acceptableFill()
{
	float minimumFillHeight = (highestPoint + (goalFillHeight)) + (depthOfTrailer - ((depthOfTrailer / numberOfStepsToFull)*currentLayerNumber));	//Wow this is a loaded equation
	if (currentTrailerBeingFilled == 2)
	{
		minimumFillHeight = (highestPoint + (goalFillHeight - 103.2)) + (depthOfTrailer - ((depthOfTrailer / numberOfStepsToFull)*currentLayerNumber));	//Wow this is a loaded equation
	}
//	printf("The minimumFillHeight is: %f\n", minimumFillHeight);
	//vector <R2000Scan::Point> inRangePoints;
	for (int i = 0; i < beneathLidarPoints.size(); i++)
	{
		if (beneathLidarPoints[i].y < 100 && beneathLidarPoints[i].y > -100)
		{
			
			if (beneathLidarPoints[i].x > minimumFillHeight)
			{
//				printf("acceptableFill returned false\n");
				return false;
			}
//			printf("The height of the point is: %f\n", allPoints[i].x);
		}
	}
	numberOfGoodFills++;
	if (numberOfGoodFills >= numberOfGoodFillsThreshold)
	{
		printf("acceptableFill returned true\n");
		numberOfGoodFills = 0;
		return true;
	}
	else
	{
		return false;
	}
/*	int toTrack = inRangePoints.size() / 10;

	deque<R2000Scan::Point> lowestTenPercent;
	for (int i = 0; i < inRangePoints.size(); i++)
	{
		for (int j = 0; j < lowestTenPercent.size(); j++)
		{
			if (inRangePoints[i].x > lowestTenPercent[j].x)
			{
				deque<R2000Scan::Point>::iterator it = lowestTenPercent.begin() + j;
				lowestTenPercent.insert(it, inRangePoints[i]);
			}
		}
	}*/
}

//bool goingBack = true;

bool shouldSkip = false;
bool switchingMode = true;
int timesNearGoalPosition = 0;
int timesNearGoalPositionThreshold = 15;

bool setPointMoveMode = false;
int timesAtSetPointMove = 0;
int timesAtSetPointMoveThreshold = 4;

void infillControl()
{
	printf("In infillControl\n");
	if (!initialFixFound)
	{
//		printf("Initial fix not found\n");
		return;
	}

	if (!positionsInitiated)
	{
		//printf("Positions not initiated\n");
		initiatePositions();
	}

	firstFirstTrailerDone = true;
	
	if (positionsInitiated && switchingMode)
	{
		if (isAtGoalPoint())
		{
			timesNearGoalPosition++;
		}
		if (timesNearGoalPosition >= timesNearGoalPositionThreshold)
		{
			switchingMode = false;
			timesNearGoalPosition = 0;
		}
		return;
	}

	if (positionsInitiated && setPointMoveMode)
	{
		if (isAtGoalPoint())
		{
			timesAtSetPointMove++;
		}
		if (timesAtSetPointMove >= timesAtSetPointMoveThreshold)
		{
			timesAtSetPointMove = 0;
			setPointMoveMode = false;
		}
		return;
	}
	if (currentTrailerBeingFilled == 2)
	{
		if (isAtGoalPoint())
		{
			//printf("At goal point\n");
			if (acceptableFill())
			{
				//printf("Fill is acceptable\n");
				if (currentPositionReference == positionsSecondTrailer.size()-1)
				{
					currentLayerNumber++;
					goingBack = false;
				}
				if (currentPositionReference == 0)
				{
					if (currentLayerNumber != numberOfStepsToFull -1)
					{
						goingBack = true;
						currentLayerNumber++;		//Remember even number controls.
					}
					else
					{
						done = true;
					}
				}
				if (done)
				{
					secondTrailerCompleted = true;		//Used to inform the gain. Thus stopping the underpowered first trailer issue.
					currentTrailerBeingFilled = 1;
					theGoalPoint = positionsFirstTrailer[positionsFirstTrailer.size()-1];
					currentPositionReference = positionsFirstTrailer.size() - 1;
					shouldSkip = true;
					currentLayerNumber = 4;
					switchingMode = true;
					printf("Trailer Full\n");
					printf("Trailer Full\n");
					printf("Trailer Full\n");
					printf("Trailer Full\n");
					printf("currentLayerNumber: %i\n", currentLayerNumber);
				}
				else if (goingBack)
				{
//					printf("Trailer Full\n");
					if (currentPositionReference != positionsSecondTrailer.size()-1)
					{
						printf("currentLayerNumber: %i\n", currentLayerNumber);
						currentPositionReference++;
						setPointMoveMode = true;
						printf("The currentPositionReference is: %i\n", currentPositionReference);
					}
					else
					{
						currentPosition--;
						currentLayerNumber++;
						goingBack = false;
						setPointMoveMode = true;
						printf("currentLayerNumber: %i\n", currentLayerNumber);
						printf("The currentPositionReference is: %i\n", currentPositionReference);
					}
				}
				else
				{
					currentPositionReference--;
					setPointMoveMode = true;
				}
				if (!shouldSkip)
				{
					theGoalPoint = positionsSecondTrailer[currentPositionReference];
					/*			for (int i = 0; i < positionsSecondTrailer.size(); i++)
								{
									printf("Position %i of the second trailer: %f\n", i, positionsSecondTrailer[i]);
								}*/
					printf("Position %i of the second trailer: %f\n", currentPositionReference, positionsSecondTrailer[currentPositionReference]);
					printf("New goal point is: %f\n", theGoalPoint);
				}
				else
				{
					shouldSkip = false;
				}
			}
			else
			{
		//		printf("Fill is not acceptable\n");
			}
		}
		else
		{
			//printf("Not at goal point\n");
		}

		return;
	}
/*	else if(!firstFirstTrailerDone)
	{
/*		theGoalPoint = positionsFirstTrailer[positionsFirstTrailer.size() -1];
		currentLayerNumber = 1;
		goingBack = false;
		firstFirstTrailerDone = true;
		return;
	}*/
	else if (currentTrailerBeingFilled == 1)
	{
		if (isAtGoalPoint())
		{
//			printf("At theGoalPoint of: ")
			if (acceptableFill())
			{
				if (goingBack)
				{
					if (currentLayerNumber >= numberOfStepsToFull)
					{
						if (currentPositionReference == positionsFirstTrailer.size() - 1)
						{
							currentTrailerBeingFilled = 2;
							theGoalPoint = positionsSecondTrailer[0];
							currentPositionReference = 0;
							currentLayerNumber = 4;
							firstTrailerComplete = true;
							shouldSkip = true;
							switchingMode = true;
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
							printf("Done.\n");
						}
					}
					if (currentPositionReference == positionsFirstTrailer.size() - 1)
					{
						goingBack = false;
						currentPositionReference--;
						currentLayerNumber++;
						setPointMoveMode = true;
					}
					else
					{
						currentPositionReference++;
						setPointMoveMode = true;
					}
				}
				else
				{
					if (currentPositionReference == 0)
					{
						currentLayerNumber++;
						currentPositionReference++;
						goingBack = true;
						setPointMoveMode = true;
					}
					else
					{
						currentPositionReference--;
						setPointMoveMode = true;
					}

				}
			}
		}
		if (!shouldSkip)
		{
			theGoalPoint = positionsFirstTrailer[currentPositionReference];
			printf("Position %i of the FIRST trailer: %f\n", currentPositionReference, positionsFirstTrailer[currentPositionReference]);
			printf("New goal point is: %f\n", theGoalPoint);
		}
		else
		{
			shouldSkip = false;
		}
	}


}



struct UIStruct {
	bool paired = false;
	bool pairRequestConfirmationPromptOnscreen = false;
	bool pairRequestConfirmed = false;
	bool pairRequestSent = false;
	bool isHarvester = true;
	//	bool justSentOutPairRequest = false;
	bool userRequestedPairQuery = false;
	bool engaged = true;
	bool userConfirmedEngagePopup = false;
	bool userRequestedEngage = false;
	bool engageRequestSent = false;
	bool disengageTimerActivated = false;
	bool userRequestedUnpair = false;
	bool userRequestedDisengage = false;
	WORD harvesterID = 12345;
};


UIStruct UIReference;
UIStruct oldUIReference; //This is used to check to see if the UI actually updated, otherwise we simply push our state to shared memory

bool compareUIStructs(UIStruct *temp)
{
	if (temp->disengageTimerActivated != oldUIReference.disengageTimerActivated)
	{
		return false;
	}
	else if (temp->engaged != oldUIReference.engaged)
	{
		return false;
	}
	else if (temp->engageRequestSent != oldUIReference.engageRequestSent)
	{
		return false;
	}
	else if (temp->paired != oldUIReference.paired)
	{
		return false;
	}
	else if (temp->pairRequestConfirmationPromptOnscreen != oldUIReference.pairRequestConfirmationPromptOnscreen)
	{
		return false;
	}
	else if (temp->pairRequestConfirmed != oldUIReference.pairRequestConfirmed)
	{
		return false;
	}
	else if (temp->pairRequestSent != oldUIReference.pairRequestSent)
	{
		return false;
	}
	else if (temp->userConfirmedEngagePopup != oldUIReference.userConfirmedEngagePopup)
	{
		return false;
	}
	else if (temp->userRequestedDisengage != oldUIReference.userRequestedDisengage)
	{
		return false;
	}
	else if (temp->userRequestedEngage != oldUIReference.userRequestedEngage)
	{
		return false;
	}
	else if (temp->userRequestedPairQuery != oldUIReference.userRequestedPairQuery)
	{
		return false;
	}
	else if (temp->userRequestedUnpair != oldUIReference.userRequestedUnpair)
	{
		return false;
	}
	return true;
}

int _tmain(int argc, _TCHAR * argv[])
{
	// [Philipp]: reserve memory for the vectors (to enable fast push_back())
	holdPackets.reserve(PACKET_VECTOR_RESERVE);
	tempHoldPackets.reserve(PACKET_VECTOR_RESERVE);

	// [Philipp]: Initialize critical section (used for thread-safe swapping of packet buffers)
	InitializeCriticalSection(&criticalSection);

	// [Philipp]: Lidar_Sensor moved to heap
	auto Lidar_Sensor = std::make_unique<R2000Interface>(&received, "192.168.5.50");           // sensor IP

	// set local configuration (not yet transmitted to device)
	Lidar_Sensor->config().frequency(10);
	Lidar_Sensor->config().samples(25200);							// [Philipp]: samples are now automatically set to the nearest compatible setting
	Lidar_Sensor->config().packetType(R2000Config::PacketType::B);	// currently supported: packet types A and B
	Lidar_Sensor->config().startAngle(0);							// start angle in [-180�, 179�]
	Lidar_Sensor->config().maxNumPointsScan(0);						// 0: points are only limited by setSamples()
	
	if (!Lidar_Sensor->connect())									// connect and transmit local configuration to device
	{
		PrintMsg("\nFailed to connect.\nClosing application\n");
		DeleteCriticalSection(&criticalSection);
		return EXIT_FAILURE;
	}

 	Lidar_Sensor->config().print();
	PrintMsg("Configuration set-----\n");
	//Start scanning with Lidar sensor thus initiating logging

	PrintMsg("Start Scanning-------\n");
	Lidar_Sensor->startScanning();

	PrintMsg("StartED---------------------- Scanning-------\n");
	//PrintMsg("26");

	checkpointN(2);

	bool paired = false;
	bool pairRequestConfirmationPromptOnscreen = false;
	bool pairRequestConfirmed = false;
	bool pairRequestSent = false;
	bool isHarvester = true;
//	bool justSentOutPairRequest = false;
	bool userRequestedPairQuery = false;
	bool engaged = true;
	bool userConfirmedEngagePopup = false;
	bool userRequestedEngage = false;
	bool engageRequestSent = false;
	bool disengageTimerActivated = false;
	bool userRequestedUnpair = false;
	checkpointN(4);

	UIReference.paired = paired;
	UIReference.pairRequestConfirmationPromptOnscreen = pairRequestConfirmationPromptOnscreen;
	UIReference.pairRequestConfirmed = pairRequestConfirmed;
	UIReference.pairRequestSent = pairRequestSent;
	UIReference.userRequestedEngage = userRequestedEngage;
	UIReference.engaged = engaged;
	UIReference.userConfirmedEngagePopup = userConfirmedEngagePopup;
	UIReference.engageRequestSent = disengageTimerActivated;
	UIReference.userRequestedUnpair = userRequestedUnpair;
	checkpointN(5);

	WORD harvesterID = 12345;
	clock_t disengageCountdown = 0;
	clock_t timeSinceLastTransmit = clock();
	clock_t controlThePosition = clock(); //This will move by 20 mm every 2 seconds;
	bool goingUp = false;
	bool runUI = false;
	checkpointN(5);
	bool test = openAndConfigurePort();
	checkpointN(3);
	enqueueMessageToSend<byte>(0, 0xFA);		//Wow it has been a while since I wrote this
	xbeeDriver();
	if (test)
	{
		enqueueMessageToSend<BYTE>(0, 0xFA);
		pairRequestSent = true;
	}
	if (test)
	{
		while (true) {
			if (runUI)
			{
				LPSECURITY_ATTRIBUTES mine{ 0 };
				
				VOID *temp;
				VOID *UIPointer;

				HANDLE mutexForUI = RtCreateMutex(mine, false, L"inUseMutexUI");
				HANDLE UIHandle = RtOpenSharedMemory(SHM_MAP_WRITE, false, L"attHarvesterUI", &UIPointer);

				
				UIReference.paired = paired;
				UIReference.pairRequestConfirmationPromptOnscreen = pairRequestConfirmationPromptOnscreen;
				UIReference.pairRequestConfirmed = pairRequestConfirmed;
				UIReference.pairRequestSent = pairRequestSent;
				UIReference.userRequestedEngage = userRequestedEngage;
				UIReference.engaged = engaged;
				UIReference.userConfirmedEngagePopup = userConfirmedEngagePopup;
				UIReference.engageRequestSent = disengageTimerActivated;
				UIReference.userRequestedUnpair = userRequestedUnpair;

				RtWaitForSingleObject(mutexForUI, 1000);
				UIHandle = RtOpenSharedMemory(SHM_MAP_WRITE, false, L"attHarvesterUI", &UIPointer);
				if (UIHandle != NULL)
				{
					UIStruct *tempPointer = static_cast<UIStruct*>(UIPointer);
					if (!compareUIStructs(tempPointer))
					{
						UIReference = *tempPointer;		//Static copy from the temp pointer to a more permanent structure that can be used as comparison against local variables
					}
					else
					{
						tempPointer->disengageTimerActivated = UIReference.disengageTimerActivated;
						tempPointer->engaged = UIReference.engaged;
						tempPointer->engageRequestSent = UIReference.engageRequestSent;
						tempPointer->paired = UIReference.paired;
						tempPointer->pairRequestConfirmationPromptOnscreen = UIReference.pairRequestConfirmationPromptOnscreen;
						tempPointer->pairRequestConfirmed = UIReference.pairRequestConfirmed;
						tempPointer->pairRequestSent = UIReference.pairRequestSent;
						tempPointer->userConfirmedEngagePopup = UIReference.userConfirmedEngagePopup;
						*tempPointer = UIReference;
					}
				}
				RtCloseHandle(UIHandle);
				RtCloseHandle(mutexForUI);
				if (!paired)
				{
					if (receivedPacket->messageType == 0xFA)
					{
						if (pairRequestSent)
						{
							paired = true;
							enqueueMessageToSend<WORD>(harvesterID, 0xFB);
							pairRequestSent = false;
							timeSinceLastTransmit = clock();
						}
						else
						{

						}
					}
					else
					{
						if (userRequestedPairQuery)
						{
							enqueueMessageToSend<BYTE>(0, 0xFA);
							userRequestedPairQuery = false;
							pairRequestSent = true;
						}
						else
						{

						}

					}
				}
				else if (paired)
				{
					if (engaged)
					{
						if (disengageTimerActivated)
						{
							if ((clock() - disengageCountdown) / CLOCKS_PER_SEC > 10)
							{
								engaged = false;
							}
							else
							{
								if ((timeSinceLastTransmit - clock()) / CLOCKS_PER_SEC > .08)
								{
									timeSinceLastTransmit = clock();
									enqueueMessageToSend<BYTE>(voltage, 0x10);
								}
							}
						}
						else
						{
							if (receivedPacket->messageType == 0xFD)
							{
								disengageCountdown = clock();
								disengageTimerActivated = true;
							}
							else
							{
								if ((timeSinceLastTransmit - clock()) / CLOCKS_PER_SEC > .08)
								{
									timeSinceLastTransmit = clock();
									enqueueMessageToSend<BYTE>(voltage, 0x10);
								}
							}
						}
					}
					else
					{
						if (receivedPacket->messageType == 0xFD)
						{
							if (engageRequestSent)
							{
								engaged = true;
							}
							else
							{
								if (userRequestedUnpair || UIReference.userRequestedUnpair)
								{
									userRequestedUnpair = false;
									paired = false;
									enqueueMessageToSend<BYTE>(0, 0xFC);
								}
								else if ((timeSinceLastTransmit - clock()) / CLOCKS_PER_SEC > .08)
								{
/*									timeSinceLastTransmit = clock();
									enqueueMessageToSend<BYTE>(voltage, 0x10);
									RtPrintf("Voltage sent of value: 0x%1x \n", (unsigned)voltage);*/		//Previously used to always engage. This will now be changed
								}
							}
						}
						else
						{
							if (userRequestedEngage || UIReference.userRequestedEngage)
							{
								enqueueMessageToSend<BYTE>(0, 0xFD);
								engageRequestSent = true;
								userRequestedEngage = false;
							}
							else if ((timeSinceLastTransmit - clock()) / CLOCKS_PER_SEC > .08)
							{
/*								timeSinceLastTransmit = clock();
								enqueueMessageToSend<BYTE>(voltage, 0x10);
								RtPrintf("Voltage sent of value: 0x%1x \n", (unsigned)voltage);*/
							}
						}
					}
				}
			}
				filterReflectiveIntensity();

				if (localizeAccomplished)
				{
					if (((controlThePosition - clock()) / CLOCKS_PER_SEC) < 1)
					{
/*						if (goingUp)
						{
							if (theGoalPoint > 14715.6)
							{
								goingUp = false;
							}
							else
							{
								theGoalPoint += 20;
							}
							
						}
						else
						{
							if (theGoalPoint < 13630)
							{
								goingUp = true;
							}
							else
							{
								theGoalPoint -= 20;
							}
						}
						controlThePosition = clock();*/
					}
	//				printf("The goalPoint is: %f\n", theGoalPoint);
					error = (localizedPosition - theGoalPoint);
					errorInFeet = error / 304.8;
//					error = error * -1;
					errorInFeet = errorInFeet * -1;
					printf("The error is: %f\n", errorInFeet);
//					printf("The localized position is: %f\n", localizedPosition);
//					printf("Test");
//					printf("The time is: %ld\n", (clock() - timeSinceLastTransmit) / (CLOCKS_PER_SEC / 1000));
				}
//				checkpointN(4);
//				checkpointN(50);
				xbeeDriver();
//				enqueueMessageToSend<byte>(100, 0x10);		//Wow it has been a while since I wrote this
//				checkpointN(51);
//				checkpointN(5);
				if (((clock() - timeSinceLastTransmit) / (CLOCKS_PER_SEC / 1000) > 80) && localizeAccomplished)
				{

					printf("Current positionReference: %i\n", currentPositionReference);
					printf("Current layerNumber: %i\n", currentLayerNumber);
					if (goingBack)
					{
						printf("Currently going back\n");
					}
					else
					{
						printf("Currently going forward\n");
					}

					if (tooFar == true)
					{
						enqueueMessageToSend<byte>(0, 0x10);
						printf("Throttle set to 0 since tractor went too far.\n");

					}
					else
					{
						checkpointN(10);
						float tempOne = ThrottleControl(errorInFeet);
//						float tempTwo = brakingControl(errorInFeet);
						float finalTemp = 0;
						byte theVoltage = 0;
						//					printf("The output from tempOne is: %f\n", tempOne);
						//					printf("The output from tempTwo is: %f\n", tempTwo);
						checkpointN(11);
						if (tempOne < 200 && tempOne >= 20)
						{
							//						checkpointN(4);
							finalTemp = (float)(tempOne/* - (tempTwo)*/);
							checkpointN(12);
						}
						else if (tempOne < 20)
						{
							finalTemp = 20;
							checkpointN(13);
							//						checkpointN(5);
						}
						else
						{
							finalTemp = 200;
						}
/*						else
						{
							checkpointN(14);
							finalTemp = -200;
							//						checkpointN(6);
						}*/
						//					printf("The finalTemp is: %f\n", finalTemp);
						//					printf("The voltage is: %f\n", (float)theVoltage);
										//	calculateDifferenceInMPH(errorInFeet);
								//			byte tempThrottle = theVoltage;
						checkpointN(15);
						if ((int(finalTemp)) > 200)
						{
							checkpointN(16);
							theVoltage = 200;
						}
						else if ((int(finalTemp)) < 0)
						{
							checkpointN(17);
							theVoltage = 0;
						}
						else
						{
							//						theVoltage += acceptedBaseVelocity;
							checkpointN(18);
							theVoltage += (byte)finalTemp;
						}

						checkpointN(19);
						finalUnmodifiedOutput = (float)theVoltage;
						checkpointN(20);
						enqueueMessageToSend<byte>(theVoltage, 0x10);		//Wow it has been a while since I wrote this
						printf("The throttle has been sent as: %x\n", theVoltage);

					}
					timeSinceLastTransmit = clock();
				}

				localizeAccomplished = false;
				if (firstBarcodeReadFound)
				{

				}

				RtSleep(10);
			}
	
	}
	else
	{
		PrintMsg("Could not connect to the Xbee. Check serial connection.\n");
	}
	PrintMsg("\n[R2000TestApp] About to close.\n");

#ifndef UNDER_RTSS
	PrintMsg("Press RETURN key ...");
	getchar();
#endif
	
	DeleteCriticalSection(&criticalSection);
	return 0;
}