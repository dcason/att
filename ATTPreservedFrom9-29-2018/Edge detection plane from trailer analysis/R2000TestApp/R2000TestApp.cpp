//////////////////////////////////////////////////////////////////
//
// R2000TestApp.cpp
//
// Sample usage for the R2000Client library
//
//////////////////////////////////////////////////////////////////

#include <cmath>
#include <queue>
#include <deque>
#include <iterator>
#include <unordered_map>

#include "RuntimeDecls.h"
#include "LogDecls.h"
#include "R2000Interface.h"

#define PACKET_VECTOR_RESERVE		11000
#define PACKET_VECTOR_CLEAR_AT_SIZE 10000

using namespace std;

/********************************************************************************************************************************************************/
/********************************************************************************************************************************************************/
/****************************************************************Variables*******************************************************************************/
/********************************************************************************************************************************************************/
/********************************************************************************************************************************************************/

struct Points
{
	int scanID = -1;
	vector <float> distances;

	Points() = default;
	Points(int _scanID, float distance) : scanID(_scanID) { distances.push_back(distance); }
};

// [Philipp]: Replaced by R2000Scan::Ptr
//struct Point
//{
//	float x;
//	float y;
//	float i;	// value range: [0,1]
//	float a;    // angle
//
//	Point(float _x, float _y, float _i, float _a = 0.0f) : x(_x), y(_y), i(_i), a(_a) {}
//	Point()
//	{
//		x = 0;
//		y = 0;
//		i = 0;
//		a = 0;
//	}
//
//};
//
//struct fakePacks
//{
//
//	uint32_t scanNumber;
//	uint64_t timeStamp;
//	std::vector<Point> points;
//
//	fakePacks(uint32_t pointCount)
//	{
//		points.reserve(pointCount);
//	}
//};
//

// [Philipp]: Added a critical section here, since there were race conditions (shared memory access from 2 threads)
// from filterReflectiveIntensity() [main thread] and received(...) [a different thread in R2000Interface]
CRITICAL_SECTION criticalSection;
vector<R2000Scan::Ptr> tempHoldPackets;
vector<R2000Scan::Ptr> holdPackets;

unordered_map<int, Points> leftEdge150;
unordered_map<int, Points> rightEdge150;

//vector to hold distance averages per scan for last 150 scans
vector<float> AveragePerScanLeftEdge;
vector<float> AveragePerScanRightEdge;
bool validAverage = false;
bool validError = false;
float averageOfLast500Outputs = 0;
deque<float> last500Outputs;
float Kp = 1;
float Ki = 1;
float Kd = 1;
float maxChange = 0.1f;
float output = 0;
float outputLast = 0;
float leftChangeWeight = 1; //A weight applied to the last amount of change on the left side of the tractor in validateAverage ::DMC::
float leftMaxChange = 4; //The maximum amount of change we will accept on the left side of the tractor ::DMC::
float rightChangeWeight = 1; //A weight applied to the last amount of change on the right side of the tractor in validateAverage ::DMC::
float rightMaxChange = 4; //The maximum amount of change we will accept on the right side of the tractor ::DMC::
float lastError = 0;
//vectors to hold accumulated average Distance from last 150 scans in feet (conversion from mm to feet
vector<float> RunningLeftTrueDistancePer150Scans;
vector<float> RunningRightTrueDistancePer150Scans;
vector<float> RunningError;
vector<float> storageOfPastRunningRightTrueDistancePer150Scans[10];
vector<float> storageOfPastRunningLeftTrueDistancePer150Scans[10];

//keep counter to keep track of how many times we dont see valid Distances
int lostModeRunCounter = 0;
bool enteredLostMode = false;


void received(R2000Scan::Ptr&&);

/********************************************************************************************************************************************************/
/********************************************************************************************************************************************************/
/********************************************************************************************************************************************************/
/********************************************************************************************************************************************************/
/********************************************************************************************************************************************************/

/*Function will take all samples collected so far in Lidar_Sensor->holdPackets vector and filter them out based
on the value of each points light intensity, value needs to be >= 0.21 to be valid point
**/
void filterReflectiveIntensity()
{
	// [Philipp]: Enter critical section and swap the scan buffers
	EnterCriticalSection(&criticalSection);
	std::swap(holdPackets, tempHoldPackets);
	LeaveCriticalSection(&criticalSection);

	//filter last points added
	for (int i = 0; i < holdPackets.size(); i++) {
		for (int j = 0; j < holdPackets[i]->points.size(); j++) {
			//filtration of light intensity
			if (holdPackets[i]->points[j].i >= 0.21) {
				//send towards left edge or right edge
				PrintMsg("\nREFLECTIVE TAPE ------------------- REFLECTIVE TAPE---------------------%f\n", holdPackets[i]->points[j].i);
				PrintMsg("%i", holdPackets[i]->scanNumber);

				if (holdPackets[i]->points[j].x < 0) {
					//left Edge, front of trailer
					PrintMsg("\nLeft Edge ---------------------x: %f y: %f\n", holdPackets[i]->points[j].x, holdPackets[i]->points[j].y);

					//check if map already seen this scan, if not then -> addd new scan id 
					//else add to existing points structure
					if (leftEdge150.find(holdPackets[i]->scanNumber) != leftEdge150.end()) {
						//scan id already in map
						leftEdge150[holdPackets[i]->scanNumber].distances.push_back(abs(holdPackets[i]->points[j].x));

					}
					else {
						//new scanID never seen
						leftEdge150[holdPackets[i]->scanNumber] = Points(holdPackets[i]->scanNumber, abs(holdPackets[i]->points[j].x));
					}

					//assuming scanNumbers start at 1, we can save all points for individual scanIDs and use that to average each
					//scan individually before we average all 150 scans together.
					//vector initial size is 0 and vector Index = scanNumber - 1
					/**if (Lidar_Sensor->leftEdge.size() < (Lidar_Sensor->holdPackets[i]->scanNumber%150)) {
					//if true, we havent seen this scan ID yet and thus we started a new scan
					Lidar_Sensor->leftEdge.push_back(Points(Lidar_Sensor->holdPackets[i]->scanNumber%150, abs(Lidar_Sensor->holdPackets[i]->points[j].x)));


					}
					else {
					//means scanId has already been seen , so just add the x distance to the distance Vector for index = scanNumber - 1
					Lidar_Sensor->leftEdge[(Lidar_Sensor->holdPackets[i]->scanNumber - 1)%150].distances.push_back(abs(Lidar_Sensor->holdPackets[i]->points[j].x));

					}**/
				}
				if (holdPackets[i]->points[j].x > 0) {
					//right Edge, rear of trailer

					PrintMsg("\nRight Edge ------------------x:%f y: %f\n", holdPackets[i]->points[j].x, holdPackets[i]->points[j].y);
					//check if map already seen this scan, if not then -> addd new scan id 
					//else add to existing points structure
					if (rightEdge150.find(holdPackets[i]->scanNumber) != rightEdge150.end()) {
						//scan id already in map
						rightEdge150[holdPackets[i]->scanNumber].distances.push_back(abs(holdPackets[i]->points[j].x));

					}
					else {
						//new scanID never seen
						rightEdge150[holdPackets[i]->scanNumber] = Points(holdPackets[i]->scanNumber, holdPackets[i]->points[j].x);
					}


					/**
					if (Lidar_Sensor->rightEdge.size() <( Lidar_Sensor->holdPackets[i]->scanNumber % 150)) {
					//if true, we havent seen this scan ID yet and thus we started a new scan
					Lidar_Sensor->rightEdge.push_back(Points((Lidar_Sensor->holdPackets[i]->scanNumber%150), Lidar_Sensor->holdPackets[i]->points[j].x));

					}
					else {
					//means scanId has already been seen , so just add the x distance to the distance Vector for index = scanNumber - 1
					Lidar_Sensor->rightEdge[(Lidar_Sensor->holdPackets[i]->scanNumber - 1)%150].distances.push_back(Lidar_Sensor->holdPackets[i]->points[j].x);

					}**/
				}

				//if x == 0 then R2000 hit an edge
			}
		}


	}

	//Dont need to hold on to old packets, so clear them from memory
	holdPackets.clear();


}

//Function called after filtering light intensity
//Calculates average distance per scan for at least last 150 scans of right and left edge vectors

void calculateAverageDistancePerScan() {
	float sum = 0.0;
	float average = 0.0;
	int tenPercentCutoffIndex = 0;
	unordered_map<int, Points>::iterator itr;
	//reset average PerScan vector
	if (AveragePerScanLeftEdge.size() > 0) {
		AveragePerScanLeftEdge.clear();
	}
	if (AveragePerScanRightEdge.size() > 0) {
		AveragePerScanRightEdge.clear();
	}


	//wait for 150 scans if not in lostMode
	if (leftEdge150.size() > 149 && rightEdge150.size() > 149 && !enteredLostMode) {
		PrintMsg("Place 1");


		//iterate through map

		for (itr = leftEdge150.begin(); itr != leftEdge150.end(); itr++) {
			PrintMsg("Place 2");

			//remove T/B 10 % of points in distances vector
			sort(itr->second.distances.begin(), itr->second.distances.end());
			tenPercentCutoffIndex = static_cast<int>(0.1 * itr->second.distances.size());
			PrintMsg("Place 3");
			//remove bottom 10 percent of distances -> distance still in mm
			itr->second.distances.erase(itr->second.distances.begin(), itr->second.distances.begin() + tenPercentCutoffIndex);

			//remove top 10 percent of distances for left Edge
			itr->second.distances.erase(itr->second.distances.end() - tenPercentCutoffIndex, itr->second.distances.end());
			//now calculate distance averages for left edge and send to ValidAverageDistanceFor150Scans Vector
			for (int j = 0; j < itr->second.distances.size(); j++) {
				sum += itr->second.distances[j];
			}
			average = sum / itr->second.distances.size();
			AveragePerScanLeftEdge.push_back(average); //populate averageDistance Per scan for left edge vector which will then aggregrate to one True Distance value for left Edge


		}


		/**
		//create averages for each scan in leftEdge -> last 150 scans
		for (int i = Lidar_Sensor->leftEdge.size() - 150; i < Lidar_Sensor->leftEdge.size(); i++) {
		//reset sum after every scan
		sum = 0.0;
		//remove T/B 10 % of points in distances vector
		sort(Lidar_Sensor->leftEdge[i].distances.begin(), Lidar_Sensor->leftEdge[i].distances.end());
		tenPercentCutoffIndex = 0.1 * Lidar_Sensor->leftEdge[i].distances.size();

		//remove bottom 10 percent of distances -> distance still in mm
		Lidar_Sensor->leftEdge[i].distances.erase(Lidar_Sensor->leftEdge[i].distances.begin(), Lidar_Sensor->leftEdge[i].distances.begin() + tenPercentCutoffIndex);

		//remove top 10 percent of distances for left Edge
		Lidar_Sensor->leftEdge[i].distances.erase(Lidar_Sensor->leftEdge[i].distances.end() - tenPercentCutoffIndex, Lidar_Sensor->leftEdge[i].distances.end());
		//now calculate distance averages for left edge and send to ValidAverageDistanceFor150Scans Vector
		for (int j = 0; j < Lidar_Sensor->leftEdge[i].distances.size(); j++) {
		sum += Lidar_Sensor->leftEdge[i].distances[j];
		}
		average = sum / Lidar_Sensor->leftEdge[i].distances.size();
		AveragePerScanLeftEdge.push_back(average); //populate averageDistance Per scan for left edge vector which will then aggregrate to one True Distance value for left Edge

		}**/

		sum = 0.0;

		//iterate through map

		for (itr = rightEdge150.begin(); itr != rightEdge150.end(); itr++) {


			//remove T/B 10 % of points in distances vector
			sort(itr->second.distances.begin(), itr->second.distances.end());
			tenPercentCutoffIndex = static_cast<int>(0.1 * itr->second.distances.size());

			//remove bottom 10 percent of distances -> distance still in mm
			itr->second.distances.erase(itr->second.distances.begin(), itr->second.distances.begin() + tenPercentCutoffIndex);

			//remove top 10 percent of distances for left Edge
			itr->second.distances.erase(itr->second.distances.end() - tenPercentCutoffIndex, itr->second.distances.end());
			//now calculate distance averages for left edge and send to ValidAverageDistanceFor150Scans Vector
			for (int j = 0; j < itr->second.distances.size(); j++) {
				sum += itr->second.distances[j];
			}
			average = sum / itr->second.distances.size();
			AveragePerScanRightEdge.push_back(average); //populate averageDistance Per scan for left edge vector which will then aggregrate to one True Distance value for left Edge


		}
		/**
		//create averages for each scan in rightEdge -> last 150 scans
		for (int i = Lidar_Sensor->rightEdge.size() - 150; i < Lidar_Sensor->rightEdge.size(); i++) {
		//reset sum after every scan
		sum = 0.0;
		//remove T/B 10 % of points in distances vector
		sort(Lidar_Sensor->rightEdge[i].distances.begin(), Lidar_Sensor->rightEdge[i].distances.end());
		tenPercentCutoffIndex = 0.1 * Lidar_Sensor->rightEdge[i].distances.size();

		//remove bottom 10 percent of distances -> distance still in mm
		Lidar_Sensor->rightEdge[i].distances.erase(Lidar_Sensor->rightEdge[i].distances.begin(), Lidar_Sensor->rightEdge[i].distances.begin() + tenPercentCutoffIndex);

		//remove top 10 percent of distances for right Edge
		Lidar_Sensor->rightEdge[i].distances.erase(Lidar_Sensor->rightEdge[i].distances.end() - tenPercentCutoffIndex, Lidar_Sensor->rightEdge[i].distances.end());
		//now calculate distance averages for right edge and send to ValidAverageDistanceFor150Scans Vector
		for (int j = 0; j < Lidar_Sensor->rightEdge[i].distances.size(); j++) {
		sum += Lidar_Sensor->rightEdge[i].distances[j];
		}
		average = sum / Lidar_Sensor->rightEdge[i].distances.size();
		AveragePerScanRightEdge.push_back(average); //populate averageDistance Per scan for right edge vector which will then aggregrate to one True Distance value for right Edge

		}**/

		//dont hold old scans
		leftEdge150.clear();
		rightEdge150.clear();

	}
	//wait for at least 25 scans ~ 0.5 seconds
	if (enteredLostMode && leftEdge150.size() >= 25 && rightEdge150.size() > 25) {
		for (itr = leftEdge150.begin(); itr != leftEdge150.end(); itr++) {

			for (int j = 0; j < itr->second.distances.size(); itr++) {
				sum += itr->second.distances[j];

			}
			average = sum / itr->second.distances.size();

			AveragePerScanLeftEdge.push_back(average); //populate averageDistance Per scan for left edge vector which will then aggregrate to one True Distance value for left Edge

		}

		/**
		for (int i = 0; i < Lidar_Sensor->leftEdge.size(); i++) {

		//now calculate distance averages for left edge and send to ValidAverageDistanceFor150Scans Vector
		for (int j = 0; j < Lidar_Sensor->leftEdge[i].distances.size(); j++) {
		sum += Lidar_Sensor->leftEdge[i].distances[j];
		}
		average = sum / Lidar_Sensor->leftEdge[i].distances.size();
		AveragePerScanLeftEdge.push_back(average); //populate averageDistance Per scan for left edge vector which will then aggregrate to one True Distance value for left Edge


		}
		**/
		sum = 0.0;
		for (itr = rightEdge150.begin(); itr != rightEdge150.end(); itr++) {

			for (int j = 0; j < itr->second.distances.size(); itr++) {
				sum += itr->second.distances[j];

			}
			average = sum / itr->second.distances.size();

			AveragePerScanLeftEdge.push_back(average); //populate averageDistance Per scan for left edge vector which will then aggregrate to one True Distance value for left Edge

		}
		/**
		for (int i = 0; i < Lidar_Sensor->rightEdge.size(); i++) {

		//now calculate distance averages for right edge and send to ValidAverageDistanceFor150Scans Vector
		for (int j = 0; j < Lidar_Sensor->rightEdge[i].distances.size(); j++) {
		sum += Lidar_Sensor->rightEdge[i].distances[j];
		}
		average = sum / Lidar_Sensor->rightEdge[i].distances.size();
		AveragePerScanRightEdge.push_back(average); //populate averageDistance Per scan for right edge vector which will then aggregrate to one True Distance value for right Edge

		}**/

		//dont hold old scans
		leftEdge150.clear();
		rightEdge150.clear();
	}




}

//trueDistanceFromLeft and TrueDistanceFromRight will give the average of averages of distances for 150 scans
//these values are then put into corresponding RunningLeftTrueDistanceAverages and RunningRightTrueDistanceAverages vectors
void calculateAverageDistancePer150Scans() {
	float TrueDistanceFromLeft = 0.0;
	float TrueDistanceFromRight = 0.0;
	float sum = 0.0;
	if (!enteredLostMode) {
		if (AveragePerScanLeftEdge.size() >= 150 && AveragePerScanRightEdge.size() >= 150) {

			for (int i = 0; i < AveragePerScanRightEdge.size(); i++) {
				sum += AveragePerScanRightEdge[i];
			}
			//Average of averages of right distance
			TrueDistanceFromRight = sum / AveragePerScanRightEdge.size(); //still in mm

			RunningRightTrueDistancePer150Scans.push_back(TrueDistanceFromRight); //push new distance to end of vector to be evaluated
			sum = 0.0;

			for (int i = 0; i < AveragePerScanLeftEdge.size(); i++) {
				sum += AveragePerScanLeftEdge[i];
			}
			TrueDistanceFromLeft = sum / AveragePerScanLeftEdge.size();

			RunningLeftTrueDistancePer150Scans.push_back(TrueDistanceFromLeft); //push new distance to end of vector to be evaluated

			AveragePerScanLeftEdge.clear();
			AveragePerScanRightEdge.clear();
		}
		else {
			//not enough scans yet
			//debug
			//cout << "Need More Scans" << endl;

		}


	}
	else if (enteredLostMode) {
		//only use next 25 scans instead of waiting for 150 scans
		sum = 0;
		if (AveragePerScanLeftEdge.size() >= 25 && AveragePerScanRightEdge.size() >= 25) {

			for (int i = 0; i < AveragePerScanRightEdge.size(); i++) {
				sum += AveragePerScanRightEdge[i];
			}
			//Average of averages of right distance
			TrueDistanceFromRight = sum / AveragePerScanRightEdge.size(); //still in mm

			RunningRightTrueDistancePer150Scans.push_back(TrueDistanceFromRight); //push new distance to end of vector to be evaluated
			sum = 0.0;

			for (int i = 0; i < AveragePerScanLeftEdge.size(); i++) {
				sum += AveragePerScanLeftEdge[i];
			}
			TrueDistanceFromLeft = sum / AveragePerScanLeftEdge.size();

			RunningLeftTrueDistancePer150Scans.push_back(TrueDistanceFromLeft); //push new distance to end of vector to be evaluated

																				//enteredLostMode = false;
		}


	}


}

//check if last distances calculated for RunningLeftTrueDistancePer150Scans and RunningRightTrueDistancePer150Scans vector
//if BOTH valid, keep in vectors and set validAverage = true, if EITHER ONE is not valid, we discard both averages from last index in vectors and validAverage = false


void validateAverage() {


	if (RunningLeftTrueDistancePer150Scans.size() != 0 && RunningRightTrueDistancePer150Scans.size() != 0) {
		//TODO: Use previous points to validate newestDistance, which is in index position RunningLeftTrueDistancePer150Scans.size() -1 and RunningRightTrueDistancePer150Scans.size()-1  //Vectors are LIFO. Thus newestDistance is at position 0 ::DMC::
		//if we see a reasonable new distance based on previous distances we saved, then we keep both left and right running distances and set validAverage = true	
		//else -> validAverage = false and discard last two right and left distances


		float runningAverageOfChangeInLast150Right = 0;			//Dynamic creation. Move up	//::DMC::
		float runningAverageOfChangeInLast150Left = 0;			//Dynamic creation. Move up	//::DMC::
		int numberOfAcceptedPointsLeft = 0;			//Dynamic creation. Move up	//::DMC::
		int numberOfAcceptedPointsRight = 0;			//Dynamic creation. Move up	//::DMC::


		for (int i = 0; i < RunningLeftTrueDistancePer150Scans.size(); i++)		//Iterate over every member of the vector		//::DMC::
		{
			if (RunningLeftTrueDistancePer150Scans[i] != 0)		//Sanity check. These should not be zero		//::DMC::
			{
				++numberOfAcceptedPointsLeft;						//Increment for every accepted point in vector that does not equal zero
			}
			else
			{
				RunningLeftTrueDistancePer150Scans.erase(RunningLeftTrueDistancePer150Scans.begin() + i);		//Cleaning bad positions out of vector //::DMC::
			}
		}

		for (int i = 0; i < RunningRightTrueDistancePer150Scans.size() - 1; i++)		//Iterate over every member of the vector		//::DMC::
		{
			if (RunningRightTrueDistancePer150Scans[i] != 0)		//Sanity check. These should not be zero		//::DMC::
			{
				++numberOfAcceptedPointsRight;						//Increment for every accepted point in vector that does not equal zero
			}
			else
			{
				RunningRightTrueDistancePer150Scans.erase(RunningRightTrueDistancePer150Scans.begin() + i);		//Cleaning bad positions out of vector //::DMC::
			}
		}

		for (int i = 0; i < numberOfAcceptedPointsLeft - 2; i++)		//Iterate over every accepted point		//::DMC::
		{
			runningAverageOfChangeInLast150Left = (RunningLeftTrueDistancePer150Scans[i] - RunningLeftTrueDistancePer150Scans[i + 1]) / numberOfAcceptedPointsLeft;		//Add that point to the running total average		//::DMC::
		}

		for (int i = 0; i < numberOfAcceptedPointsRight - 2; i++)
		{
			runningAverageOfChangeInLast150Right = (RunningRightTrueDistancePer150Scans[i] - RunningRightTrueDistancePer150Scans[i + 1]) / numberOfAcceptedPointsRight;	//Add the change to the running average	//::DMC::
		}

		int lastRightIndex = static_cast<int>(RunningRightTrueDistancePer150Scans.size() - 1);
		int lastLeftIndex = static_cast<int>(RunningLeftTrueDistancePer150Scans.size() - 1);
		//Idea: These should be seperated when it comes time to push to production. The algorithm should be able to continue to run when only seeing one side.
		if ((RunningLeftTrueDistancePer150Scans[lastLeftIndex] < leftMaxChange) && (RunningLeftTrueDistancePer150Scans[lastLeftIndex] < runningAverageOfChangeInLast150Left * leftChangeWeight))		//If left change acceptable		//::DMC::
		{
			if ((RunningRightTrueDistancePer150Scans[lastRightIndex] < rightMaxChange) && (RunningRightTrueDistancePer150Scans[lastRightIndex] < runningAverageOfChangeInLast150Right * rightChangeWeight))		//If right change acceptable		//::DMC::
			{
				validAverage = true;
				lostModeRunCounter = 0; //reset counter because valid average
			}
			else
			{
				validAverage = false;
			}
		}
		else
		{
			validAverage = false;
		}
	}
	else {
		//No average calc yet
		validAverage = false;
	}


	if (validAverage != true) {
		if (RunningLeftTrueDistancePer150Scans.size() > 0 && RunningRightTrueDistancePer150Scans.size() > 0) {
			RunningLeftTrueDistancePer150Scans.pop_back();
			RunningRightTrueDistancePer150Scans.pop_back();
		}

	}

	lostModeRunCounter++;//increment counter, once we are >150 scans without valid distances, we set to lostMode

	if (lostModeRunCounter == 25) {
		enteredLostMode = true;
		lostModeRunCounter = 0;
		//clear previous data
		RunningLeftTrueDistancePer150Scans.clear();
		RunningRightTrueDistancePer150Scans.clear();
	}
}

// [Philipp]: removed (see below and at the top of this file)
//struct tempPoint
//{
//	float x;
//	float y;
//	float i;	// value range: [0,1]
//	float a;    // angle
//
//	tempPoint(float _x, float _y, float _i, float _a = 0.0f) : x(_x), y(_y), i(_i), a(_a) {}
//};

void received(R2000Scan::Ptr&& packet)
{
	// the callback function should be left as soon as possible, otherwise old scans might get discarded
	// hold packets in vector for evaluation later
	// newPackets.points.resize()

	// [Philipp]: It would be nice to also move this vector::clear outside of this
	// function, since clearing a vector with many elements is a costly operation
	// (each item's destructor has to be called and the memory deallocated)

	EnterCriticalSection(&criticalSection);

	if (tempHoldPackets.size() > PACKET_VECTOR_CLEAR_AT_SIZE)
	{
		tempHoldPackets.clear();
	}

	// [Philipp]: Replaced holdPackets vector with vector<R2000Scan::Ptr>
	// Copying the scan each time is an expensive operation

	//for (int i = 0; i < packet->points.size(); ++i)
	//{
	//	newPacket.points.push_back(temp); 
	//}

	//for (int i = packet->points.size()-1; i >= 0; --i)
	//{
	//	newPacket.points[i].a = packet->points[i].a;
	//	newPacket.points[i].x = packet->points[i].x;
	//	newPacket.points[i].y = packet->points[i].y;
	//	newPacket.points[i].i = packet->points[i].i;
	//}
	//newPacket.timeStamp = packet->timeStamp;
	//newPacket.scanNumber = packet->scanNumber;

	tempHoldPackets.push_back(std::move(packet));

	LeaveCriticalSection(&criticalSection);
}

//used to validate error using previous error values to see if new calculated error( differenece between edge distances) is reasonable when
//compared to prior errors, set validError to true if we can say it is reasonably placed
void validateError(float error) {
	//validate error then push to running errors


	//TODO:Check if error reasonable and set validError to true if it is a reasonably calculated error based on what we would expect when looking at prior runningErrors


	//if error validated
	if (validError) {
		//this ensures we only keep track of all reasonable errors and dont save unreasonable ones
		RunningError.push_back(error);
	}

}

void ThrottleControl(float Error) {


	if (enteredLostMode) {
		//entireDistance in mm
		//24 ft = 7315.2 mm... 22 ft = 6705.6 mm ... 26 ft = 7924.8 mm
		float entireDistance = RunningLeftTrueDistancePer150Scans[RunningLeftTrueDistancePer150Scans.size() - 1] + RunningRightTrueDistancePer150Scans[RunningRightTrueDistancePer150Scans.size() - 1];
		if (entireDistance >= 6705.6 && entireDistance <= 7924.8) {//if true, then we can believe the measurement and actuate throttle
			enteredLostMode = false; //get out of lost mode now that we have a valid distances from left and right edge
		}
		else {
			RunningLeftTrueDistancePer150Scans.pop_back();
			RunningRightTrueDistancePer150Scans.pop_back();
			return;
		}
	}
	//need this to be able to iterate and access elements from last500Outputs Queue
	//	deque<float> copyLast500Outputs;
	//float element;

	float derivative = (Error - lastError);

	//copy
	//	copyLast500Outputs = last500Outputs;
	for (int i = 0; i < 500; i++)
	{
		//		element = copyLast500Outputs.front();
		//		averageOfLast500Outputs += (element / 500);
		//		copyLast500Outputs.pop();
		averageOfLast500Outputs += (last500Outputs[i] / 500);
	}
	output = (Error * Kp) + (averageOfLast500Outputs * Ki) + (derivative * Kd);
	if ((output - outputLast) > maxChange)
	{
		output = outputLast + maxChange;
	}
	else if ((outputLast - output) > maxChange)
	{
		output = outputLast - maxChange;
	}
	if (last500Outputs.size()> 500)
	{
		last500Outputs.pop_front();
	}
	last500Outputs.push_back(output);

	outputLast = output;
	lastError = Error;

}

int _tmain(int argc, _TCHAR * argv[])
{
	// [Philipp]: reserve memory for the vectors (to enable fast push_back())
	holdPackets.reserve(PACKET_VECTOR_RESERVE);
	tempHoldPackets.reserve(PACKET_VECTOR_RESERVE);

	// [Philipp]: Initialize critical section (used for thread-safe swapping of packet buffers)
	InitializeCriticalSection(&criticalSection);

	// [Philipp]: Lidar_Sensor moved to heap
	//auto Lidar_Sensor = std::make_unique<R2000Interface>(&received, "169.254.64.50");           // sensor IP
	auto Lidar_Sensor = std::make_unique<R2000Interface>(&received, "192.168.0.35");           // sensor IP

																								// set local configuration (not yet transmitted to device)
	Lidar_Sensor->config().frequency(50);
	Lidar_Sensor->config().samples(5040);							// [Philipp]: samples are now automatically set to the nearest compatible setting
	Lidar_Sensor->config().packetType(R2000Config::PacketType::B);	// currently supported: packet types A and B
	Lidar_Sensor->config().startAngle(0);							// start angle in [-180�, 179�]
	Lidar_Sensor->config().maxNumPointsScan(0);						// 0: points are only limited by setSamples()

	if (!Lidar_Sensor->connect())									// connect and transmit local configuration to device
	{
		PrintMsg("\nFailed to connect.\nClosing application\n");
		DeleteCriticalSection(&criticalSection);
		return EXIT_FAILURE;
	}

	Lidar_Sensor->config().print();
	PrintMsg("Configuration set-----\n");
	//Start scanning with Lidar sensor thus initiating logging

	PrintMsg("Start Scanning-------\n");
	Lidar_Sensor->startScanning();

	PrintMsg("StartED---------------------- Scanning-------\n");
	//PrintMsg("26");


	while (true) {
		//PrintMsg("27\n");
		//PrintMsg("27\n");

		PrintMsg("\n%lli", leftEdge150.size());
		PrintMsg("\n\n%lli", rightEdge150.size());
		filterReflectiveIntensity();
		//PrintMsg("28\n");
		//PrintMsg("28\n");

		//take away top/bottom 10 percent of points per scan then average out each scan
		calculateAverageDistancePerScan();
		//PrintMsg("29\n");
		//PrintMsg("29\n");

		//once, we have a average Distance per scan, we will have two vectors(AveragePerScanRightEdge, AveragePerScanLeftEdge) populated with distance averages for each scan out of 150,
		//we will then aggregate these distances into a true distance from left and right edge, once we have a True Average distance per 150 scans,
		//we will check to see if these true distances are reasonable and calculate the difference between true left edge and right edge average Distance from Lidar
		//we then check to see if the difference is a reasonable value and if so, we change throttle, if not we discard difference and keep throttle the same
		calculateAverageDistancePer150Scans();
		//		PrintMsg("29a\n");

		//		PrintMsg("29a\n");

		validateAverage();
		//		PrintMsg("30\n");


		if (validAverage || enteredLostMode) {
			//		PrintMsg("31\n");

			//manipulate throttle
			//first time we enter lost, mode we will not have any correct runningRight and left Distances
			if (RunningLeftTrueDistancePer150Scans.size() != 0 && RunningRightTrueDistancePer150Scans.size() != 0) {
				PrintMsg("32\n");

				float error = abs(RunningLeftTrueDistancePer150Scans[RunningLeftTrueDistancePer150Scans.size() - 1] - RunningRightTrueDistancePer150Scans[RunningRightTrueDistancePer150Scans.size() - 1]);
				PrintMsg("33\n");

				validateError(error);
				PrintMsg("34");

				if (validError) {
					PrintMsg("35");

					ThrottleControl(error);
					PrintMsg("36");

					validError = false;
					PrintMsg("37");

				}

				validAverage = false;

			}

		}


	}
	PrintMsg("\n[R2000TestApp] About to close.\n");

	Lidar_Sensor->disconnect(); // [Philipps-EMail vom 01.06.2018-22:15]

#ifndef UNDER_RTSS
	PrintMsg("Press RETURN key ...");
	getchar();
#endif

	DeleteCriticalSection(&criticalSection);
	return 0;
}
